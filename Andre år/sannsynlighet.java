import java.lang.Math;

class sannsynlighet {
    public static void main(String[] args){
        //double Xverdier[] = {1, 2, 3, 4, 5, 6};
        //double Xsannsynligheter[] = {0.2, 0.17, 0.17, 0.17, 0.17, 0.12};
        double Xverdier[] = {0.0, 0.3, 0.5, 0.6, 0.7, 0.8, 1.0, 1.2, 1.6};
        double Xsannsynligheter[] = {1.0/28.0, 2.0/28.0, 5.0/28.0, 8.0/28.0, 5.0/28.0, 3.0/28.0, 2.0/28.0, 3.0/56.0, 1.0/56.0};

        System.out.println("Forventning: " + forventning(Xverdier, Xsannsynligheter));
        System.out.println("Varians: " + varians(Xverdier, Xsannsynligheter));
        System.out.println("Standardavvik: " + standardavviket(Xverdier, Xsannsynligheter));
        double[] fordeling = fordelingsfunksjon(Xverdier, Xsannsynligheter);
        System.out.println("Fordeling: ");
        for(int i = 0; i < fordeling.length; i++){
            System.out.println(fordeling[i]);
        }
    }

    private static double forventning(double[] verdier, double[] sannsynligheter){
        double sum = 0;
        for(int i = 0; i < verdier.length; i++){
            sum += verdier[i]*sannsynligheter[i];
        }
        return sum;
    }

    private static double varians(double[] verdier, double[] sannsynligheter){
        double sum = 0;
        double forventning = forventning(verdier, sannsynligheter);
        for(int i = 0; i < verdier.length; i++){
            sum += (verdier[i]-forventning)*(verdier[i]-forventning)*sannsynligheter[i];
        }
        return sum;
    }

    private static double standardavviket(double[] verdier, double[] sannsynligheter){
        return Math.sqrt(varians(verdier, sannsynligheter));
    }

    private static double[] fordelingsfunksjon(double[] verdier, double[] sannsynligheter){
        double[] fordeling = new double[verdier.length];
        double sum = 0;
        for(int i = 0; i < verdier.length; i++){
            sum += sannsynligheter[i];
            fordeling[i] = sum;
        }
        return fordeling;
    }
}