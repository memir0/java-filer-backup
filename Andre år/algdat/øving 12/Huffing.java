import java.util.ArrayList;
import java.util.PriorityQueue;
import java.io.ObjectOutputStream;

class Huffing {
    public static void main(String[] args){
        // Leser inn filen som ArrayList hvor hver index er en linje av filen
        FilLeser fl = new FilLeser();
        ArrayList<String> fileLines = fl.lesFil("loremIpsum2.txt");

        // Bruker hjelpemetode for å telle frekvens av de ulike charene
        ArrayList<Pair> frequencyTable = createFrequencyTable(fileLines);
        /*
            Kommentar: Valgte å bruke arraylist for evig char støtte og for 
            å unngå å kaste bort plass på chars som aldri blir brukt. 
            Går på bekostning av at jeg må lagre Pair som inneholder chars.
        */

        for(int i = 0; i < frequencyTable.size(); i++){
            System.out.print(frequencyTable.get(i).character + ": ");
            System.out.println(frequencyTable.get(i).frequency);
        }

        // Priority queue av charene som sorteres etter frekvens
        PriorityQueue<HuffmanNode> charQueue = new PriorityQueue<HuffmanNode>(frequencyTable.size(), new HuffingComparator());

        for (int i = 0; i < frequencyTable.size(); i++) {
            // Lager ny node for hvert innlegg i frekvenstabellen
            HuffmanNode newHuffNode = new HuffmanNode(
                String.valueOf(frequencyTable.get(i).character),
                frequencyTable.get(i).frequency
            );
  
            // Legger til noden i køen
            charQueue.add(newHuffNode); 
        }

        // Bygger treet
        HuffmanNode root = null; 

        while (charQueue.size() > 1) { 
            // Plukker ut de to minst frekvente nodene
            HuffmanNode tempHuffNode1 = charQueue.poll(); 
            HuffmanNode tempHuffNode2 = charQueue.poll(); 
  
            // Forelderen har de to nodene som bar og har derfor deres samlede frekvens
            int parentFrequency = tempHuffNode1.frequency + tempHuffNode2.frequency; 
            
            // Vi setter char verdien til \0 for å indikere at dette er en forelder og ikke et løv
            String parentString = tempHuffNode1.characters + tempHuffNode2.characters;

            // Denne noden er de to andre sin nye forelder 
            HuffmanNode parent = new HuffmanNode(parentString, parentFrequency); 
  
            parent.leftChild = tempHuffNode1; 
            parent.rightChild = tempHuffNode2;  

            root = parent; 
  
            // Putter forelderen tilbake i queuen for at den også skal få foreldre.
            charQueue.add(parent);
        }

        // Nå er treet bygget og root er roten til treet. Da begyner vi å kode teksten
        byte[] bitString = encodeFile(fileLines, root);

        EncodedFile file = new EncodedFile(frequencyTable, bitString, root);

        if(fl.skrivKodetFil(file)){
            System.out.println("Fil Skrevet!");
        }
        else {
            System.out.println("Noe gikk galt");
        }
        //root.print2D(root);
        //root.printChildren();
        /*for(int i = 0; i < frequencyTable.size(); i++){
            System.out.print(frequencyTable.get(i).character + ": ");
            System.out.println(frequencyTable.get(i).frequency);
        }*/
    }

    public static ArrayList<Pair> createFrequencyTable(ArrayList<String> fileLines){
        ArrayList<Pair> frequencyTable = new ArrayList<Pair>();
    
        // Legger til linjeskift
        Pair antLinjeskift = new Pair('\n', fileLines.size()-1);
        frequencyTable.add(antLinjeskift);

        // Teller frekvens av chars
        // Første for-løkke er bare per linje og teller ikke på kompleksiteten per char
        for(int i = 0; i < fileLines.size(); i++){
            String tempLine = fileLines.get(i);
            for(int j = 0; j < tempLine.length(); j++){
                char tempChar = tempLine.charAt(j);
                boolean found = false;
                for(int g = 0; g < frequencyTable.size(); g++){
                    if(frequencyTable.get(g).character == tempChar){
                        frequencyTable.get(g).frequency++;
                        found = true;
                        break;
                    }
                }
                // Lag nytt pair hvis charen ikke allerede er i tabellen
                if(!found){
                    Pair newPair = new Pair(tempChar, 1);
                    frequencyTable.add(newPair);
                }
            }
        }
        return frequencyTable;
    }

    public static byte[] encodeFile(ArrayList<String> fileLines, HuffmanNode root){
        ArrayList<Byte> tempFile = new ArrayList<Byte>();
        Byte tempByte = 0;
        byte bitIndex = 0;

        for(int i = 0; i < fileLines.size(); i++){
            String tempLine = fileLines.get(i);
            for(int j = 0; j < tempLine.length(); j++){
                char tempChar = tempLine.charAt(j);
                HuffmanNode tempHuffNode = root;

                while(tempHuffNode.leftChild != null && tempHuffNode.rightChild != null){
                    boolean found = false;
                    for(int g = 0; g < tempHuffNode.leftChild.characters.length(); g++){
                        char charInNode = tempHuffNode.leftChild.characters.charAt(g);
                        if(tempChar == charInNode){
                            tempHuffNode = tempHuffNode.leftChild;
                            found = true;
                            byte bit = (byte)Math.pow(2, bitIndex);
                            tempByte = (byte)(tempByte | bit);
                            break;
                        }
                    }
                    // Hvis den ikke er i venstre må den være i høyre barn
                    if(!found){
                        tempHuffNode = tempHuffNode.rightChild;
                    }
                    // Sjekker omm byten er full
                    bitIndex++;
                    if(bitIndex % 8 == 0){
                        tempFile.add(tempByte);
                        tempByte = 0;
                        bitIndex = 0;
                    }
                }
            }
        }
        System.out.println(tempFile.size());
        byte[] file = new byte[tempFile.size()];
        for(int i = 0; i < tempFile.size(); i++) file[i] = tempFile.get(i);
        return file;
    }
}