import java.util.ArrayList;
// Dette objektet inneholder den kodede filen og frekvenstabellen
class EncodedFile {
    byte[] frequencyTableCharSize = new byte[4];
    String frequencyTableChars = "";
    byte[] frequencyTableFrequenciesSize = new byte[4];
    byte[] frequencyTableFrequencies;
    byte[] file;

    EncodedFile(ArrayList<Pair> frequencyTable, byte[] file, HuffmanNode root){
        // Hver int får 4 bytes
        frequencyTableFrequencies = new byte[frequencyTable.size()*4];
        for(int i = 0; i < frequencyTable.size(); i++){
            frequencyTableChars += frequencyTable.get(i).character;
            for(int j = 0; j < 4; j++){
                //System.out.println((byte)(frequencyTable.get(i).frequency >>> (j * 8)));
                frequencyTableFrequencies[(i+1)*4-(4-j)]  = (byte)(frequencyTable.get(i).frequency >>> (j * 8));
                //frequencyTableFrequencies[(i+1)*4-(4-j)] = (byte)(frequencyTable.get(i).frequency/Math.pow(10, 4-j)%10);
            }
        }
        //frequencyTableChars = root.characters;
        //System.out.println(frequencyTableChars);
        
        //Gjør om størrelsene fra int til byte[4]
        for(int j = 0; j < 4; j++){
            //System.out.println(frequencyTable.size() >>> j*4);
            this.frequencyTableFrequenciesSize[j] = (byte)(frequencyTable.size() >>> j*4);
        }
        for(int j = 0; j < 4; j++){
            //System.out.println(frequencyTableChars.getBytes().length >>> j*4);
            this.frequencyTableCharSize[j] = (byte)(frequencyTableChars.getBytes().length >>> j*4);
        }
        this.file = file;
    }
}