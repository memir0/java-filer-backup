import java.util.ArrayList;
import java.util.Arrays;
import java.io.*;
import java.util.PriorityQueue;

public class HuffingReader {
    public static String frequencyTableChars;
    public static int[] frequencyTableFrequencies;
    public static byte[] file;

    public static void main(String[] args){
        String filepath="C:\\Users\\Memir\\Desktop\\fil";
        lesKodetFil(filepath);
        
        HuffmanNode root = buildTree();

        //root.printChildren();
        String decodedFile = decodeFile(root);
        System.out.println(decodedFile);
    }

    static boolean lesKodetFil(String filepath){
        try (DataInputStream innfil = new DataInputStream(new BufferedInputStream(new FileInputStream(filepath)));) {
            int length = innfil.available();
            
            // Leser til buffer
            byte[] buf = new byte[length];
            innfil.readFully(buf);

            // Første 4 bytes are lengde på char andre 4 bytes er lengden vi skal lese av frekvenser
            int charLengde = 0;
            int frekLengde = 0;
            for(int j = 0; j < 4; j++){
                byte tempByte = buf[j];
                //System.out.println(buf[j]);
                if(tempByte < 0) tempByte = (byte)(256+tempByte);
                charLengde += (int)(tempByte) << j;
            }
            for(int j = 4; j < 8; j++){
                byte tempByte = buf[j];
                //System.out.println(buf[j]);
                if(tempByte < 0) tempByte = (byte)(256+tempByte);
                frekLengde += (int)(tempByte) << j-4;
            }
            //System.out.println("Charlengde: " + charLengde);
            //System.out.println("frekLengde: " + frekLengde);

            byte[] charBuf = Arrays.copyOfRange(buf, 8, 8+charLengde);
            frequencyTableChars = new String(charBuf,"UTF-8");

            frequencyTableFrequencies = new int[frekLengde];
            /*for(int i = 0; i < frekLengde*4; i++){
                System.out.println(buf[i+8+charLengde]);
            }*/
            for(int i = 0; i < frekLengde-2; i++){
                for(int j = 0; j < 4; j++){
                    //System.out.println("i*4+8+charLengde-j: " + (i*4+8+charLengde-j));
                    int tempInt = buf[(i+1)*4+charLengde+j+2];
                    System.out.print("J: " + j + " Byte: " + tempInt);
                    if(tempInt < 0){
                        tempInt = (256+tempInt);
                    }
                    tempInt = tempInt << j*8;
                    System.out.println(" Int: " + tempInt);
                    frequencyTableFrequencies[i] += tempInt;
                }
            }

            file = Arrays.copyOfRange(buf, frekLengde*4+charLengde+8, buf.length);
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
    public static String decodeFile(HuffmanNode root){
        String returnFile = "";
        Byte tempByte = 0;
        byte bitIndex = 0;
        HuffmanNode tempNode = root;

        for(int i = 0; i < file.length; i++){
            tempByte = file[i];
            for(int j = 0; j < 8; j++){
                byte mask = (byte)Math.pow(2, j);
                if((tempByte & mask) != 0){
                    tempNode = tempNode.leftChild;
                } else {
                    tempNode = tempNode.rightChild;
                }
                
                if(tempNode.leftChild == null && tempNode.rightChild == null){
                    // Fant et løv
                    returnFile += tempNode.characters;
                    tempNode = root;
                }
            }
        }
        return returnFile;
    }

    public static HuffmanNode buildTree(){
        // Bruker hjelpemetode for å telle frekvens av de ulike charene
        ArrayList<Pair> frequencyTable = createFrequencyTable();
        for(int i = 0; i < frequencyTable.size(); i++){
            System.out.print(frequencyTable.get(i).character + ": ");
            System.out.println(frequencyTable.get(i).frequency);
        }

        // Priority queue av charene som sorteres etter frekvens
        PriorityQueue<HuffmanNode> charQueue = new PriorityQueue<HuffmanNode>(frequencyTable.size(), new HuffingComparator());

        for (int i = 0; i < frequencyTable.size(); i++) {
            // Lager ny node for hvert innlegg i frekvenstabellen
            HuffmanNode newHuffNode = new HuffmanNode(
                String.valueOf(frequencyTable.get(i).character),
                frequencyTable.get(i).frequency
            );
  
            // Legger til noden i køen
            charQueue.add(newHuffNode); 
        }

        // Bygger treet
        HuffmanNode root = null; 

        while (charQueue.size() > 1) { 
            // Plukker ut de to minst frekvente nodene
            HuffmanNode tempHuffNode1 = charQueue.poll(); 
            HuffmanNode tempHuffNode2 = charQueue.poll(); 
  
            // Forelderen har de to nodene som bar og har derfor deres samlede frekvens
            int parentFrequency = tempHuffNode1.frequency + tempHuffNode2.frequency; 
            
            // Vi setter char verdien til \0 for å indikere at dette er en forelder og ikke et løv
            String parentString = tempHuffNode1.characters + tempHuffNode2.characters;

            // Denne noden er de to andre sin nye forelder 
            HuffmanNode parent = new HuffmanNode(parentString, parentFrequency); 
  
            parent.leftChild = tempHuffNode1; 
            parent.rightChild = tempHuffNode2;  

            root = parent; 
  
            // Putter forelderen tilbake i queuen for at den også skal få foreldre.
            charQueue.add(parent);
        }

        // Nå er treet bygget og root er roten til treet. Da begyner vi å dekode teksten
        return root;
    }

    public static ArrayList<Pair> createFrequencyTable(){
        ArrayList<Pair> frequencyTable = new ArrayList<Pair>();
        for(int i = 0; i < frequencyTableFrequencies.length; i++){
            char tempChar = frequencyTableChars.charAt(i);
            int tempFrequency = frequencyTableFrequencies[i];
            Pair newPair = new Pair(tempChar, tempFrequency);
            frequencyTable.add(newPair);
        }
        return frequencyTable;
    }
}