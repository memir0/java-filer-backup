//  Hver char i frekvenstabbelen blir en node når vi bygger treet
class HuffmanNode { 
    String characters; 
    int frequency;
    HuffmanNode leftChild; 
    HuffmanNode rightChild; 
    static final int COUNT = 10;  

    HuffmanNode(String characters, int frequency){
        this.characters = characters;
        this.frequency = frequency;
    }

    void printChildren(){
        System.out.println(characters);
        if(leftChild != null) leftChild.printChildren();
        if(rightChild != null) rightChild.printChildren();
    }

    static void print2DUtil(HuffmanNode root, int space)  
    {  
        // Base case  
        if (root == null)  
            return;  
    
        // Increase distance between levels  
        space += COUNT;  
    
        // Process right child first  
        print2DUtil(root.rightChild, space);  
    
        // Print current node after space  
        // count  
        System.out.print("\n");  
        for (int i = COUNT; i < space; i++)  
            System.out.print(" ");  
        System.out.print(root.characters + "\n");  
    
        // Process left child  
        print2DUtil(root.leftChild, space);  
    }  
    
    // Wrapper over print2DUtil()  
    static void print2D(HuffmanNode root)  
    {  
        // Pass initial space count as 0  
        print2DUtil(root, 0);  
    }  
} 