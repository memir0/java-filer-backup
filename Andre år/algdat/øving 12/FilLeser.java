import java.util.ArrayList;
import java.io.*;

public class FilLeser {
    static ArrayList<String> lesFil(String filnavn){
        try{
            FileReader leseforbTilFil = new FileReader(filnavn);
            BufferedReader leser = new BufferedReader(leseforbTilFil);
            ArrayList<String> linjer = new ArrayList<String>();
            String tempLinje = leser.readLine();
            while(tempLinje != null){
                linjer.add(tempLinje);
                tempLinje = leser.readLine();
            }
            leser.close();
            return linjer;
        }
        catch(Exception e) {
            System.out.println("ERROR");
            return null;
        }
    }
    static boolean skrivKodetFil(EncodedFile file){
        String filepath="C:\\Users\\Memir\\Desktop\\fil";
        try {
            try (DataOutputStream fos = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(filepath)));) {
                fos.write(file.frequencyTableCharSize);
                fos.write(file.frequencyTableFrequenciesSize);
                fos.write(file.frequencyTableChars.getBytes());
                fos.write(file.frequencyTableFrequencies);
                fos.write(file.file);
            }
            return true;
 
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
}