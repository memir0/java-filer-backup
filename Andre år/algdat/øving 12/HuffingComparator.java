import java.util.Comparator; 

// For å sammenligne nodene når vi bygger treet
class HuffingComparator implements Comparator<HuffmanNode> { 
    public int compare(HuffmanNode x, HuffmanNode y) {
        return x.frequency - y.frequency;
    }
} 