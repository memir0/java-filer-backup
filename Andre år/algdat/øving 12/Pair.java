// I frekvenstabellen har vi par av chars med deres korisponderende frekvens
class Pair{
    public char character;
    public int frequency;

    Pair(char character, int frequency){
        this.character = character;
        this.frequency = frequency;
    }
}