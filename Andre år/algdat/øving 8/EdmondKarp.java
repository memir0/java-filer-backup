import java.util.ArrayList;
import java.io.*;

class EdmondKarp {
    public static void main(String[] args){
        ArrayList<String> fil = lesFil("flytgraf3.txt");
        Graf graf = new Graf(Integer.parseInt(fil.get(0).split(" ")[0]));
        graf.leggTilKanter(fil);
        System.out.println(graf.maksFlyt(0, 1));
    }

    static ArrayList<String> lesFil(String filnavn){
        try{
            FileReader leseforbTilFil = new FileReader(filnavn);
            BufferedReader leser = new BufferedReader(leseforbTilFil);
            ArrayList<String> linjer = new ArrayList<String>();
            String tempLinje = leser.readLine();
            while(tempLinje != null){
                linjer.add(tempLinje);
                tempLinje = leser.readLine();
            }
            leser.close();
            return linjer;
        }
        catch(Exception e) {
            System.out.println("ERROR");
            return null;
        }
    }
}