class Kant {
    int tilNode;
    int fraNode;
    int kapasitet;
    int flyt = 0;
    Kant revers;

    Kant(int tilNode, int fraNode, int kapasitet){
        this.tilNode = tilNode;
        this.fraNode = fraNode;
        this.kapasitet = kapasitet;
    }

    public void setRevers(Kant revers){
        this.revers = revers;
    }
}