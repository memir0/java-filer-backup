import java.util.ArrayList;
import java.util.Queue;
import java.util.LinkedList;

class Graf {
    Node[] noder;

    Graf(int size){
        noder = new Node[size];
        for(int i = 0; i < size; i++){
            noder[i] = new Node();
        }
    }

    public int maksFlyt(int start, int slutt) {
        int maksFlyt = 0;
        Kant[] forgjenger;
        do{
            Queue<Integer> queue = new LinkedList<Integer>();
            queue.add(start);
            forgjenger = new Kant[noder.length];
            while(queue.size() != 0){
                Node tempNode = noder[queue.poll()];
                for(int i = 0; i < tempNode.kanter.size(); i++){
                    Kant tempKant = tempNode.kanter.get(i); 
                    if(forgjenger[tempKant.tilNode] == null && tempKant.tilNode != start && tempKant.kapasitet > tempKant.flyt){
                        forgjenger[tempKant.tilNode] = tempKant;
                        queue.add(tempKant.tilNode);
                    }
                }
            }
            if(forgjenger[slutt] != null){
                String utskrift = "";
                int df = 1000000000;
                for(Kant e = forgjenger[slutt]; e != null; e = forgjenger[e.fraNode]){
                    df = min(df, e.kapasitet-e.flyt);
                    utskrift = e.fraNode + " " + utskrift;
                }
                for(Kant e = forgjenger[slutt]; e != null; e = forgjenger[e.fraNode]){
                    e.flyt += df;
                    e.revers.flyt -= df;
                }
                System.out.println("Okning: " + df +  " | " + utskrift + slutt);
                maksFlyt += df;
            }
        } while(forgjenger[slutt] != null);

        return maksFlyt;
    }

    private int min(int num1, int num2){
        if(num1 < num2) return num1;
        else return num2;
    }

    public void leggTilKanter(ArrayList<String> fil){
        for(int i = 1; i < fil.size(); i++){
            String[] kantSplit = fil.get(i).split(" ");
            int fraNode = Integer.parseInt(kantSplit[0]);
            int tilNode = Integer.parseInt(kantSplit[1]);
            int kapasitet = Integer.parseInt(kantSplit[2]);

            Kant nykant = new Kant(tilNode, fraNode, kapasitet);
            Kant nykantRevers = new Kant(fraNode, tilNode, 0);

            nykant.setRevers(nykantRevers);
            nykantRevers.setRevers(nykant);

            noder[fraNode].leggTilKant(nykant);
            noder[tilNode].leggTilKant(nykantRevers);
        }
    }
}