import java.util.ArrayList;
import java.util.Queue;
import java.util.LinkedList;
import java.io.*;

class graf{
    static ArrayList<String> lesFil(String filnavn){
        try{
            FileReader leseforbTilFil = new FileReader(filnavn);
            BufferedReader leser = new BufferedReader(leseforbTilFil);
            ArrayList<String> linjer = new ArrayList<String>();
            String tempLinje = leser.readLine();
            while(tempLinje != null){
                linjer.add(tempLinje);
                tempLinje = leser.readLine();
            }
            leser.close();
            return linjer;
        }
        catch(Exception e) {
            System.out.println("ERROR");
            return null;
        }
    }

    static public String nodeToString(Node[] hashStrings){
        String returString = "";
        for(int i = 0; i < hashStrings.length; i++){
            if(hashStrings[i] == null){
                returString += "null \n";
            }
            else{
                Node tempNode = hashStrings[i];
                while(tempNode != null){
                    returString += tempNode.getPlass() + " --> ";
                    tempNode = tempNode.getBarn();
                }
                returString += "null\n";
            }
        }
        return returString;
    }

    static public String BFS(int start, Node[] noder){
        int INF = 1000000000;
        Queue<Node> nodeKo = new LinkedList<Node>();
        Queue<Integer> nodePlassKo = new LinkedList<Integer>();
        int[][] tabell = new int[noder.length][2];

        for(int i = 0; i < noder.length; i++) tabell[i][1] = INF;

        nodeKo.add(noder[start]);
        nodePlassKo.add(start);
        tabell[start][0] = -1;
        tabell[start][1] = 0;

        while(!nodeKo.isEmpty()){
            Node tempNode = nodeKo.poll();
            Integer forgjenger = nodePlassKo.poll();
            int distanse = tabell[forgjenger][1];

            while(tempNode != null){
                if(tabell[tempNode.getPlass()][1] == INF){ 
                    tabell[tempNode.getPlass()][1] = distanse+1;
                    tabell[tempNode.getPlass()][0] = forgjenger;
                    nodeKo.add(noder[tempNode.getPlass()]);
                    nodePlassKo.add(tempNode.getPlass());
                }
                tempNode = tempNode.getBarn();
            }
        }

        String returString = "Node Forgj Dist";
        for(int i = 0; i < noder.length; i++){
            returString += "\n" + i + "    " + tabell[i][0] + "     " + tabell[i][1];
        }
        return returString; 
    }

    public static String topologiskSortering(Node[] noder){
        ArrayList<Integer> liste = new ArrayList<Integer>();
        int antallDead = 0;
        int teller = 0;
        for(int i = 0; i < noder.length; i++){
            if(noder[i] == null){
                antallDead++;
                liste.add(i);
                System.out.println(i);
            }
        }
        while(antallDead != noder.length){
            if(noder[teller] != null){
                Node tempNode = noder[teller];
                int forrigeNode = teller;
                while(noder[tempNode.getPlass()] != null){
                    forrigeNode  = tempNode.getPlass();
                    tempNode = noder[tempNode.getPlass()];
                }
                noder[forrigeNode] = null;
                liste.add(forrigeNode);
                antallDead++;
            }
            teller = (teller+1)%noder.length;
        }
        String returString = "";
        for(int i = 1; i < liste.size()+1; i++) returString += liste.get(liste.size()-i) + " ";
        return returString;
    }

    public static void main(String[] args){
        ArrayList<String> graf = lesFil("L7g5.txt");

        Node[] noder = new Node[Integer.parseInt(graf.get(0).split(" ")[0])];

        for(int i = 1; i < graf.size(); i++){
            String[] fraTilNode = graf.get(i).split(" ");
            if(noder[Integer.parseInt(fraTilNode[0])] == null){
                noder[Integer.parseInt(fraTilNode[0])] = new Node(Integer.parseInt(fraTilNode[1]));
            }
            else {
                Node tempNode = noder[Integer.parseInt(fraTilNode[0])];
                while(tempNode.getBarn() != null) tempNode = tempNode.getBarn();
                tempNode.setBarn(new Node(Integer.parseInt(fraTilNode[1])));
            }
        }
        //System.out.println(nodeToString(noder));

        //System.out.println(BFS(5, noder));

        System.out.println(topologiskSortering(noder));
    }
}

class Node {
    private Node barn;
    private int plass;

    Node(int plass){
        this.plass = plass;
    }

    public Node getBarn(){
        return barn;
    }

    public void setBarn(Node barn){
        this.barn = barn;
    }

    public int getPlass(){
        return plass;
    }
}