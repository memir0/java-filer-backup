class Automat {
    constructor(inputAlfabet, aksepterteTilstander) {
        this.inputAlfabet = inputAlfabet;
        this.aksepterteTilstander = aksepterteTilstander;
    }

    sjekkInput(input) {
        return input.match(this.aksepterteTilstander);
    }
}
var regexPattern = /00*1{1}/;
var alfabet = "01";
var minAutomat = new Automat(alfabet, regexPattern);

console.log(minAutomat.sjekkInput("010"));
console.log(minAutomat.sjekkInput("111"));
console.log(minAutomat.sjekkInput("010110"));
console.log(minAutomat.sjekkInput("001000"));

regexPattern = /^(ab)|^(ba)/;
alfabet = "ab";
minAutomat = new Automat(alfabet, regexPattern);

console.log(minAutomat.sjekkInput("abbb"));
console.log(minAutomat.sjekkInput("aaab"));
console.log(minAutomat.sjekkInput("babab"));
