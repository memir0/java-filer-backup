import java.lang.Math;
import java.util.Random;
import java.util.HashMap;

class DobbelHash{
    int[] tabell;
    int antallElementer = 0;
    int antallKolisjoner = 0;
    double A = ((Math.sqrt(5))-1)/2;

    DobbelHash(int lengde){
        tabell = new int[lengde];
    }

    boolean leggInn(int nyttTall){
        // Sjekker om vi har plass
        if(antallElementer-1 >= tabell.length) return false;

        int indeks = multiplikativHash(nyttTall);

        // Finner ledig plass
        int tempInt = tabell[indeks];
        if(tempInt == 0){
            // Legger inn element hvis første plass var tom
            tabell[indeks] = nyttTall;
        }
        else{
            antallKolisjoner++;
            // Finner neste plass med h2 (rest divisjon) til vi har funnet en ledig, modulo med tabell lengde for å unngå array out of bounds
            while(tempInt != 0){ 
                indeks = (indeks + (nyttTall*2+1))%tabell.length;
                
                tempInt = tabell[indeks];
            }
            tabell[indeks] = nyttTall;
        }
        antallElementer++;
        return true;
    }

    boolean finnTall(int tall){
        int indeks = multiplikativHash(tall);

        // Finner ledig plass
        int tempInt = tabell[indeks];
        if(tempInt == tall){
            return true;
        }
        else if(tempInt == 0){
            return false;
        }
        else{
            // Finner neste plass med h2 til vi har funnet en som er tom eller tallet
            while(tempInt != 0){ 
                indeks = (indeks + (indeks*2+1))%tabell.length;
                tempInt = tabell[indeks];
                if(tall == tempInt) return true;
            }
            return false;
        }
    }

    int multiplikativHash(int k){
        return (int)(tabell.length*((double)k*A-(int)(k*A)));
    }

    double lastfaktor(){
        return (double)antallElementer/(double)tabell.length;
    }
}

class dobbelHashTest {
    public static void main(String[] args){
        int lengde = 5000000;
        DobbelHash dh = new DobbelHash(6000011);
        Random rand = new Random();
        int[] prepedInts = new int[lengde];

        for(int i = 0; i < lengde; i++){
            prepedInts[i] = rand.nextInt(2147483)+1;
        }

        long startTime = System.nanoTime();
        for(int i = 0; i < lengde; i++){
            dh.leggInn(prepedInts[i]);
        }
        long endTime = System.nanoTime();

        System.out.println("Tid: " + (endTime-startTime)/1000000 + " millisekunder");
        System.out.println(dh.finnTall(prepedInts[0]));
        System.out.println(dh.finnTall(prepedInts[100]));
        System.out.println(dh.finnTall(100));
        System.out.println(dh.lastfaktor());
        System.out.println(dh.antallKolisjoner);
        
        // Java sin hash map
        HashMap hm = new HashMap(6000011);
        startTime = System.nanoTime();
        for(int i = 0; i < lengde; i++){
            hm.put(prepedInts[i], 0);
        }
        endTime = System.nanoTime();
        System.out.println("Tid for java sin: " + (endTime-startTime)/1000000 + " millisekunder");
    }
}