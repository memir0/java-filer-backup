import java.util.ArrayList;
import java.io.*;

class HashTabel{
    Node[] hashStrings;
    int antallElementer = 0;
    int antallKolisjoner = 0;

    HashTabel(int lengde){
        hashStrings = new Node[lengde];
    }

    void leggInn(String nyString){
        int indeks = finnIndeks(nyString);

        // Finner ledig plass
        Node tempNode = hashStrings[indeks];
        if(tempNode == null){
            // Legger inn hode
            hashStrings[indeks] = new Node(nyString);
        }
        else{
            // Finner halen
            while(tempNode.neste != null){ 
                antallKolisjoner++;
                System.out.println(nyString + " koliderte med "+ tempNode.neste.navn);
                tempNode = tempNode.neste;
            }
            tempNode.neste = new Node(nyString);
        }
        antallElementer++;
    }

    boolean finnPerson(String finnString){
        int indeks = finnIndeks(finnString);

        // Finner ledig plass
        Node tempNode = hashStrings[indeks];
        while(tempNode != null){
            // Ser om personen sitt navn er i listen
            if(tempNode.navn.equals(finnString)) return true;
            tempNode = tempNode.neste;
        }
        return false;
    }

    int finnIndeks(String nyString){
        int indeks = 0;
        for(int i = 0; i < nyString.length(); i++){
            // Legger til i for å vekte forskjellig, modulo lengden på arrayen for å holde den innafor
            indeks = (indeks+(int)nyString.charAt(i)*(i+1))%hashStrings.length;
        }
        return indeks;
    }

    double lastfaktor(){
        return (double)antallElementer/(double)hashStrings.length;
    }

    double antallKolisjonerPerPers(){
        return (double)antallKolisjoner/(double)antallElementer;
    }

    public String toString(){
        String returString = "";
        for(int i = 0; i < hashStrings.length; i++){
            if(hashStrings[i] == null){
                returString += "null \n";
            }
            else{
                Node tempNode = hashStrings[i];
                while(tempNode != null){
                    returString += tempNode.navn + " --> ";
                    tempNode = tempNode.neste;
                }
                returString += "null\n";
            }
        }
        return returString;
    }
}

class Node {
    public Node neste;
    public String navn;

    Node(String navn){
        this.navn = navn;
    }
}

class hashTest{
    public static void main(String[] args){
        HashTabel ht = new HashTabel(125);
        ArrayList<String> navn = lesFil("navn.txt");

        for(int i = 0; i < navn.size(); i++){
            ht.leggInn(navn.get(i));
        }

        System.out.println("Andersson,Vegard er i tabellen: " + ht.finnPerson("Andersson,Vegard"));
        System.out.println("Andersson,Vgarde er i tabellen: " + ht.finnPerson("Andersson,Vgarde"));
        System.out.println("Last faktor: " + ht.lastfaktor());
        System.out.println("Antall kolisjoner: " + ht.antallKolisjoner);
        System.out.println("Antall kolisjoner per pers: " + ht.antallKolisjonerPerPers());
        //System.out.println(ht.toString());
    }

    static ArrayList<String> lesFil(String filnavn){
        try{
            FileReader leseforbTilFil = new FileReader(filnavn);
            BufferedReader leser = new BufferedReader(leseforbTilFil);
            ArrayList<String> linjer = new ArrayList<String>();
            String tempLinje = leser.readLine();
            while(tempLinje != null){
                linjer.add(tempLinje);
                tempLinje = leser.readLine();
            }
            leser.close();
            return linjer;
        }
        catch(Exception e) {
            System.out.println("ERROR");
            return null;
        }
        //Mer spesifike catches
        //try with resources
    }
}