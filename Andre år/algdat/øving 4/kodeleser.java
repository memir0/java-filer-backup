import java.util.ArrayList;
import java.util.Stack;
import java.io.*;

class kodeleser {
    public static void main(String[] args){
        System.out.println(sjekkfil("kodeleser.java"));
    }

    static boolean sjekkfil(String filnavn){
        ArrayList<String> fil = lesFil(filnavn);
        Stack<Character> brackets = new Stack<Character>();

        boolean ignore = false;
        boolean rett = true;

        for(int i = 0; i < fil.size()-1; i++){
            for(int j = 0; j < fil.get(i).length(); j++){
                char tempChar = fil.get(i).charAt(j);
                if(ignore){
                    if(tempChar == '"' || tempChar == '\''){
                        ignore = false;
                    }
                }
                else {
                    if(tempChar == '"' || tempChar == '\''){
                        ignore = true;
                    }
                    else {
                        if(tempChar == '(' || tempChar == '{' || tempChar == '['){
                            brackets.push(tempChar);
                        }
                        else if (tempChar == ')' || tempChar == '}' || tempChar == ']'){
                            if(brackets.empty()){
                                System.out.println("Stack er tom");
                                return false;
                            }
                            else{ 
                                char popchar = brackets.pop();
                                if (tempChar == ')' && popchar == '('){}
                                else if (tempChar == ']' && popchar == '['){}
                                else if (tempChar == '}' && popchar == '{'){}
                                else{
                                    System.out.println("Feil: " + tempChar + " != " + popchar);
                                    return false;
                                }
                            }
                        }
                    }
                }
            }
        }
        if(brackets.empty()) return true;
        else return false;
    }

    static ArrayList<String> lesFil(String filnavn){
        try{
            FileReader leseforbTilFil = new FileReader(filnavn);
            BufferedReader leser = new BufferedReader(leseforbTilFil);
            ArrayList<String> linjer = new ArrayList<String>();
            String tempLinje = leser.readLine();
            while(tempLinje != null){
                linjer.add(tempLinje);
                tempLinje = leser.readLine();
            }
            leser.close();
            return linjer;
        }
        catch(Exception e) {
            System.out.println("ERROR");
            return null;
        }
        //Mer spesifike catches
        //try with resources
    }
}