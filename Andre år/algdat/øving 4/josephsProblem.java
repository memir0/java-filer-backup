import java.util.Scanner;

class josephsProblem {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        int antall = sc.nextInt();
        EnkeltLenke ek = new EnkeltLenke();
        Node tempNode = new Node(null, 0);
        ek.hode = tempNode;
        ek.hale = tempNode;
        for(int i = 0; i < antall-1; i++){
            tempNode = new Node(ek.hode, ek.antElementer);
            ek.hale.neste = tempNode;
            ek.hale = tempNode;
            ek.antElementer++;
        }

        Node forrigeNode = ek.hode;
        Node nesteNode = forrigeNode.neste;
        int teller = 1;

        while(nesteNode != forrigeNode){
            if(teller >= 2){
                // Oppdater hode hvis det slettes
                if(forrigeNode.neste == ek.hode){
                    ek.hode = nesteNode.neste;
                }
                else if(forrigeNode.neste == ek.hale){
                    ek.hale = forrigeNode;
                }
                forrigeNode.neste = nesteNode.neste;
                ek.antElementer--;
                teller = -1;
            }
            forrigeNode = nesteNode;
            nesteNode = forrigeNode.neste;
            teller++;
        }
        System.out.println(nesteNode.plass);
        sc.close();
    }
}

class Node {
    public Node neste;
    public int plass;

    Node(Node neste, int plass){
        this.neste = neste;
        this.plass = plass;
    }
}

class EnkeltLenke {
    public Node hode;
    public Node hale;
    public int antElementer = 1;
}