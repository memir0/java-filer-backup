import java.util.Scanner;
class pow {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        System.out.println("Skriv inn base tall x:");
        double x = (double) sc.nextDouble();
        System.out.println("Skriv inn eksponent n:");
        int n = sc.nextInt();

        int runs = 100000;
        System.out.println("amount of runs per method: " + runs);

        double out = 0;
        long startTime = System.nanoTime();
        for(int i = 0; i < runs; i++){
            out = recursivePow(x, n);
        }
        long endTime = System.nanoTime();
        System.out.println("resultat pow1:" + out);
        System.out.println("Time: " + (endTime-startTime));

        startTime = System.nanoTime();
        for(int i = 0; i < runs; i++){
            out = recursivePow2(x, n);
        }
        endTime = System.nanoTime();
        System.out.println("resultat pow2:" + out);
        System.out.println("Time: " + (endTime-startTime));

        startTime = System.nanoTime();
        for(int i = 0; i < runs; i++){
            out = Math.pow(x,n);
        }
        endTime = System.nanoTime();
        System.out.println("resultat innebygd pow:" + out);
        System.out.println("Time: " + (endTime-startTime));

        sc.close();
    }

    // Kompleksitet: n
    private static double recursivePow(double x, int n){
        if (n <= 0){
            return 1;
        }
        else {
            n--;
            return x * recursivePow(x, n);
        }
    }

    //kompleksitet: log n
    private static double recursivePow2(double x, int n){
        if (n <= 0){
            return 1;
        }
        else if(n % 2 == 1){
            return x*recursivePow2(x*x, n/2);
        }
        else {
            return recursivePow2(x*x, n/2);
        }
    }
}