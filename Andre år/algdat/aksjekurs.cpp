#include <iostream>
#include <vector>
#include <chrono>
#include <stdlib.h>     /* srand, rand */
#include <time.h> 

int main(){
    // 1 tilorning
    //int kurser[9] = {-1, 3, -9, 2, 2, -1, 2, -1, -5};
    //int kurser[660] = {-1, 3, -9, 2, 2, -1, 2, -1, -5, 2, 35,39,31,70,89,28,5,4,7,82,70,4,67,57,27,79,6,40,92,95,92,94,92,7,80,13,41,39,86,82,51,97,62,51,5,49,52,29,19,33,23,91,38,80,18,49,88,44,46,98,22,89,34,12,81,73,8,95,69,48,57,82,57,71,50,1,50,57,85,67,52,88,91,78,86,64,95,29,7,29,82,44,39,23,35,99,11,23,21,1,34,86,97,47,64,32,16,88,8,96, -1, 3, -9, 2, 2, -1, 2, -1, -5, 2, 35,39,31,70,89,28,5,4,7,82,70,4,67,57,27,79,6,40,92,95,92,94,92,7,80,13,41,39,86,82,51,97,62,51,5,49,52,29,19,33,23,91,38,80,18,49,88,44,46,98,22,89,34,12,81,73,8,95,69,48,57,82,57,71,50,1,50,57,85,67,52,88,91,78,86,64,95,29,7,29,82,44,39,23,35,99,11,23,21,1,34,86,97,47,64,32,16,88,8,96, -1, 3, -9, 2, 2, -1, 2, -1, -5, 2, 35,39,31,70,89,28,5,4,7,82,70,4,67,57,27,79,6,40,92,95,92,94,92,7,80,13,41,39,86,82,51,97,62,51,5,49,52,29,19,33,23,91,38,80,18,49,88,44,46,98,22,89,34,12,81,73,8,95,69,48,57,82,57,71,50,1,50,57,85,67,52,88,91,78,86,64,95,29,7,29,82,44,39,23,35,99,11,23,21,1,34,86,97,47,64,32,16,88,8,96, -1, 3, -9, 2, 2, -1, 2, -1, -5, 2, 35,39,31,70,89,28,5,4,7,82,70,4,67,57,27,79,6,40,92,95,92,94,92,7,80,13,41,39,86,82,51,97,62,51,5,49,52,29,19,33,23,91,38,80,18,49,88,44,46,98,22,89,34,12,81,73,8,95,69,48,57,82,57,71,50,1,50,57,85,67,52,88,91,78,86,64,95,29,7,29,82,44,39,23,35,99,11,23,21,1,34,86,97,47,64,32,16,88,8,96, -1, 3, -9, 2, 2, -1, 2, -1, -5, 2, 35,39,31,70,89,28,5,4,7,82,70,4,67,57,27,79,6,40,92,95,92,94,92,7,80,13,41,39,86,82,51,97,62,51,5,49,52,29,19,33,23,91,38,80,18,49,88,44,46,98,22,89,34,12,81,73,8,95,69,48,57,82,57,71,50,1,50,57,85,67,52,88,91,78,86,64,95,29,7,29,82,44,39,23,35,99,11,23,21,1,34,86,97,47,64,32,16,88,8,96, -1, 3, -9, 2, 2, -1, 2, -1, -5, 2, 35,39,31,70,89,28,5,4,7,82,70,4,67,57,27,79,6,40,92,95,92,94,92,7,80,13,41,39,86,82,51,97,62,51,5,49,52,29,19,33,23,91,38,80,18,49,88,44,46,98,22,89,34,12,81,73,8,95,69,48,57,82,57,71,50,1,50,57,85,67,52,88,91,78,86,64,95,29,7,29,82,44,39,23,35,99,11,23,21,1,34,86,97,47,64,32,16,88,8,96};
    int kurser[100];
    int antallDager = 100;
    int value = kurser[0];
    int lowerbound = kurser[0];
    int upperbound = kurser[0];
    int buyDay = 0;
    int sellDay = 0;
    int biggestDiff = 0;
    int bestBuyDay = 0;
    int bestSellDay = 0;
    int bestDiff = 0;
    
    // Fyll kurser
    srand (time(NULL));
    for(int i = 0; i < antallDager; i++){
        // gir tilfeldig fortegn
        if(rand() %2 == 0){
            kurser[i] = rand() % 100 + 1;
        }
        else{
            kurser[i] = -1 * rand() % 100 + 1;
        }
    }


    // Klokkestart
    auto t1 = std::chrono::high_resolution_clock::now();

    for(int i = 1; i < antallDager; i ++){
        value += kurser[i];
        if(value > upperbound) {
            upperbound = value;
            sellDay = i;
        }
        else if(value < lowerbound) {
            //Lagrer differansen før nytt bunnpunkt
            if((upperbound-lowerbound) > bestDiff){
                bestBuyDay = buyDay;
                bestSellDay = sellDay;
                bestDiff = (upperbound-lowerbound);
            }
            lowerbound = value;
            upperbound = value;
            buyDay = i;
        }
    }

    //Klokkeslutt
    auto t2 = std::chrono::high_resolution_clock::now();
    
    std::cout << "Diff: " << bestDiff << std::endl;
    std::cout << "BuyDate: " << bestBuyDay+1 << std::endl;
    std::cout << "SellDate: " << bestSellDay+1 << std::endl;
    std::cout << "Funksjonen brukte "
              << std::chrono::duration_cast<std::chrono::nanoseconds>(t2-t1).count()
              << " nanosekunder\n";

    /* Ekstremalpunk løsning
    std::vector<int> ekstremalpunkter;
    std::vector<int> ekstremaldager;

    // 1 tilorning
    ekstremalpunkter.push_back(kurser[0]);
    ekstremaldager.push_back(0);

    // n tilorning
    for(int i = 1; i < antallDager-1; i++){
        tempVerdi += kurser[i];
        if(kurser[i] > 0 && kurser[i+1] <= 0){
            ekstremalpunkter.push_back(tempVerdi);
            ekstremaldager.push_back(i);
        }
        else if(kurser[i] < 0 && kurser[i+1] >= 0){
            ekstremalpunkter.push_back(tempVerdi);
            ekstremaldager.push_back(i);
        }
        else {
            //Rettningen er lik, gjør ingenting
        }
    }
    tempVerdi += kurser[antallDager-1];
    ekstremalpunkter.push_back(tempVerdi);
    ekstremaldager.push_back(antallDager-1);
    
    int biggestDiff = 0;
    int buyDay = 0, sellDay = 0;
    // n*n tilordning
    for(int i = 0; i < ekstremalpunkter.size(); i += 2){
        for(int j = i+1; j < ekstremalpunkter.size(); j += 2){
            if(ekstremalpunkter[j]-ekstremalpunkter[i] > biggestDiff){
                biggestDiff = ekstremalpunkter[j]-ekstremalpunkter[i];
                buyDay = ekstremaldager[i];
                sellDay = ekstremaldager[j];
            }
        }
    }
    */
    
    /* Nikkos løsning
    auto t1 = std::chrono::high_resolution_clock::now();
    // kompleksitetsanalyse
    int kursEndring[9] = {-1, 3, -9, 2, 2, -1, 2, -1, -5}; // 1 tilordning
    int currentMax = 0; // 1 tilordning
    int fortjeneste = 0; // 1 tilordning
    int dagKjop = 0; // 1 tilordning
    int dagSelg = 0; // 1 tilordning

    for (int i = 0; i < 9; i++) { // 1 tilordning, n x tilordning, n x tilordninger
      fortjeneste = 0; // 1 tilordning
      for (int k = i; k < 9; k++) {// 1 tilorning,
        fortjeneste += kursEndring[k];
        if (fortjeneste > currentMax) {
          currentMax = fortjeneste;
          dagKjop = i;
          dagSelg = k + 1;
        }
      }
    }
    std::cout << "Dag kjøpt: " << dagKjop << "\nDag solgt: " << dagSelg << "\nFortjeneste: " << currentMax << std::endl;
    auto t2 = std::chrono::high_resolution_clock::now();
    std::cout << "Funksjonen brukte "
              << std::chrono::duration_cast<std::chrono::nanoseconds>(t2-t1).count()
              << " nanosekunder\n";
    */
}