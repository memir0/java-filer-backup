static ArrayList<String> lesFil(String filnavn){
    try{
        FileReader leseforbTilFil = new FileReader(filnavn);
        BufferedReader leser = new BufferedReader(leseforbTilFil);
        ArrayList<String> linjer = new ArrayList<String>();
        String tempLinje = leser.readLine();
        while(tempLinje != null){
            linjer.add(tempLinje);
            tempLinje = leser.readLine();
        }
        leser.close();
        return linjer;
    }
    catch(Exception e) {
        System.out.println("ERROR");
        return null;
    }
    //Mer spesifike catches
    //try with resources
}