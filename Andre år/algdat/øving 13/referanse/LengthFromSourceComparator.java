class LengthFromSourceComparator implements Comparator<Integer> {
    private int[] lengthFromSource;

    LengthFromSourceComparator(int[] lengthFromSource) {
        this.lengthFromSource = lengthFromSource;
    }

    @Override
    public int compare(Integer node1, Integer node2) {
        return lengthFromSource[node1] - lengthFromSource[node2];
    }
}