class LengthBetweenNodesComparator implements Comparator<Integer> {
    private int[] lengthFromSource;
    private int[] distanceTo;

    LengthBetweenNodesComparator(int[] distanceTo, int[] lengthFromSource) {
        this.distanceTo = distanceTo;
        this.lengthFromSource = lengthFromSource;
    }

    @Override
    public int compare(Integer node1, Integer node2) {
        return distanceTo[node1] + lengthFromSource[node1] - distanceTo[node2] - lengthFromSource[node2];
    }
}