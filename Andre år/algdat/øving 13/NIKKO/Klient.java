class Klient {
    public static void main(String[] args) {
        FilLeser m = new FilLeser();

        m.lesNoderOgKanter("nordenNoder.txt", "nordenKanter.txt");
        System.out.println("Lest ferdig");
        // m.lesNoderOgKanter("islandNoder.txt", "islandKanter.txt");

        Graf.astar(Graf.node[5108028], Graf.node[5709083], false, "output.csv"); // Gjemnes-Kårvåg
        // A*

        // Graf.astar(Graf.node[14465], Graf.node[14455], true); //
        // Reykjavík–Hafnarfjörður A*

        // Graf.astar(Graf.node[30236], Graf.node[94114], true); // Oslo til Bergen

        // Graf.astar(Graf.node[2058549], Graf.node[1051859], false); // Kristiansund
        // til Helsinki
        // Graf.astar(Graf.node[2058549], Graf.node[1051859], true); // Samme med
        // djikstra

        // Graf.astar(Graf.node[3237536], Graf.node[1881040], false); // Test

    }
}