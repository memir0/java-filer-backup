import java.io.*;
import java.util.ArrayList;

class FilSkriver {
    public void skrivKordinater(ArrayList<Pair> kordinater, String outputFil) {
        try (FileWriter writer = new FileWriter(outputFil);
            BufferedWriter bw = new BufferedWriter(writer)) {

            bw.write("\"latitude\";\"longitude\";\"name\";\"place\"");

            for (int i = 0; i < kordinater.size(); i++) {
                String linje = kordinater.get(i).breddegrad + ";" + kordinater.get(i).lengdegrad + ";" + "" + ";" + "";
                bw.newLine();
                bw.write(linje);
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }
}