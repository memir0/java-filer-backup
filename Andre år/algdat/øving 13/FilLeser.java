import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

class FilLeser {

    public static String[] hsplit(String linje, int antall) {
        int j = 0;
        int lengde = linje.length();
        String[] felt = new String[antall];
        for (int i = 0; i < antall; ++i) {
            // Hopp over innledende blanke, finn starten på ordet
            while (linje.charAt(j) <= ' ')
                ++j;
            int ordstart = j;
            // Finn slutten på ordet, hopp over ikke-blanke
            while (j < lengde && linje.charAt(j) > ' ')
                ++j;
            felt[i] = linje.substring(ordstart, j);
        }
        return felt;
    }

    public Graf lesNoderOgKanter(String filnavnNoder, String filnavnKanter) {
        Graf graf = new Graf();
        try (BufferedReader br = new BufferedReader(new FileReader(filnavnNoder))) {
            int n = Integer.parseInt(br.readLine().replace(" ", ""));
            graf.N = n;
            graf.node = new Node[graf.N];

            for (int i = 0; i < n; i++) {
                // process the line.
                String[] data = hsplit(br.readLine(), 3);
                int id = Integer.parseInt(data[0]);
                graf.node[id] = new Node();
                graf.node[id].id = id;
                graf.node[id].breddegrad = Double.parseDouble(data[1]);
                graf.node[id].lengdegrad = Double.parseDouble(data[2]);
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }

        try (BufferedReader br = new BufferedReader(new FileReader(filnavnKanter))) {
            int n = Integer.parseInt(br.readLine().replace(" ", ""));
            graf.K = n;
            for (int i = 0; i < n; i++) {
                // process the line.
                String[] data = hsplit(br.readLine(), 5);
                int fra = Integer.parseInt(data[0]);
                int til = Integer.parseInt(data[1]);
                int vekt = Integer.parseInt(data[2]);
                VKant k = new VKant((VKant) graf.node[fra].kant1, graf.node[til], vekt);
                graf.node[fra].kant1 = k;
            }

        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        return graf;
    }
}