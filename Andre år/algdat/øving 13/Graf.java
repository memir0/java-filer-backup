import java.util.PriorityQueue;
import java.util.ArrayList;

class Graf {
    int N, K;
    Node[] node;
    int sjekket = 0;

    ArrayList<Pair> astar(Node startNode, Node maalNode, boolean dijkstra) {
        //PriorityQueue<Node> priko = new PriorityQueue<>(N, new NodeComparator());
        PriorityQueue<Node> priko = new PriorityQueue<>(N, (o1, o2) -> o1.d.prioritet - o2.d.prioritet);
        startNode.d = new Forgj();
        startNode.d.distanseTilStart = 0;
        priko.add(startNode);

        while (!priko.isEmpty()) {
            Node curr = priko.poll();
            sjekket++;
            if (curr == maalNode) {
                return skrivPath(maalNode, startNode, dijkstra); // Vi har nådd målnoden og må jobbe oss tilbake til start
            }
            for (VKant k = (VKant) curr.kant1; k != null; k = (VKant) k.neste) {
                Node n = k.til;
                if (curr.d != null && n.d != null
                        && curr.d.distanseTilStart + k.vekt >= n.d.distanseTilStart)
                    continue; // Not a better match
                n.d = new Forgj();
                n.d.forgj = curr;
                n.d.kjoretid = curr.d.kjoretid + k.vekt;
                n.d.distanseTilStart = curr.d.distanseTilStart + k.vekt;
                n.d.prioritet = curr.d.distanseTilStart + (dijkstra ? k.vekt + helgeAvstand(curr, maalNode) : helgeAvstand(curr, maalNode));
                priko.remove(n);
                priko.add(n);
            }

        }
        return null;
    }

    public double forventetAvstand(Node start, Node goal) {
        return (2 * 6371 
                * Math.asin(Math.sqrt(Math.sin((Math.toRadians(start.breddegrad) - Math.toRadians(goal.breddegrad)) / 2)
                * Math.sin((Math.toRadians(start.breddegrad) - Math.toRadians(goal.breddegrad)) / 2)
                + Math.cos(Math.toRadians(start.breddegrad)) * Math.cos(Math.toRadians(goal.breddegrad))
                * Math.sin((Math.toRadians(start.lengdegrad) - Math.toRadians(goal.lengdegrad)) / 2)
                * Math.sin((Math.toRadians(start.lengdegrad) - Math.toRadians(goal.lengdegrad)) / 2))));
    }

    public static int helgeAvstand(Node n1, Node n2) {
        double sin_bredde = Math.sin((n1.breddegrad - n2.breddegrad) / 2.0);
        double sin_lengde = Math.sin((n1.lengdegrad - n2.lengdegrad) / 2.0);
        return (int) (35285538.46153846153846153846 * Math.asin(Math.sqrt(sin_bredde * sin_bredde + n1.cosBredde * n2.cosBredde * sin_lengde * sin_lengde)));
    }

    private ArrayList<Pair> skrivPath(Node n, Node s, boolean dijkstra) {
        ArrayList<Pair> kordinater = new ArrayList<Pair>();
        Node m = n;
        while (m != s) {
            //System.out.println(m.breddegrad + "," + m.lengdegrad); // Skriv ut alle noder baklengs
            Pair tempPair = new Pair(m.breddegrad, m.lengdegrad);
            kordinater.add(tempPair);
            m = ((Forgj) m.d).forgj;
        }
        String type = (dijkstra) ? "Dijkstra: " : "A*: ";
        System.out.println(type);
        System.out.println("Startet på: " + s.breddegrad + "," + s.lengdegrad); // Skriv ut startnode
        System.out.println("Antall sjekkede noder: " + sjekket);
        int totSekunder = ((Forgj) n.d).kjoretid / 100;
        int timer = totSekunder / 3600;
        int minutter = (totSekunder % 3600) / 60;
        int sekunder = (totSekunder % 3600) % 60;
        System.out.println("Kjøretid: " + timer + " timer, " + minutter + " minutter og " + sekunder + " sekunder");
        return kordinater;
    }
}