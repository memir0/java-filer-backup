import java.util.ArrayList;

class Klient {
    public static void main(String[] args) {
        FilLeser fl= new FilLeser();
        FilSkriver fs = new FilSkriver();
           
        // Island
        System.out.println("Island:");
        Graf graf = fl.lesNoderOgKanter("islandNoder.txt", "islandKanter.txt");

        // A*
        long startTime = System.nanoTime();
        //ArrayList<Pair> kordinater = graf.astar(graf.node[14416], graf.node[3023]); // Reykjavík–Hafnarfjörður A*
        ArrayList<Pair> kordinater = graf.astar(graf.node[14416], graf.node[3023], false);
        System.out.println("Algoritme Kjøretid: " + (System.nanoTime()-startTime) + "\n");
        fs.skrivKordinater(kordinater, "output.csv");
        
        graf = fl.lesNoderOgKanter("islandNoder.txt", "islandKanter.txt");

        // dijkstra
        startTime = System.nanoTime();
        kordinater = graf.astar(graf.node[14416], graf.node[3023], true); // Reykjavík–Hafnarfjörður A*
        System.out.println("Algoritme Kjøretid: " + (System.nanoTime()-startTime));
        fs.skrivKordinater(kordinater, "output2.csv");

        
        // Norden
        System.out.println("\nNorden:");
        graf = fl.lesNoderOgKanter("nordenNoder.txt", "nordenKanter.txt");

        // A*
        startTime = System.nanoTime();
        kordinater = graf.astar(graf.node[5709083], graf.node[5108028], false); // Gjemnes-Kårvåg
        System.out.println("Algoritme Kjøretid: " + (System.nanoTime()-startTime) + "\n");
        fs.skrivKordinater(kordinater, "output3.csv");
        
        // djikstra
        startTime = System.nanoTime();
        kordinater = graf.astar(graf.node[5709083], graf.node[5108028], true); // Gjemnes-Kårvåg
        System.out.println("Algoritme Kjøretid: " + (System.nanoTime()-startTime));
        fs.skrivKordinater(kordinater, "output4.csv");

        /* 
            Ubrukte veier:
            graf.astar(graf.node[30236], graf.node[94114]); // Oslo til Bergen
            graf.astar(graf.node[2058549], graf.node[1051859]); // Kristiansund til Helsinki
            graf.astar(graf.node[5108028], graf.node[5709083]); // Gjemnes-Kårvåg
        */
    }
}