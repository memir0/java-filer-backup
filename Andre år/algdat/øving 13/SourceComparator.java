import java.util.*; 
class SourceComparator implements Comparator<Node>{ 
    public int compare(Node node1, Node node2) { 
        return node1.d.prioritet - node2.d.prioritet;
    } 
} 