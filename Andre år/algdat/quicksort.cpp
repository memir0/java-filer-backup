#include <cstdlib> 
#include <ctime>
#include <time.h> 
#include <chrono>
#include <vector>
#include <iostream>

using namespace std;

vector<int> sortThis;

int sumIsEquals(){
    int returTall = 0;
    for(int i = 0; i < sortThis.size(); i++){
        returTall = (returTall+sortThis[i])%1000007;
    }
    return returTall;
}

bool isSorted(){
    for(int i = 1; i < sortThis.size(); i++){
        if(sortThis[i-1] > sortThis[i]){
            cout << i-1 << ": " << sortThis[i-1] << " " << i << ": " << sortThis[i] << endl;
            return false;
        }
    }
    return true;
}

bool isSorted(int start, int slutt){
    for(int i = start+1; i < slutt; i++){
        if(sortThis[i-1] > sortThis[i]){
            return false;
        }
    }
    return true;
}

void bubblesort(int start, int slutt){
    int tempHolder = 0;
    for(int i = start; i < slutt; i++){
        for(int j = i; j < slutt; j++){
            if(sortThis[j] < sortThis[i]){
                tempHolder = sortThis[j];
                sortThis[j] = sortThis[i];
                sortThis[i] = tempHolder;
            }
        }
    }
}

void quicksort(int start, int slutt){
    //if (start > 0 && slutt < sortThis.size()-1 && sortThis[start-1] == sortThis[slutt+1]) return;
    if(slutt-start <= 10) bubblesort(start,slutt);
    else{
        int delingstall[3] = {sortThis[start], sortThis[start+(slutt-start)/2], sortThis[slutt-1]};

        // Sjekker om delen er sortert
        if(delingstall[0] <= delingstall[1] && delingstall[1] <= delingstall[2]){
            if(isSorted(start, slutt)) return;
        }
        else{
            int indekser[3] = {start, start+(slutt-start)/2, slutt-1};

            // Sorter delingstallene
            int tempHolder = 0;
            for(int i = 0; i < 3; i++){
                for(int j = i; j < 3; j++){
                    if(delingstall[j] < delingstall[i]){
                        tempHolder = delingstall[j];
                        delingstall[j] = delingstall[i];
                        sortThis[indekser[j]] = delingstall[i];
                        delingstall[i] = tempHolder;
                        sortThis[indekser[i]] = tempHolder;
                    }
                }
            }
        }
        

        int amountOfSmaller = 1;

        // Fjerner start og slutt fordi vi vet at de står rett
        for(int i = start+1; i < slutt-1; i++){
            if(sortThis[i] < delingstall[1]){
                int temp = sortThis[start+amountOfSmaller];
                sortThis[start+amountOfSmaller] = sortThis[i];
                sortThis[i] = temp;
                amountOfSmaller++;
            }|
        }
        quicksort(start, start+amountOfSmaller);
        quicksort(start+amountOfSmaller, slutt);
    }
}

int main(){
    // Fyll tabell med random tall
    srand((unsigned)time(0)); 

    for(int i = 0; i < 10000000; i++){
        sortThis.push_back((rand()%100000)+1);
    }

    // summer
    int size = sumIsEquals();

    // Klokkestart
    auto t1 = std::chrono::high_resolution_clock::now();

    // Kjør quicksort
    quicksort(0, sortThis.size());

    // Klokkeslutt
    auto t2 = std::chrono::high_resolution_clock::now();

    if(size == sumIsEquals()){
        cout << "summen er lik" << endl;
    }

    // Sjekk quicksort
    cout << endl << isSorted() << endl;
    cout << "Funksjonen brukte "
              << std::chrono::duration_cast<std::chrono::milliseconds>(t2-t1).count()
              << " millisekunder\n";

    return 0;
}