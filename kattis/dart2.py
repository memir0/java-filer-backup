#inn = int(input())
for test in range(181):
    print(str(test) + ": ")
    inn = test
    if inn > 120:
        rest = inn - 120
        if rest <= 20:
            print("triple 20")
            print("triple 20")
            print("single " + str(rest))
        elif rest <= 40 and rest%2 == 0:
            print("triple 20")
            print("triple 20")
            thirdNumber = int(rest/2)
            print("double " + str(thirdNumber))
        elif rest%3 == 0:
            print("triple 20")
            print("triple 20")
            thirdNumber = int(rest/3)
            print("triple " + str(thirdNumber))
        else:
            print("impossible")
            
    else:
        output = []
        while inn != 0:
            trips = 60
            index = 6
            while index > 0:
                if inn - trips > 0:
                    diff = int(trips/3)
                    inn -= trips
                    output.append("triple " + str(diff))
                else:
                    index -= 1
                    trips -= 3

            for i in range(40, 19, -1):
                if i%2 == 0 and inn - i > 0:
                    diff = int(i/2)
                    inn -= i
                    output.append("double " + str(diff))
                elif i%3 == 0 and inn - i > 0:
                    diff = int(i/3)
                    inn -= i
                    output.append("double " + str(diff))
            
            if inn <= 20:
                output.append("single " + str(inn))
                inn = 0
        
        if len(output) > 3:
            print("impossible")
        else:
            for i in output:
                print(i)