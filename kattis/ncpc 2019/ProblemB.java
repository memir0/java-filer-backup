import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

class ProblemB {
    public static void main(String[] args) {
        //initialize
        Scanner in = new Scanner(System.in);
        int t = in.nextInt();
        in.nextLine();
        int[] ans = new int[t];
        int[][] arr = new int[3][2];
        int maxIx=-1;
        int maxIy=-1;
        int maxIx1=-1;
        int maxIy2=-1;
        int max = 0;
        int prevMaxI = 0;
        for(int i = 0; i < t; i++){
            for (int x = 0; x < 3; x++) {
                for (int y = 0; y < 2; y++) {
                    int tall = in.nextInt();
                    arr[x][y] = tall;
                    if(tall > max) {
                        maxIx1 = maxIx;
                        maxIy1 = maxIy;
                        maxIx = x;
                        maxIy = y;
                        max = tall;
                    }
                }
            }
        }

        //Code:

        int bredde = max;
        int høyde = arr[maxIx][(maxIx+1)%2];
        høyde += arr[maxIx1][(maxIx1+1)%2];


        //print
        for (int var : ans) {
            System.out.println(var);
        }
    }
}
    