import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Scanner;

class ProblemE {
    public static void main(String[] args){
        boolean team = false;
        Scanner sc = new Scanner(System.in);

        ArrayList<String> team1 = new ArrayList<String>();
        ArrayList<String> team2 = new ArrayList<String>();

        String regle = sc.nextLine();
        int antallHopp = regle.split(" ").length;

        int N = Integer.parseInt(sc.nextLine());

        Node tempNode = null;

        // Sett inn i linked list og gjør litt arbied på vei
        int teller = 1;

        for(int i = 0; i < N; i++){
            String tempNavn = sc.nextLine();

            if(teller == antallHopp){
                if(!team){
                    team1.add(tempNavn);
                    team = true;
                }
                else {
                    team2.add(tempNavn);
                    team = false;
                }
                teller = 1;
            }

            else {
                if(tempNode == null){
                    tempNode = new Node(tempNavn);
                    tempNode.nesteNode = tempNode;
                }
                else {
                    Node nyNode = new Node(tempNode.nesteNode, tempNavn);
                    tempNode.nesteNode = nyNode;
                    tempNode = nyNode;
                }
                teller++;
            }
        }
        teller++;
        Node forrigeNode = tempNode.nesteNode;
        Node nesteNode = forrigeNode.nesteNode;

        while(nesteNode != forrigeNode){
            String tempNavn = nesteNode.navn;
            if(teller == antallHopp){
                if(!team){
                    team1.add(tempNavn);
                    team = true;
                }
                else {
                    team2.add(tempNavn);
                    team = false;
                }
                forrigeNode.nesteNode = nesteNode.nesteNode;
                nesteNode = forrigeNode.nesteNode;
                teller = 0;
            }
            else {
                forrigeNode = nesteNode;
                nesteNode = nesteNode.nesteNode;
            }
            teller++;
        }

        if(!team){
            team1.add(forrigeNode.navn);
        }
        else {
            team2.add(forrigeNode.navn);
        }

        System.out.println(team1.size());
        for(int i = 0; i < team1.size(); i++) System.out.println(team1.get(i));

        System.out.println(team2.size());
        for(int i = 0; i < team2.size(); i++) System.out.println(team2.get(i));

        sc.close();
    }
}

class Node {
    Node nesteNode;
    String navn;

    Node(Node hode, String navn){
        nesteNode = hode;
        this.navn = navn;
    }
    Node(String navn){
        this.navn = navn;
    }
}

// main
