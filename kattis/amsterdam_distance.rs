use std::io;
use std::sync::{Arc, Mutex};

#[derive(Clone)]
struct Node {
    neighbors: Arc<Mutex<Vec<Node>>>
}

// Dijkstra implementation
fn main(){
    // Dimensions
    let mut input = String::new();
    let _result = io::stdin().read_line(&mut input);
    let input_split_whitespace = input.split(" ");
    let input_split = input_split_whitespace.collect::<Vec<&str>>();
    let radius: f32 = input_split[2].parse().unwrap();

    // Target
    let target_split_whitespace = input.split(" ");
    let target_split = target_split_whitespace.collect::<Vec<&str>>();
    let goal_ring: f32 = target_split[3].parse().unwrap();

    let mut nodes: Vec<Vec<Node>> = vec![];

    let mut genisis_node = Node{neighbors: Arc::new(Mutex::new(vec![]))};
    let mut node_line: Vec<Node> = vec![];
    node_line.push(genisis_node.clone());
    
    let mut previous_node = genisis_node.clone();
    // first line
    for j in 0..input_split[1].parse().unwrap() {
        let mut new_node = Node{neighbors: Arc::new(Mutex::new(vec![]))};
        new_node.neighbors.lock().unwrap().push(previous_node.clone());
        previous_node.neighbors.lock().unwrap().push(new_node.clone());
        previous_node = new_node;
    }
    nodes.push(node_line);

    for i in 1..input_split[0].parse().unwrap(){
        previous_node = genisis_node;
        node_line = vec![];
        for j in 1..input_split[1].parse().unwrap(){
            let mut new_node = Node{neighbors: Arc::new(Mutex::new(vec![]))};
            new_node.neighbors.lock().unwrap().push(previous_node);
            previous_node.neighbors.lock().unwrap().push(new_node);
            nodes[i-1][j].neighbors.lock().unwrap().push(new_node); // Add self as neighboor to left node
        }
        nodes.push(node_line);
    }
}