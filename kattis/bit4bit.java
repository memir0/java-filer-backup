import java.util.Scanner;
import java.util.HashMap;
import java.util.ArrayList;
import java.math.BigInteger;

class Node {
	Node(int parrent1, int parrent2, BigInteger length) {
		this.parrent1 = parrent1;
		this.parrent2 = parrent2;
		this.length = length;
		type = true;
	}
	Node(int parrent1, BigInteger length, int indexToReplace, int valueToReplace) {
		this.parrent1 = parrent1;
		this.length = length;
		this.indexToReplace = indexToReplace;
		this.valueToReplace = valueToReplace;
		type = false;

	}
	Node() {
		type = false;
		length = new BigInteger(1);
	}

	public boolean type;
	public int parrent1;
	public int parrent2;
	public BigInteger length;
	public BigInteger indexToReplace;
	public int valueToReplace;
}

class bit4bit{
    static ArrayList<Node> nodes;
    static int m;
    static BigInteger startIndex;
    static BigInteger stopIndex;
    static HashMap<Integer, Boolean> timesAlreadyChanged;

    private static int addReplacers(int nodeIndex, int offset) {
        if (nodeIndex != 0) {
            if (nodes[nodeIndex].type) { // Copy
                if (nodes[nodes[nodeIndex].parrent1].length < stopIndex) {
                    return (addReplacers(nodes[nodeIndex].parrent1, offset) + addReplacers(nodes[nodeIndex].parrent2, offset + nodes[nodes[nodeIndex].parrent1].length)) % 1000000007;
                }
                else {
                    return addReplacers(nodes[nodeIndex].parrent1, offset); // if parrent 1 encompases the entire scope, parrent 2 is ignored
                }
            }
            else { // Replace
                if (nodes[nodeIndex].indexToReplace + offset >= startIndex && stopIndex >= nodes[nodeIndex].indexToReplace + offset && timesAlreadyChanged.find(nodes[nodeIndex].indexToReplace + offset) == timesAlreadyChanged.end()) { // index within bounds and index not already writen to
                    timesAlreadyChanged[nodes[nodeIndex].indexToReplace + offset] = true;
                    return (nodes[nodeIndex].valueToReplace - m + addReplacers(nodes[nodeIndex].parrent1, offset)) % 1000000007;
                }
                else { // if the replace index is out of bounds, it will simply move on to its parrent
                    return addReplacers(nodes[nodeIndex].parrent1, offset);
                }
            }
        }
        else { //if genisis node
            return 0;
        }
    }

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int P = scan.nextInt();
        int Q = scan.nextInt();
        m = scan.nextInt();

        nodes.add(Node());

        for (int i = 1; i <= P; i++) {
            String command = scan.next();

            if (command == "replace") { //Replace
                int playlistToCopy = scan.nextInt();
                int indexToReplace = scan.nextInt();
                int newTime = scan.nextInt();

                nodes.add(Node(playlistToCopy, nodes[playlistToCopy].length, indexToReplace, newTime));
            }
            else { //Copy
                int playlist1 = scan.nextInt();
                int playlist2 = scan.nextInt();

                nodes.add(Node(playlist1, playlist2, nodes[playlist1].length + nodes[playlist2].length));
            }
        }

        for (int i = 0; i < Q; i++) {
            timesAlreadyChanged.clear();

            int playlistToCheck = scan.nextInt();
            startIndex = scan.nextBigInteger();
            stopIndex = scan.nextBigInteger();

            int totalPlayTime = (m*(stopIndex - startIndex + 1) % 1000000007);

            totalPlayTime = (totalPlayTime + addReplacers(playlistToCheck, 0)) % 1000000007;

            System.out.println(totalPlayTime);
        }
    }
}










/*public class bit4bit {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int P = scan.nextInt();
        int Q = scan.nextInt();
        int m = scan.nextInt();

        int[][] playlists = new int[P+1][];

        playlists[0] = new int[1];

        playlists[0][0] = m;

        scan.nextLine();

        for(int i = 1; i < P+1; i++){

            String command = scan.next();

            if(command.equals("replace")){
                int playlistToCopy= scan.nextInt();
                int indexToReplace = scan.nextInt();
                int newTime = scan.nextInt();

                playlists[i] = new int[playlists[playlistToCopy].length];

                for(int j = 0; j < playlists[playlistToCopy].length; j++){
                    playlists[i][j] = playlists[playlistToCopy][j];
                }

                playlists[i][indexToReplace] = newTime;
                scan.nextLine();
            }
            else {
                int playlist1 = scan.nextInt();
                int playlist2 = scan.nextInt();

                int newLength = (playlists[playlist1].length+playlists[playlist2].length);

                playlists[i] = new int[newLength];

                int playlist2Counter = 0;

                for(int j = 0; j < newLength; j++){
                    if(j < playlists[playlist1].length){
                        playlists[i][j] = playlists[playlist1][j];
                    }else{
                        int replaceShit = playlists[playlist2][playlist2Counter];
                        playlists[i][j] = playlists[playlist2][playlist2Counter];
                        playlist2Counter++;
                    }
                }
                scan.nextLine();
            }
        }

        int[] totalTime = new int[Q];

        for(int i = 0; i < Q; i++){
            int playlistToCheck = scan.nextInt();
            int startIndex = scan.nextInt();
            int stopIndex = scan.nextInt();

            totalTime[i] = 0;

            for(int j = startIndex; j <= stopIndex; j++){
                totalTime[i] += playlists[playlistToCheck][j];
            }

            scan.nextLine();
        }

        for(int i = 0; i < Q; i++){
            for(int j = 0; j < playlists[i].length; j++){
                System.out.print(playlists[i][j]);
            }
            System.out.println();
            System.out.println(totalTime[i]);
        }
    }
}
*/