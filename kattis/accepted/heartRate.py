import sys

N = int(input()) 
for i in range(N):
    inn = input().split(" ")
    sys.stdout.write('{:.4f}'.format(round(60*(int(inn[0])-1)/float(inn[1]), 4)))
    sys.stdout.write(' ')
    sys.stdout.write('{:.4f}'.format(round(int(inn[0])*60/float(inn[1]), 4)))
    sys.stdout.write(' ')
    sys.stdout.write('{:.4f}'.format(round(60*(int(inn[0])+1)/float(inn[1]), 4)))
    sys.stdout.write('\n')