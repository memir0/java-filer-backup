x = int(input())

output = 2

for i in range(x):
    output += pow(2, i)

output = pow(output, 2)

print(output)