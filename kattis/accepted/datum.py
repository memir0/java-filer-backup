dates = ["Thursday", "Friday", "Saturday", "Sunday", "Monday", "Tuesday", "Wednesday"]

inn = input().split()

day = int(inn[0])-1

month = int(inn[1])

for i in range(month-1):
    if(i == 1):
        day += 28
    elif(i < 7):
        if(i%2 == 0):
            day += 31
        else:
            day += 30
    else:
        if(i%2 == 0):
            day += 30
        else:
            day += 31

print(dates[day%7])