use std::io;

fn main() {
    let mut input = String::new();
    let _result = io::stdin().read_line(&mut input);
    let mut j: u32 = input.trim().parse().unwrap();
    let mut reverse_number = 0;
    while j > 0{
        reverse_number *= 2;
        reverse_number += j%2;
        j /= 2;
    }
    println!("{}", reverse_number);
}