inn = input().split(" ")

A = []
D = []
H = []

sumPokemon = int(inn[1])*3

for i in range(int(inn[0])):
    stats = input().split(" ")
    A.append([int(stats[0]), i])
    D.append([int(stats[1]), i])
    H.append([int(stats[2]), i])

A.sort(reverse = True)
D.sort(reverse = True)
H.sort(reverse = True)

team = []

for i in range(int(inn[1])):
    if(A[i][1] not in team):
        team.append(A[i][1])
    if(D[i][1] not in team):
        team.append(D[i][1])
    if(H[i][1] not in team):
        team.append(H[i][1])

print(len(team))