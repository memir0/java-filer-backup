import java.util.Scanner;
import java.util.ArrayList;

class ICPCawards{
    public static void main(String[] args){
        ArrayList<String> winningSchools = new ArrayList<String>();
        ArrayList<String> winningTeams = new ArrayList<String>();

        Scanner scanner = new Scanner(System.in);

        int N = scanner.nextInt();

        for(int i = 0; i < N; i++){
            String tempSchool = scanner.next();
            String tempTeam = scanner.next();
            boolean alreadyWon = false;

            for(int j = 0; j < winningSchools.size(); j++){
                if(winningSchools.get(j).equals(tempSchool)) alreadyWon = true;
            }

            if(!alreadyWon){ 
                winningTeams.add(tempTeam);
                winningSchools.add(tempSchool);
            }
            alreadyWon = false;
        }
        for(int i = 0; i < 12; i++){
            System.out.println(winningSchools.get(i) + " " + winningTeams.get(i));
        }
    }
}