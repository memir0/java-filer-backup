import java.util.Scanner;

public class Printer {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int antall = scanner.nextInt();
        int antallPrintere;

        if(antall%2 != 0){
            antallPrintere = antall/2+1;
        } else {
            antallPrintere = antall/2;
        }       
        
        int dager = 1;
        int temp = 1;
        
        while(temp < antallPrintere){
            temp *= 2;
            dager++;
        }

        if(antallPrintere < antall){
            dager++;
        }

        System.out.println(dager);
    }
}