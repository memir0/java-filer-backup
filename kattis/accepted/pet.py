highscore = 0
highscoreOwner = 0

for i in range(5):
    score = 0
    inn = input().split(" ")
    for j in range(len(inn)):
        score += int(inn[j])
    if(score > highscore):
        highscore = score
        highscoreOwner = i + 1

output = str(highscoreOwner) + " " + str(highscore)