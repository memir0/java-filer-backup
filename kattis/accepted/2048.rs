use std::io;

fn main(){
    let mut grid: Vec<Vec<u32>> = vec![];
    for _i in 0..5 {
        let mut temp_vec = vec![];
        let mut input_string = String::new();
        io::stdin().read_line(&mut input_string).expect("failed to read from stdin");
        for number in input_string.split_whitespace() {
            temp_vec.push(number.parse().unwrap());
        }
        grid.push(temp_vec);
    }
    
    let direction = grid[4][0];

    if direction == 0 {
        for i in 0..4 {
            for j in 0..3 {
                for x in 1..(4-j){
                    if grid[i][j+x] != 0 {
                        if grid[i][j] == grid[i][j+x]{
                            grid[i][j] *= 2;
                            grid[i][j+x] = 0;
                            break;
                        }
                        else if grid[i][j] == 0 {
                            grid[i][j] = grid[i][j+x];
                            grid[i][j+x] = 0;
                        }
                        else {break;}
                    }
                }
            }
        }
    }
    else if direction == 1 {
        for i in 0..4 {
            for j in 0..3 {
                for x in 1..(4-j){
                    if grid[j+x][i] != 0 {
                        if grid[j][i] == grid[j+x][i]{
                            grid[j][i] *= 2;
                            grid[j+x][i] = 0;
                            break;
                        }
                        else if grid[j][i] == 0 {
                            grid[j][i] = grid[j+x][i];
                            grid[j+x][i] = 0;
                        }
                        else {break;}
                    }
                }
            }
        }
    }
    else if direction == 2 {
        for i in 0..4 {
            for j in (1..4).rev() {
                for x in 1..(j+1){
                    if grid[i][j-x] != 0 {
                        if grid[i][j] == grid[i][j-x]{
                            grid[i][j] *= 2;
                            grid[i][j-x] = 0;
                            break;
                        }
                        else if grid[i][j] == 0 {
                            grid[i][j] = grid[i][j-x];
                            grid[i][j-x] = 0;
                        }
                        else {break;}
                    }
                }
            }
        }
    }
    else if direction == 3 {
        for i in 0..4 {
            for j in (1..4).rev() {
                for x in 1..(j+1){
                    if grid[j-x][i] != 0 {
                        if grid[j][i] == grid[j-x][i]{
                            grid[j][i] *= 2;
                            grid[j-x][i] = 0;
                            break;
                        }
                        else if grid[j][i] == 0 {
                            grid[j][i] = grid[j-x][i];
                            grid[j-x][i] = 0;
                        }
                        else {break;}
                    }
                }
            }
        }
    }

    for i in 0..4 {
        for j in 0..4 {
            print!("{} ", grid[i][j]);
        }
        println!();
    }
}