import sys

time = input().split(" ")

if(int(time[1]) >= 45):
    sys.stdout.write(time[0])
    sys.stdout.write(" ")
    sys.stdout.write(str(int(time[1])-45))
else:
    if(int(time[0]) == 0):
        sys.stdout.write("23")
        sys.stdout.write(" ")
        sys.stdout.write(str(int(time[1])+60-45))
    else:
        sys.stdout.write(str(int(time[0])-1))
        sys.stdout.write(" ")
        sys.stdout.write(str(int(time[1])+60-45))