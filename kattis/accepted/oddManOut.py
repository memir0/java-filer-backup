N = int(input())

lonley = ""

for i in range(N):
    totalGuests = int(input())
    inn = input().split(" ")

    lonley = inn[0]
    inn.pop(0)

    keepGoing = True

    while(keepGoing):
        try:
            inn.remove(lonley)
        except ValueError:
            keepGoing = False
        else:
            lonley = inn[0]
            inn.pop(0)
        
    print("Case #" + str(i+1) + ": " + lonley)