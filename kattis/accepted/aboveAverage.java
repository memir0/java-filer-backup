import java.util.Scanner;
import java.util.Arrays;

public class aboveAverage {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        
        int N = scanner.nextInt(), sum = 0;

        for(int i = 0; i < N; i++){
            int B = scanner.nextInt();
            double[] temp = new double[B];

            for(int j = 0; j < B; j++){
                temp[j] = scanner.nextInt();
                sum += temp[j];
            }
            double average = sum/B;
            double aboveAverage = 0;

            for(int j = 0; j < B; j++){
                if(temp[j] > average) aboveAverage++;
            }

            int output = (int)(1000000*aboveAverage/B);

            int temp2 = output;

            while(temp2/10 > 10) temp2 = temp2/10;

            if(temp2 > 10) temp2 = temp2/10;

            if(temp2 >= 5) output++;

            System.out.printf("%.3f",(double)(output)/10000d);
            System.out.print("%");
            System.out.println();
            sum = 0;
        }
    }
}