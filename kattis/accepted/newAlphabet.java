import java.util.Scanner;
import java.util.Arrays;
import java.util.HashMap;

public class newAlphabet {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        
        String sentence = scanner.nextLine();
        sentence = sentence.toLowerCase();

        HashMap<Integer, String> hmap = new HashMap<Integer, String>();

        String[] newLang = {"@", "8", "(", "|)", "3", "#", "6", "[-]", 
                            "|", "_|", "|<", "1", "[]\\/[]", "[]\\[]", 
                            "0", "|D", "(,)", "|Z", "$", "']['", "|_|",
                            "\\/", "\\/\\/", "}{", "`/", "2"};

        for(int i = 0; i < 26; i++){
            hmap.put(97+i, newLang[i]);
        }

        int temp;

        for(int i = 0; i < sentence.length(); i++){
            temp = (int)(sentence.charAt(i));
            if(temp > 96 && temp < 123) System.out.print(hmap.get(temp));
            else System.out.print(sentence.charAt(i));
        }
    }
}