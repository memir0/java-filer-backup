import java.util.Scanner;

public class tjuefortiate {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int[][] tall = new int[4][4];
		
		for(int j = 0; j < 4; j++){
			for(int i = 0; i < 4; i++){
				tall[j][i] = scanner.nextInt();
			}
		}

		int move = scanner.nextInt();

		if(move == 0){
			for(int j = 0; j < 4; j++){
				for(int i = 0; i < 3; i++){
					if(tall[j][i] == tall[j][i+1]){
						tall[j][i] *= 2;
						tall[j][i+1] = 0;
						i++;
					}
					else if(tall[j][i+1] == 0){
						if(i+2 < 4){
							if(tall[j][i] == tall[j][i+2]){
								tall[j][i] *= 2;
								tall[j][i+2] = 0;
								i++;
							}
							else if(tall[j][i+2] == 0){
								if(i+3 < 4){
									if(tall[j][i] == tall[j][i+3]){
										tall[j][i] *= 2;
										tall[j][i+3] = 0;
										i++;
									}
								}
							}
						}
					}
				}
			}
			for(int j = 0; j < 4; j++){
				for(int i = 0; i < 4; i++){
					if(tall[j][i] == 0){
						if(i+1 < 4){
							if(tall[j][i+1] == 0){
								if(i+2 < 4){
									if(tall[j][i+2] == 0){
										if(i+3 < 4){
											if(tall[j][i+3] != 0){
												tall[j][i] = tall[j][i+3];
												tall[j][i+3] = 0;
											}
										}
									} else {
										tall[j][i] = tall[j][i+2];
										tall[j][i+2] = 0;
									}
								}
							} else {
								tall[j][i] = tall[j][i+1];
								tall[j][i+1] = 0;
							}
						}
					}
				}
			}
		}
		else if(move == 1){
			for(int i = 0; i < 4; i++){
				for(int j = 0; j < 3; j++){
					if(tall[j][i] == tall[j+1][i]){
						tall[j][i] *= 2;
						tall[j+1][i] = 0;
						j++;
					}
					else if(tall[j+1][i] == 0){
						if(j+2 < 4){
							if(tall[j][i] == tall[j+2][i]){
								tall[j][i] *= 2;
								tall[j+2][i] = 0;
								j++;
							}
							else if(tall[j+2][i] == 0){
								if(j+3 < 4){
									if(tall[j][i] == tall[j+3][i]){
										tall[j][i] *= 2;
										tall[j+3][i] = 0;
										j++;
									}
								}
							}
						}
					}
				}
			}
			for(int j = 0; j < 4; j++){
				for(int i = 0; i < 4; i++){
					if(tall[j][i] == 0){
						if(j+1 < 4){
							if(tall[j+1][i] == 0){
								if(j+2 < 4){
									if(tall[j+2][i] == 0){
										if(j+3 < 4){
											if(tall[j+3][i] != 0){
												tall[j][i] = tall[j+3][i];
												tall[j+3][i] = 0;
											}
										}
									} else {
										tall[j][i] = tall[j+2][i];
										tall[j+2][i] = 0;
									}
								}
							} else {
								tall[j][i] = tall[j+1][i];
								tall[j+1][i] = 0;
							}
						}
					}
				}
			}
		}
		else if(move == 2){
			for(int j = 0; j < 4; j++){
				for(int i = 3; i > 0; i--){
					if(tall[j][i] == tall[j][i-1]){
						tall[j][i] *= 2;
						tall[j][i-1] = 0;
						i--;
					}
					else if(tall[j][i-1] == 0){
						if(i-2 >= 0){
							if(tall[j][i] == tall[j][i-2]){
								tall[j][i] *= 2;
								tall[j][i-2] = 0;
								i--;
							}
							else if(tall[j][i-2] == 0){
								if(i-3 >= 0){
									if(tall[j][i] == tall[j][i-3]){
										tall[j][i] *= 2;
										tall[j][i-3] = 0;
										i--;
									}
								}
							}
						}
					}
				}
			}
			for(int j = 0; j < 4; j++){
				for(int i = 3; i > 0; i--){
					if(tall[j][i] == 0){
						if(i > 0){
							if(tall[j][i-1] == 0){
								if(i-1 > 0){
									if(tall[j][i-2] == 0){
										if(i-2 > 0){
											if(tall[j][i-3] != 0){
												tall[j][i] = tall[j][i-3];
												tall[j][i-3] = 0;
											}
										}
									} else {
										tall[j][i] = tall[j][i-2];
										tall[j][i-2] = 0;
									}
								}
							} else {
								tall[j][i] = tall[j][i-1];
								tall[j][i-1] = 0;
							}
						}
					}
				}
			}
		}
		else{
			for(int i = 0; i < 4; i++){
				for(int j = 3; j > 0; j--){
					if(tall[j][i] == tall[j-1][i]){
						tall[j][i] *= 2;
						tall[j-1][i] = 0;
						j--;
					}
					else if(tall[j-1][i] == 0){
						if(j-2 >= 0){
							if(tall[j][i] == tall[j][i-2]){
								tall[j][i] *= 2;
								tall[j-2][i] = 0;
								j--;
							}
							else if(tall[j][i-2] == 0){
								if(j-3 >= 0){
									if(tall[j][i] == tall[j][i-3]){
										tall[j][i] *= 2;
										tall[j-3][i] = 0;
										j--;
									}
								}
							}
						}
					}
				}
			}
			for(int i = 0; i < 4; i++){
				for(int j = 3; j > 0; j--){
					if(tall[j][i] == 0){
						if(j > 0){
							if(tall[j-1][i] == 0){
								if(j-1 > 0){
									if(tall[j-2][i] == 0){
										if(j-2 > 0){
											if(tall[j-3][i] != 0){
												tall[j][i] = tall[j-3][i];
												tall[j-3][i] = 0;
											}
										}
									} else {
										tall[j][i] = tall[j-2][i];
										tall[j-2][i] = 0;
									}
								}
							} else {
								tall[j][i] = tall[j-1][i];
								tall[j-1][i] = 0;
							}
						}
					}
				}
			}
		}
		for(int j = 0; j < 4; j++){
			for(int i = 0; i < 4; i++){
				System.out.print(tall[j][i] + " ");
			}
			System.out.println();
		}
	}
}