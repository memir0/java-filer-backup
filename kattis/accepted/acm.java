import java.util.Scanner;
import java.util.Arrays;
import java.util.HashMap;

public class acm {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        
        int[][] tries = new int[26][2];
        int sumTime = 0, tempCharCode;

        int time = scanner.nextInt();

        while(time != -1){
            tempCharCode = (int)(scanner.next().charAt(0))-65;

            tries[tempCharCode][0]++;

            if(scanner.next().equals("right")){
                tries[tempCharCode][1] = 1;
                sumTime += time;
            }

            time = scanner.nextInt();
        }

        int sumCorrect = 0;

        for(int i = 0; i < 26; i++){
            if(tries[i][1] == 1){
                sumTime += (tries[i][0]-1)*20;
                sumCorrect++;
            }
        }

        System.out.println(sumCorrect + " " + sumTime);
    }
}