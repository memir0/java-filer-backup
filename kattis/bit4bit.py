class Node:
 def __init__(self, parrent1= 0, parrent2=0, indexToReplace=0,  valueToReplace= 0, isCopy = False, length= 1): 
  self.parrent1 = parrent1
  self.parrent2 = parrent2
  self.length = length
  self.indexToReplace = indexToReplace
  self.valueToReplace = valueToReplace
  self.isCopy = isCopy
  self.totalTime = 0


nodes = []
m = 0
startIndex = 0
stopIndex = 0
timesAlreadyChanged = {}

def addReplacers(nodeIndex, offset):
 if(nodeIndex is not 0):
  if (nodes[nodeIndex].length + offset >= startIndex and stopIndex >= nodes[nodeIndex].length + offset):
   if(nodes[nodeIndex].totalTime is not 0):
    return nodes[nodeIndex].totalTime
   else:
    if (nodes[nodeIndex].isCopy):#Copy
     newOffset = offset + nodes[nodes[nodeIndex].parrent1].length
     if newOffset < stopIndex:
      if nodes[nodes[nodeIndex].parrent1].length+offset < startIndex:
       nodes[nodeIndex].totalTime =  addReplacers(nodes[nodeIndex].parrent2, newOffset)
       return nodes[nodeIndex].totalTime
      else:
       nodes[nodeIndex].totalTime = (addReplacers(nodes[nodeIndex].parrent1, offset) + addReplacers(nodes[nodeIndex].parrent2, newOffset) % 1000000007)
       return nodes[nodeIndex].totalTime
     elif nodes[nodes[nodeIndex].parrent1].length+offset < startIndex:
       nodes[nodeIndex].totalTime = addReplacers(nodes[nodeIndex].parrent2, newOffset)
       return nodes[nodeIndex].totalTime
     else:
       if nodes[nodes[nodeIndex].parrent1].length+offset < startIndex:
         #None of them are within bounds
         return 0
       else:
         nodes[nodeIndex].totalTime =  addReplacers(nodes[nodeIndex].parrent1, offset)
         return nodes[nodeIndex].totalTime
    else:# Replace
     isWithinBounds = False
     if (nodes[nodeIndex].indexToReplace + offset >= startIndex and stopIndex >= nodes[nodeIndex].indexToReplace + offset):
       isWithinBounds = True
     indexIsntAlreadyChanged = not nodes[nodeIndex].indexToReplace + offset in timesAlreadyChanged
     if indexIsntAlreadyChanged:
       timesAlreadyChanged[nodes[nodeIndex].indexToReplace + offset] = True
     if (isWithinBounds and indexIsntAlreadyChanged):# index within bounds and index not already writen to
       nodes[nodeIndex].totalTime = (nodes[nodeIndex].valueToReplace - m + addReplacers(nodes[nodeIndex].parrent1, offset)) % 1000000007
       return nodes[nodeIndex].totalTime
     else:# if the replace index is out of bounds or already changed, it will simply move on to its parrent
       nodes[nodeIndex].totalTime = addReplacers(nodes[nodeIndex].parrent1, offset)
       return nodes[nodeIndex].totalTime
  else: #not within bounds
   if (nodes[nodeIndex].isCopy):#Copy
     newOffset = offset + nodes[nodes[nodeIndex].parrent1].length
     if newOffset < stopIndex:
      if nodes[nodes[nodeIndex].parrent1].length+offset < startIndex:
       return addReplacers(nodes[nodeIndex].parrent2, newOffset)
      else:
       return (addReplacers(nodes[nodeIndex].parrent1, offset) + addReplacers(nodes[nodeIndex].parrent2, newOffset) % 1000000007)
     elif nodes[nodes[nodeIndex].parrent1].length+offset < startIndex:
       return addReplacers(nodes[nodeIndex].parrent2, newOffset)
     else:
       if nodes[nodes[nodeIndex].parrent1].length+offset < startIndex:
         #None of them are within bounds
         return 0
       else:
         return addReplacers(nodes[nodeIndex].parrent1, offset)
   else:# Replace
    isWithinBounds = False
    if (nodes[nodeIndex].indexToReplace + offset >= startIndex and stopIndex >= nodes[nodeIndex].indexToReplace + offset):
      isWithinBounds = True
    indexIsntAlreadyChanged = not nodes[nodeIndex].indexToReplace + offset in timesAlreadyChanged
    if indexIsntAlreadyChanged:
      timesAlreadyChanged[nodes[nodeIndex].indexToReplace + offset] = True
    if (isWithinBounds and indexIsntAlreadyChanged):# index within bounds and index not already writen to
      return (nodes[nodeIndex].valueToReplace - m + addReplacers(nodes[nodeIndex].parrent1, offset)) % 1000000007
    else:# if the replace index is out of bounds or already changed, it will simply move on to its parrent
      return addReplacers(nodes[nodeIndex].parrent1, offset) 
 else:#genesis node
  return 0

P, Q, m = [int(i) for i in input().split()]

nodes.append(Node())

for i in range(1, P+1): 
 command = [i for i in input().split()]

 if (command[0] == "replace"):  #Replace
  playlistToCopy = int(command[1])
  indexToReplace = int(command[2])
  newTime = int(command[3])
  nodes.append(Node(playlistToCopy,0,indexToReplace, newTime, False, nodes[playlistToCopy].length))
  
 else:  #Copy
  playlist1 = int(command[1])
  playlist2 = int(command[2])
  nodes.append(Node(playlist1, playlist2, 0, 0, True, nodes[playlist1].length + nodes[playlist2].length))

for i in range(Q):
 timesAlreadyChanged.clear()
 playlistToCheck, startIndex, stopIndex = [int(x) for x in input().split()]
 totalPlayTime = (m*(stopIndex - startIndex + 1) % 1000000007)
 totalPlayTime = (totalPlayTime+addReplacers(playlistToCheck, 0)) % 1000000007
 print(totalPlayTime)