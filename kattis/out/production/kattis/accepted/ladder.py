import math

inn = input().split(" ")

output = math.ceil(int(inn[0])/math.sin(math.radians(int(inn[1]))))

print(output)