cost = float(input())
lawns = int(input())

totalCost = 0

for i in range(lawns):
    lawnSize = input().split(" ")
    totalCost += float(lawnSize[0])*float(lawnSize[1])*cost

print(totalCost)