for foo in range(4, 21):
    for bar in range(4, 21):
        #inn = input().split(" ")

        inn = [foo, bar]

        numOfSum = dict()

        for i in range(int(inn[0])):
            for j in range(int(inn[1])):
                sum = (i+1)+(j+1)

                if(sum in numOfSum):
                    numOfSum[sum] += 1
                else:
                    numOfSum[sum] = 1

        equalFrequent = []

        mostFrequent = 0

        for i in numOfSum:
            if(numOfSum[i] > mostFrequent):
                mostFrequent = numOfSum[i]
                equalFrequent = [i]

            elif(numOfSum[i] == mostFrequent):
                equalFrequent.append(i)

        numbers = str(foo) + " og " + str(bar)+ ":"
        for i in equalFrequent:
            numbers+= " " + str(i)
        print(numbers)