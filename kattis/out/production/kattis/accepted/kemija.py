sentence = input()

vowels = ["a", "e", "i", "o", "u"]

skip = 0

translated = ""

for i in sentence:
    if(skip > 0):
        skip -= 1
    elif(i in vowels):
        translated += i
        skip = 2
    else:
        translated += i

print(translated)