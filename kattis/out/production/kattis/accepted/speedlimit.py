N = int(input())

while (N != -1):

    previousHour = 0

    totalMiles = 0

    for i in range(N):
        speed = input().split(" ")
        totalMiles += (int(speed[1])-previousHour)*int(speed[0])
        previousHour = int(speed[1])

    print(str(totalMiles) + " miles")

    N = int(input())