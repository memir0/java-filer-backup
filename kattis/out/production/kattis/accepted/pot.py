N = int(input())

sum = 0

for i in range(N):
    numString = input()
    exponent = int(numString[len(numString)-1])
    sum += pow(int(int(numString)/10), exponent)
print(sum)