N = int(input())

for i in range(N):
    inn = input().split(" ")

    b = int(inn[1])
    n = int(inn[2])

    sum = 0

    while(n/b > 0):
        rest = n%b
        n = int(n/b)
        sum += pow(rest, 2)

    output = inn[0] + " " + str(sum)
    print(output)