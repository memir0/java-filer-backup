N = int(input())

for i in range(N):
    num = int(input())
    output = ""
    
    if (abs(num)%2 == 0):
        output = str(num) + " is even"
    else:
        output = str(num) + " is odd"
    
    print(output)