import math

N = int(input())

for i in range(N):
    variables = input().split(" ")
    
    v0 = float(variables[0])
    θ = float(variables[1])
    x = float(variables[2])
    h1 = float(variables[3])
    h2 = float(variables[4])

    t = x/(v0*abs(math.cos(math.radians(θ))))

    yPosition = v0*t*abs(math.sin(math.radians(θ))) - 9.81*t*t/2

    if(yPosition > (h1+1) and yPosition < (h2-1)):
        print("Safe")
    else:
        print("Not Safe")