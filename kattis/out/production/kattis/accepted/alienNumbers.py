import sys

N = int(input())

caseIndex = 1

for i in range(N):
    read = input().split(" ")
    original = read[0]
    originalLang = read[1]
    targetLang = read[2]

    originalLangLenght = len(originalLang)

    originalDecimalValue = 0

    index = len(original)-1

    for j in original:
        index2 = 0
        for g in originalLang:
            if j == g:
                originalDecimalValue += index2*pow(originalLangLenght,index)
                break
            index2 += 1
        index -= 1
    
    targetSentence = {}
    targetLangLenght = len(targetLang)

    index = 0
    while int(originalDecimalValue/targetLangLenght) > 0:
        rest = int(originalDecimalValue % targetLangLenght)
        originalDecimalValue = int(originalDecimalValue / targetLangLenght)

        targetSentence[index] = targetLang[int(rest)]

        index += 1

    rest = int(originalDecimalValue % targetLangLenght)
    targetSentence[index] = targetLang[int(rest)]

    sys.stdout.write('Case #')
    sys.stdout.write(str(caseIndex))
    sys.stdout.write(': ')

    for i in range(len(targetSentence)):
        sys.stdout.write(targetSentence[len(targetSentence)-1-i])
    sys.stdout.write('\n')

    caseIndex += 1