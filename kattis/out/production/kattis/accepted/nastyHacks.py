N = int(input())

for i in range(N):
    inn = input().split(" ")
    
    if(int(inn[1]) - int(inn[0]) < int(inn[2])):
        print("do not advertise")
    elif(int(inn[1]) - int(inn[0]) == int(inn[2])):
        print("does not matter")
    else:
        print("advertise")