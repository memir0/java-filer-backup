import sys

name = input()
next = True

for i in name:
    if (next):
        sys.stdout.write(i)
        next = False
    elif (i == '-'):
        next = True 