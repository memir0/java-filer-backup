use std::io;

fn main(){
    let mut input_string = String::new();
    io::stdin().read_line(&mut input_string);
    let case_amount: u32 = input_string.trim().parse().unwrap();
    'outer: for _i in 0..case_amount {
        input_string = String::new();
        io::stdin().read_line(&mut input_string);
        let number_amount: usize = input_string.trim().parse().unwrap();

        let mut numbers: Vec<String> = vec![];
        for _j in 0..number_amount {
            input_string = String::new();
            io::stdin().read_line(&mut input_string);
            numbers.push(input_string.trim().to_string());
        }

        for j in 0..number_amount {
            for x in j+1..number_amount{
                if numbers[x].len() > numbers[j].len() {
                    if &numbers[x][0..numbers[j].len()] == numbers[j] {
                        println!("NO");
                        continue 'outer;
                    }
                }
                else {
                    if &numbers[j][0..numbers[x].len()] == numbers[x] {
                        println!("NO");
                        continue 'outer;
                    }
                }
            }
        }
        println!("YES");
    }
}