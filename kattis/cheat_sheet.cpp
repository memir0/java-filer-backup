Contents

Compilation
Standard template
Dijkstra
SCC
Max flow
Hashing
Fast exponentiation
Matrix multiplication
Euler phi
Fenwick tree
Order Statistics set / map
Binary search
Convex hull



//Compile like kattis
alias k++='g++ -g -O2 -std=gnu++17 -static';
//Compile with debugging
alias d++='g++ -std=c++17 -Wshadow -Wall -fsanitize=address -fsanitize=undefined -D_GLIBCXX_DEBUG -g ';

alias w++='g++ -g -O2 -std=gnu++17 -static -Wfatal-errors';


// Standard template
#include <bits/stdc++.h>
using namespace std;
using ll = long long;

int main() {
  ios::sync_with_stdio(0); cin.tie(0);

}



// Dijkstra O(E log E)
const int nax = ?;
vector<pair<int,ll>> node[nax];
ll dist[nax];
//int par[nax];

int main() {
  int n, m;
  cin >> n >> m;
  while (m--) {
    int a, b;
    ll c;
    cin >> a >> b >> c;
    a--, b--;
    node[a].push_back({b,c});
    //node[b].push_back({a,c});
  }

  fill_n(dist, n, 1e18);
  priority_queue<pair<ll, int> > pq;
  pq.push(make_pair(0, 0));
  dist[0] = 0;
  while (pq.size()) {
    int p = pq.top().second;
    ll d = -pq.top().first;
    pq.pop();
    if (d > dist[p]) continue;
    for (auto e : node[p]) {
      int j = e.first;
      ll nd = d+e.second;
      if (nd < dist[j]) {
	//par[j] = p;
	dist[j] = nd;
	pq.push(make_pair(-nd, j));
      }
    }
  }

  for (int i = 0; i < n; i++) {
    cout << dist[i] << ' ';
  }
  cout << endl;
}



//SCC O(E)
const int nax = ?;
vector<int> out[nax], in[nax];

int done1[nax];
vector<int> topo;
void dfs1(int p) {
  if (done1[p]++) return;
  for (int i : out[p])
    dfs1(i);
  topo.push_back(p);
}

int done2[nax], component[nax], components = 0;
void dfs2(int p) {
  if (done2[p]++) return;
  for (int i : in[p])
    dfs2(i);
  component[p] = components;
}

{
  while (m--) {
    int a, b;
    cin >> a >> b;
    a--, b--;
    out[a].push_back(b);
    in[b].push_back(a);
  }

  for (int i = 0; i < n; i++) dfs1(i);
  reverse(topo.begin(), topo.end());
  for (int i : topo) {
    if (done2[i]) continue;
    dfs2(i);
    components++;
  }
}



//Max flow, theoretically very slow, but in practice min( O(V * E), O(flow * E) )
const int nax = ?;
vector<int> node[nax];
int done[nax];

struct Edge {
  int start, end;
  ll flow, cap;
};
vector<Edge> edges;

int maxflow(int p, int sink, ll ma = 1e18) {
  if (p == sink) return ma;
  done[p] = 1;
  for (int ei : node[p]) {
    Edge&e = edges[ei];
    ll flow;
    if (e.flow < e.cap && !done[e.end] && (flow = maxflow(e.end, sink, min(ma, e.cap-e.flow)))) {
      edges[ei].flow += flow;
      edges[ei^1].flow -= flow;
      return flow;
    }
  }
  return 0;
}

void addEdge(int a, int b, ll c) {
  node[a].push_back(edges.size());
  edges.push_back({a,b,0,c});
  node[b].push_back(edges.size());
  edges.push_back({b,a,0,0});
}


int main() {
  ios::sync_with_stdio(0); cin.tie(0);

  int n, m;
  cin >> n >> m;
  while (m--) {
    int a, b;
    ll c;
    cin >> a >> b >> c;
    a--, b--;
    addEdge(a, b, c);
  }
  int source = 0, sink = n-1;

  ll tot = 0;
  while (1) {
    fill_n(done, nax, 0);
    ll flow = maxflow(source, sink);
    if (!flow) break;
    tot += flow;
  }
  cout << tot << endl;

  //Reconstruct flow
  for (Edge e : edges) {
    if (e.flow > 0) {
      cout << e.start << ' ' << e.end << ' ' << e.flow << endl;
    }
  }
}



// Hashing O(n) build, O(1) getHash
typedef unsigned long long ull;
const int nax = 1e6+10;
const ull base = 137, mod = (1ll<<55)-55;

ull hpow[nax], ha[nax], hb[nax];

// a * b % mod, for a * b > 2^63
ull mul(ull a, ull b) {
  long long r = a*b-ull((long double)a*b/mod)*mod;
  while (r < 0) r += mod;
  while (r >= mod) r -= mod;
  return r;
}

ull getHash(ull*h, int i, int sz) {
  return (h[i+sz]-mul(hpow[sz], h[i])+mod)%mod;
}

int main() {
  ios::sync_with_stdio(0); cin.tie(0);

  hpow[0] = 1;
  for (int i = 1; i < nax; i++)
    hpow[i] = mul(hpow[i-1], base);

  string a, b;
  cin >> a >> b;
  for (int i = 0; i < a.size(); i++) {
    ha[i+1] = (mul(ha[i], base)+a[i])%mod;
  }
  for (int i = 0; i < b.size(); i++) {
    hb[i+1] = (mul(hb[i], base)+b[i])%mod;
  }

  int ans = 0;
  for (int i = 0; i+b.size() <= a.size(); i++) {
    if (getHash(ha, i, b.size()) == getHash(hb, 0, b.size())) {
      ans++;
    }
  }
  cout << ans << endl;
}



//Fast exponentiation O(log p)

//Expects a * mod < 9*10^18
//Use mul(r,a) and mul(a,a) from hashing if r*a or a*a can overflow!
//pow(a,p,mod) is already in python
ll bin_pow(ll a, ll p, ll mod) {
  ll r = 1;
  while (p) {
    if (p%2) r = r*a % mod;
    a = a*a % mod;
    p /= 2;
  }
  return r;
}

//bin_pow(a, mod-2, mod) is the modular inverse of a modulo mod, if mod is a prime (like 10^9+7)



//Matrix multiplication O(n^3) for n x n matrices
ll mod = ?;
vector<vector<ll>> operator*(vector<vector<ll>> A, vector<vector<ll>> B) {
  ll ni = A.size(), nj = B.size(), nk = B[0].size();
  assert(nj == A[0].size());
  vector<vector<ll>> R(ni,vector<ll>(nk,0));
  for (int i = 0; i < ni; i++)
    for (int j = 0; j < nj; j++)
      for (int k = 0; k < nk; k++)
	(R[i][j] += A[i][k]*B[k][j]) %= mod;
  return R;
}

{
  //p'th fibonacci number, if f(0) = 0, f(1) = 1. O(log p)
  vector<vector<ll>> A = {{1,1},
			  {1,0}};
  vector<vector<ll>> R = {{1,0},
			  {0,1}};
  ll p;
  cin >> p;
  while (p) {
    if (p%2) R = R*A;
    A = A*A;
    p /= 2;
  }
  cout << ((R[1][0])%mod+mod)%mod << endl;
}



//Euler phi function is product of n*(1-1/p1)*(1-1/p2)*... for all primes dividing n
bin_pow(a, phi(n), n) = 1; // if gcd(a,n) == 1
bin_pow(a, phi(n)-1, n) = 1/a; // if gcd(a,n) == 1
bin_pow(a, x, n) = bin_pow(a, x%phi(n) + phi(n), n); // if x >= phi(n)
bin_pow(a, x, n) = bin_pow(a, x, n) ;// if x < phi(n) :)



//Fenwick tree O(log n)
const int nax = ?+20;
ll data[nax];

void add(int i, ll v) {
  for (i+=10; i < nax; i += i&-i) data[i] += v;
}

//sum up to and including i
ll sum(int i) {
  ll r = 0;
  for(i+=10; i; i -= i&-i) r += data[i];
  return r;
}



//Order Statistics set / map  O(log n)
#include <bits/extc++.h>
using namespace __gnu_pbds;
template<class T>
using os_set = tree<T, null_type, less<T>, rb_tree_tag, tree_order_statistics_node_update>;

template<class T, class S>
using os_map = tree<T, S, less<T>, rb_tree_tag, tree_order_statistics_node_update>;

{
  os_set<pair<int,int>> ost;
  ost.insert({0,1});
  ost.erase({0,1});
  ost.order_of_key({2,0}); //number of elements strictly less than {2,0}

  assert(10 < ost.size());
  pair<int,int> p = *ost.find_by_order(10); //10'th element in sorted order

  os_map<int,int> osm;
  for (int i = 0; i < n; i++) cin >> osm[i*10];
  pair<int,int> q = *osm.find_by_order(10);
}



//Binary search O(log n)
{
  auto check = [&](ll x) {
    if (ok) return 1;
    return 0;
  };

  //Watch out for long long overflow if pass / fail is large!
  ll fail = ?, pass = ?;
  //assert(!check(fail));
  //assert(check(pass));
  while (abs(pass-fail) > 1) {
    ll mid = pass+fail>>1;
    if (check(mid)) pass = mid;
    else fail = mid;
  }
  cout << pass << endl;
}



//Convex hull O(n log n)
#define x first
#define y second
typedef pair<ll,ll> point;

point operator-(point a, point b) {
  return {a.x-b.x, a.y-b.y};
}
ll operator%(point a, point b) {
  return a.x*b.y - a.y*b.x;
}


ll operator*(point a, point b) {
  return a.x*b.x + a.y*b.y;
}
point operator+(point a, point b) {
  return {a.x+b.x, a.y+b.y};
}

//~(a-b) gives distance from a to b
double operator~(point a) {
  return hypot(a.x,a.y);
}


bool angle_cmp(point a, point b) {
  if (a%b == 0) return a*a < b*b;
  return a%b > 0;
}

vector<point> convexHull(vector<point> a) {
  point base = *min_element(a.begin(), a.end());
  for (point&p : a) {
    p = p-base;
  }

  //Sorts counterclockwise
  sort(a.begin(), a.end(), angle_cmp);

  vector<point> hull;
  for (point p : a) {
    while (hull.size() >= 2 && (p-hull.end()[-2]) % (hull.end()[-1]-hull.end()[-2]) >= 0) {
      hull.pop_back();
    }
    hull.push_back(p);
  }
  return hull;
}


chinese.h
Description: Chinese Remainder Theorem.
chinese(a, m, b, n) returns a number x, such that x ≡ a (mod m) and
x ≡ b (mod n). For not coprime n, m, use chinese common. Note that all
numbers must be less than 2 31 if you have Z = unsigned long long.
Time: log(m + n)
da3099, 13 lines
"euclid.h"
template<class Z> Z chinese(Z a, Z m, Z b, Z n) {
Z x, y; euclid(m, n, x, y);
Z ret = a * (y + m) % m * n + b * (x + n) % n * m;
if (ret >= m * n) ret -= m * n;
return ret;
}
template<class Z> Z chinese_common(Z a, Z m, Z b, Z n) {
Z d = gcd(m, n);
if (((b -= a) %= n) < 0) b += n;
if (b % d) return -1; // No solution
return d * chinese(Z(0), m/d, b/d, n/d) + a;
}

// next_permutation example
#include <iostream>     // std::cout
#include <algorithm>    // std::next_permutation, std::sort

int main () {
  int myints[] = {1,2,3};

  std::sort (myints,myints+3);

  std::cout << "The 3! possible permutations with 3 elements:\n";
  do {
    std::cout << myints[0] << ' ' << myints[1] << ' ' << myints[2] << '\n';
  } while ( std::next_permutation(myints,myints+3) );

  std::cout << "After loop: " << myints[0] << ' ' << myints[1] << ' ' << myints[2] << '\n';

  return 0;
}

// unique algorithm example
#include <iostream>     // std::cout
#include <algorithm>    // std::unique, std::distance
#include <vector>       // std::vector

bool myfunction (int i, int j) {
  return (i==j);
}

int main () {
  int myints[] = {10,20,20,20,30,30,20,20,10};           // 10 20 20 20 30 30 20 20 10
  std::vector<int> myvector (myints,myints+9);

  // using default comparison:
  std::vector<int>::iterator it;
  it = std::unique (myvector.begin(), myvector.end());   // 10 20 30 20 10 ?  ?  ?  ?
                                                         //                ^

  myvector.resize( std::distance(myvector.begin(),it) ); // 10 20 30 20 10

  // using predicate comparison:
  std::unique (myvector.begin(), myvector.end(), myfunction);   // (no changes)

  // print out content:
  std::cout << "myvector contains:";
  for (it=myvector.begin(); it!=myvector.end(); ++it)
    std::cout << ' ' << *it;
  std::cout << '\n';

  return 0;
}

//Union find
const int nax = 1e5;
int par[nax], sz[nax];

int find(int p) {
  return par[p] = (p == par[p] ? p : find(par[p]));
}

void join(int a, int b) {
  a = find(a);
  b = find(b);
  if (a == b) return;

  if (sz[a] < sz[b]) swap(a,b);
  //assert(sz[a] >= sz[b]);
  par[b] = a;
  sz[a] += sz[b];
}

{
  for (int i = 0; i < n; i++)
    par[i] = i, sz[i] = 1;
}

int cp = n;
vector<int> primes;
for (int i = 2; i*i <= n; i++) {
  if (n%i) continue;
  primes.push_back(i);
  while (n%i == 0) {
    n /= i;
  }
 }
if (n > 1) primes.push_back(n);

int phi = n;
for (int p : primes)
  phi = phi/p*(p-1);
