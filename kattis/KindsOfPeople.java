
// Java Program to demonstrate adjacency list  
// representation of graphs 
import java.util.LinkedList; 
import java.util.Scanner;
import java.util.Queue; 

public class KindsOfPeople{ 
    // A user define class to represent a graph. 
    // A graph is an array of adjacency lists. 
    // Size of array will be V (number of vertices  
    // in graph) 
    static class Graph{ 
        int V; 
        LinkedList<Integer> adjListArray[]; 
          
        Graph(int V){ 
            this.V = V; 
              
            // define the size of array as  
            // number of vertices 
            adjListArray = new LinkedList[V]; 
              
            // Create a new list for each vertex 
            // such that adjacent nodes can be stored 
            for(int i = 0; i < V ; i++){ 
                adjListArray[i] = new LinkedList<>(); 
            } 
        }
         
        void addEdge(int src, int dest){ 
            // Add an edge from sourve to destination 
            adjListArray[src].add(dest); 
            
            // Since the graph is undirected, add an edge from destination 
            // to source aswell 
            adjListArray[dest].add(src); 
        } 


        boolean isConnected(int sourceNode, int destinationNode, int numberOfNodes){
            Queue<Integer> toDo = new LinkedList<>(); 
            boolean[] done = new boolean[numberOfNodes];

            toDo.add(sourceNode);

            while(!toDo.isEmpty()){
                int temp = toDo.remove();
                done[temp] = true;
                for(Integer pCrawl: adjListArray[temp]){
                    if(pCrawl == destinationNode){
                        return true;
                    }
                    if(!done[pCrawl]){
                        toDo.add(pCrawl);
                    }
                }
            }
            return false;
        }
       
        // A utility function to print the adjacency list  
        // representation of graph 
        void printGraph(int c){        
            for(int v = 0; v < V; v++){ 
                System.out.println("Adjacency list of vertex row: "+ (v/c+1) + " place: " + (v%c+1)); 
                System.out.print("head"); 
                for(Integer pCrawl: adjListArray[v]){ 
                    System.out.print(" -> row: "+ (pCrawl/c+1) + " place: " + (pCrawl%c+1)); 
                } 
                System.out.println("\n"); 
            } 
        } 
    } 
      
      // TODO
      // Put gruppene i arrayer.
    
    public static void main(String args[]){
        Scanner scanner = new Scanner(System.in);

        int r = scanner.nextInt();
        int c = scanner.nextInt();
        int cAdress = c-1;

        int numberOfNodes = r*c;

        String[] mapLine = new String[r];
        boolean[] map = new boolean[numberOfNodes];

        Graph graph = new Graph(numberOfNodes); 

        for(int i = 0; i < r; i++){
            mapLine[i] = scanner.next();
        }

        for(int i = 0; i < r; i++){
            for(int j = 0; j < c; j++){
                if(mapLine[i].charAt(j)=='1'){
                    map[i*c+j] = true;
                } else {
                    map[i*c+j] = false;
                }
            }
        }

        // First line
        for(int i = 0; i < cAdress; i++){
            if(map[i] == map[i+1]){
                graph.addEdge(i, i+1);

                //System.out.println(map[0].charAt(i) + " == " + map[0].charAt(i+1));
            }
        }

        // Rest (checks above itself)
        for(int i = 1; i < r; i++){
            for(int j = 0; j < c; j++){
                int localAdress = i*c+j;
                boolean tempValue = map[i*c+j];
                if(j == cAdress){
                    if(tempValue == map[localAdress-c]){
                        //System.out.println((i+1) + ", " + (j+1) + " == " + i + ", " + (j+1));
                        graph.addEdge(localAdress, localAdress-c);
                    }
                } else {
                    if(tempValue == map[localAdress-c]){
                        //System.out.println((i+1) + ", " + (j+1) + " == " + i + ", " + (j+1));
                        graph.addEdge(localAdress, localAdress-c);
                    }
                    if(tempValue == map[localAdress+1]){
                        //System.out.println((i+1) + ", " + (j+1) + " == " + (i+1) + ", " + (j+2));
                        graph.addEdge(localAdress, localAdress+1);
                    }
                }
            }
        }
       
        /*** 
        print the adjacency list representation of  
        the above graph:
            graph.printGraph(c); 
        ***/

        int n = scanner.nextInt();

        for(int i=0; i < n; i++){
            int x1 = scanner.nextInt();
            int y1 = scanner.nextInt();
            int x2 = scanner.nextInt();
            int y2 = scanner.nextInt();

            int sourceNode = (x1-1)*c+(y1-1);
            int destinationNode = (x2-1)*c+(y2-1);

            if(map[(x1-1)*c+(y1-1)] == map[(x2-1)*c+(y2-1)]){
                if(graph.isConnected(sourceNode, destinationNode, numberOfNodes)){
                    if(map[(x1-1)*c+(y1-1)]){
                        System.out.println("decimal");
                    } else {
                        System.out.println("binary");
                    }
                } else {
                    System.out.println("neither");
                }
            } else {
                System.out.println("neither");
            }
        }

    } 
} 