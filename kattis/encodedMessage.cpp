#include <iostream>
#include <string>
#include <math.h>
using namespace std;

int main(){
    int N = 0;
    cin >> N;

    for(int i = 0; i < N; i++){
        string codedMessage;
        cin >> codedMessage;
        int squareSize = sqrt(codedMessage.length());
        int index = squareSize-1;
        for(int j = 0; j < squareSize; j++){
            while(index < codedMessage.length()){
                cout << codedMessage[index];
                index += squareSize;
            }
            index = squareSize-j-2;
        }
        cout << endl;
    }
}