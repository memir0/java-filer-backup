import java.io.*;
import static javax.swing.JOptionPane.*;

class Klientprogram implements java.io.Serializable{
    
    private static Soppregister lesRegFraFil(String filnavn){
        //bruker try fordi det er flere ting som kan gå galt og vi vil ikke at programmet skal kræsje
        try{
            //prøver å åpne den angitte filen 
            FileInputStream fis = new FileInputStream(filnavn);
            ObjectInputStream ois = new ObjectInputStream(fis);
            
            //leser objektet og caster det til typen Soppregister
            Soppregister register = (Soppregister) ois.readObject();
            
            //lukker filen slik at andre prosesser kan få tilgang til den.
            ois.close();
            fis.close();
            
            return register;
        }
         catch(FileNotFoundException ioe){
            String respons = "Fant ikke filen " + filnavn;
            showMessageDialog(null, respons);
            return null;
        }
        catch(EOFException efoe){
            showMessageDialog(null, "filen mangler et objekt");
            return null;
        }
        catch(IOException ioe){
            showMessageDialog(null, "Noe gikk galt med I/O enheten");
            return null;
        }
        catch(SecurityException se){
            showMessageDialog(null, "Du har ikke de nødvendlige privilegene for å åpne filen");
            return null;
        }
        catch(Exception e){
            showMessageDialog(null, "Noe gikk galt med lesing av fil, vennligst kontakt administrator");
            e.printStackTrace();
            return null;
        }
    }
    
    private static Soppregister opprettNyttRegister(){
        Soppregister tempRegister = new Soppregister();
        return tempRegister;
    }
    
    public static void main(String[] args){
        String filnavn = "soppregister.ser";
        Soppregister register = lesRegFraFil(filnavn);
        
        if (register == null){
            register = opprettNyttRegister();   // metode som opppretter ett tomt register
        }

        String[] muligheter = {"List alle", "List matsopper", "Legg til ny", "Søk", "Avslutt"};
        final int LIST_ALLE = 0;
        final int LIST_MATSOPPER = 1;
        final int REG_SOPP = 2;
        final int SOK = 3;
        final int AVSLUTT = 4;

        int valg = showOptionDialog(null, "Velg", "Eksamen des 2018",  YES_NO_OPTION,              INFORMATION_MESSAGE, null, muligheter, muligheter[0]);
                while (valg != AVSLUTT){
                    switch (valg){
                        case LIST_ALLE:
                          /*Anta at koden eksisterer*/
                        break;

                        case LIST_MATSOPPER:
                          /*Anta at koden eksisterer*/
                        break;

                        case REG_SOPP:
                            //henter inn navn og beskrivelse som strings
                            String navn = showInputDialog(null, "Vennligst skriv inn navnet på soppen");
                            String beskrivelse = showInputDialog(null, "Vennligst beskriv soppen");
                            //for giftighet bruker jeg option dialog for å minimere sannsynlighet for at brukeren bruker programmet feil
                            String[] giftigValg = {"Matsopp", "Giftig"};
                            int giftig = showOptionDialog(null, "Er soppen giftig eller er det en matsopp?", "Giftig?", YES_NO_OPTION, INFORMATION_MESSAGE, null, giftigValg, giftigValg[0]);
                            
                            //siden giftig er en int sjekker jeg den slik for å gjøre om til boolean:
                            if(giftig == 1){
                                Soppart nySopp = new Soppart(navn, beskrivelse, true);
                                register.registrerSopp(nySopp);
                            } else {
                                Soppart nySopp = new Soppart(navn, beskrivelse, false);
                                register.registrerSopp(nySopp);
                            }
                        break;

                        case SOK:
                            //tar inn et ord som skal søkes med
                            String sokOrd = showInputDialog(null, "Vennligst skriv inn ordet du vil søke med");
                            
                            //vi tar høyde for at brukeren kan skrive inn flere ord ved å dele opp stringen og bare bruke det første ordet
                            String[] sokOrdSplittet = sokOrd.split(" ");
                            
                            String svar = register.sok(sokOrdSplittet[0]);
                            
                            //viser informasjon om soppene med søkordet i beskrivelsen til brukeren
                            showMessageDialog(null, svar);
                        break;

                        default: break;
                    }
                    valg = showOptionDialog(null, "Velg", "Eksamen des 2018", YES_NO_OPTION, INFORMATION_MESSAGE, null, muligheter, muligheter[0]);
                }
        //skrivRegTilfil(filnavn,register);
    }
}