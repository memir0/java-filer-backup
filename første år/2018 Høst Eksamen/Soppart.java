class Soppart{
    //klassens objektvariabler (read only)
    final private String navn;
    final private String beskrivelse;
    final private boolean giftig;
    
    //konstruktør som tar inn variablene
    Soppart(String navn, String beskrivelse, boolean giftig){
        this.navn = navn;
        this.beskrivelse = beskrivelse;
        this.giftig = giftig;
    }
    
    //konstruktør som tar inn et annet Soppart objekt
    Soppart(Soppart etablertSopp){
        this.navn = etablertSopp.getNavn();
        this.beskrivelse = etablertSopp.getBeskrivelse();
        this.giftig = etablertSopp.getGiftig();
    }
    
    //tilgangsmetoder
    /*  her har jeg anntatt at "klassen skal være immutabel" sikter til at attributtene skal 
        være immutable. Vi kan også bruke nøkkelordet final på metodene til klassen for å
        sørge for at de ikke utvides til å gjøre ting vi ikke ønsker, men jeg har valgt å
        ikke gjøre dette på grunn av min tidligerenevnte tolkning av oppgaven.
    */
    public String getNavn(){
        return navn;
    }
    
    public String getBeskrivelse(){
        return beskrivelse;
    }
    
    public boolean getGiftig(){
        return giftig;
    }
    
    //sammenligner navn stringen med navnet til en annen, her bruker vi String sin innebygget equals() metode
    public boolean equals(Soppart annenSopp){
        return navn.equals(annenSopp.getNavn());
    }
    
    //Søker i beksrivelsen etter søkordet, her er det viktig at stringen kun er ett ord, hvis ikke returnerer den alltid false
    public boolean sok(String sokOrd){
        //sjekker først at søkeordet eksisterer for å unngå komplikasjoner senere
        if(sokOrd.equals("") || sokOrd.equals(null)){
            return false;
        }
        
        //splitter beksrivelsen i ord som er delt med mellomrom
        String[] splittetBeskrivelse = beskrivelse.split(" ");
        
        //går igjennom alle ord i beskrivelsen og sammenligner de med søkordet ved hjelp av String sin innebygde equals() metode
        for(int i = 0; i < splittetBeskrivelse.length; i++){
            if(sokOrd.equals(splittetBeskrivelse[i])){
                //Om den finner ordet returnerer den true
                return true;
            }
        }
        //hvis den ikke finner ordet i beskrivelsen returnerer den false
        return false;
    }
}