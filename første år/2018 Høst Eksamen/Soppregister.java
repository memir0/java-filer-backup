import java.util.*;

class Soppregister{
    //setter opp en tom liste med 10 plasser til objekter av typen Soppart
    private Soppart[] sopparter = new Soppart[10];
    
    //holder styr på hvor mange plasser som fylt med sopper for å unngå overskriving og lesing av null objekter 
    private int antallSopper = 0;
    
    Soppregister(){
      /*Trenger ikke å definere noe konstruktør her,
        anntar at denne er her for soppart = new Soppart[10];
        som jeg allerede har definert
      */
    }
    
    //hjelpemetode for å sjekke om en sopp allerede er registert
    private boolean finnesAllerede(Soppart sokSopp){
        for(int i = 0; i < antallSopper; i++){
            //her bruker vi vår egendefinerte equals() metode for å sammenligne
            if(sokSopp.equals(sopparter[i])){
                //Hvis søk soppen har samme navn som en sopp i registeret returnerer den true
                return true;
            }
        }
        //Hvis det er en ny sopp returneres false
        return false;
    }
    
    //hjelpemetode for å utvide listen med 10, returnerer true om det gikk bra
    private boolean utvidRegister(){
        //lager en ny liste som er 10 lengre enn forrige
        try{
            Soppart[] nyListe = new Soppart[(sopparter.length+10)];
        
            //kopierer sopper fra gammel liste over i ny liste
            for(int i = 0; i < antallSopper; i++){
                //Her koppieres kun refferansen til elementene, dette sparer litt tid og gjør at vi slipper å slette de gammle elementene
                nyListe[i] = sopparter[i]; 
            }
            
            //setter sopparter sin referanse til å være den nye listen
            sopparter = nyListe;
            return true;
        }
        catch(Exception e){
            //her kan det oppstå flere problemer, men mest sannsynlig er det mangel på minneplass til den nye listen
            e.printStackTrace(); //printer feilmeldingen
            return false;
        }
    }
    
    //registerer ny sopp i registeret
    public boolean registrerSopp(Soppart sopp){
        //Hvis det er plass til en ny sopp
        try{
            //hvis soppen ikke finnes i registeret kan den bli lagt inn
            if(!finnesAllerede(sopp)){
                //hvis det er mer plass i registeret blir soppen bare lagt til
                if(antallSopper > sopparter.length){
                    sopparter[antallSopper] = sopp;
                    antallSopper++;
                    return true; //signaliserer at registreringen ble gjennomført
                } else {
                    //hvis ikke blir listen utvidet først
                    if(utvidRegister()){
                        sopparter[antallSopper] = sopp;
                        antallSopper++;
                        return true;
                    } else {
                        //dersom noe gikk galt med utvidelsen av listen blir false returnert
                        //Feilmeldingen blir også printet ut som gjør at man kan skille mellom feilmeldingene for utvidelsen og feilmeldingen for finnesAllerede
                        return false;
                    }
                }
            } else {
                //Hvis soppen allerede eksisterer blir false returnert
                return false;
            }
        }
        catch(Exception e){
            //hvis noe går galt med registertingen får brukeren beskjed ved at det blir returnert false og ved at feilmeldingen blir skrevet ut
            e.printStackTrace();
            return false;
        }
    }
    
    //Metode for å returnere all registrert matsopp, hvor matsopp er definert som en sopp som ikke er giftig
    public Soppart[] registrertMatsopp(){
        //lager en arraylist fordi vi ikke vet hvor mange giftige sopper som er registrert og det er derfor lettvint å ha en liste som kan dynamisk varriere lengde
        ArrayList<Soppart> matsopp = new ArrayList<Soppart>();
        
        //for alle sopper i registeret
        for(int i = 0; i < antallSopper; i++){
            //hvis soppen ikke er giftig
            if(!sopparter[i].getGiftig()){
                //legger til soppen i ArrayListen
                matsopp.add(sopparter[i]);
            }
        }
        
        //lager en ny liste som skal returneres 
        Soppart[] returListe = new Soppart[matsopp.size()];
        
        //for all matsopp (ArrayList bruker size() istedenfor lenght)
        for(int i = 0; i < matsopp.size(); i++){
            //legger til alle elementene fra matsopp inn i returListe
            returListe[i] = matsopp.get(i);
        }
        
        return returListe;
    }
    
    //returnerer en tekst string med alle sopper i registeret
    public String toString(){
        String ferdigString = "Alle registrerte Sopparter (Navn Beskrivelse Spiselig):";
        try{
            for(int i = 0; i < antallSopper; i++){
                //legger til en ny linje navn og beskrivelse til ferdigString
                ferdigString += "\n" + (i+1) + ": " + sopparter[i].getNavn() + " " + sopparter[i].getBeskrivelse();
                
                //legger til gifitg eller matsopp avhengig av bool verdien til variablen giftig
                if(sopparter[i].getGiftig()){
                    ferdigString += " Giftig";
                } else {
                    ferdigString += " Matsopp";
                }
            }
            
            return ferdigString;
        }
        catch(Exception e){
            //her kan det for eksempel hende at stringen blir alt for stor og man får ressursmangel exception
            e.printStackTrace();
            return "Noe gikk galt med toString()";
        }
    }
    
    
    public String sok(String sokOrd){
        try{
            String sopperMedOrdet = "Sopper med " + sokOrd + " i beskrivelsen:";
            
            for(int i = 0; i < antallSopper; i++){
                if(sopparter[i].sok(sokOrd)){
                    //her anntar jeg at navnet, beskrivelsen og giftigheten er informasjon man er ute etter om soppen
                    sopperMedOrdet += "\n" + (i+1) + ": " + sopparter[i].getNavn() + " " + sopparter[i].getBeskrivelse();
                    
                    //legger til gifitg eller matsopp avhengig av bool verdien til variablen giftig
                    if(sopparter[i].getGiftig()){
                        sopperMedOrdet += " Giftig";
                    } else {
                        sopperMedOrdet += " Matsopp";
                    }
                }
            }
        
            return sopperMedOrdet;
        }
        catch(Exception e){
            //her kan det for eksempel hende at stringen blir alt for stor og man får ressursmangel exception
            e.printStackTrace();
            return "Noe gikk galt med søket";
        }
    }
}