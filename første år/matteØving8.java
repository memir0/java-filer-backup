class integralKalkulator{
    public static void main(String[] args){
        double[][] resultat = new double[3][3];

        int x0 = 1, x1 = 2;
        int[] n = {10, 100, 1000};

        for(int i = 0; i < 3; i++){
            resultat[i][0] = trapesMetoden(x0, x1, n[i]);
            resultat[i][1] = midtpunktsMetode(x0, x1, n[i]);
            resultat[i][2] = simpsonsMetode(x0, x1, n[i]);
        }

        //System.out.println(trapesMetoden(0,1,8));
        //System.out.println(midtpunktsMetode(0,1,8));

        System.out.println("_______________________________________________________________________");
        System.out.println("|n     | Trapes Metoden     | Midtpunkts Metoden | Simpson Metoden    |");

        for(int i = 0; i < 3; i++){
            System.out.print("|" + n[i]);
            for(int j = 3; j > i; j--) System.out.print(" ");
            System.out.print(" | ");
            System.out.print(resultat[i][0] + " | ");
            System.out.print(resultat[i][1] + " | ");
            System.out.println(resultat[i][2]+ " | ");
        }
    }

    public static double funksjonen(double x){
        return Math.sqrt(Math.pow(x, 3)-1);
        //return Math.cos(Math.pow(x, 2));
    }

    public static double trapesMetoden(int x0, int x1, int n){
        double deltaX = (double)(x1-x0)/n;

        double tn = (deltaX/2);

        double multiple = funksjonen(x0);

        double step = (double)(x1-x0)/n;
        for(int i = 1; i < n-1; i++){
            multiple += 2*funksjonen(x0+step*i);
        }

        multiple += funksjonen(x1);

        return tn*multiple;
    }

    public static double simpsonsMetode(int x0, int x1, int n){
        double sn = (double)(x1-x0)/(3*n);

        double step = (double)(x1-x0)/n;

        double multiple = funksjonen(x0);

        for(int i = 1; i < n-1; i+=2){
            multiple += 4*funksjonen(x0+step*i);
        }
        for(int i = 2; i < n-1; i+=2){
            multiple += 2*funksjonen(x0+step*i);
        }

        multiple += funksjonen(x1);

        return sn*multiple;
    }

    public static double midtpunktsMetode(int x0, int x1, int n){
        double mp = (double)(x1 - x0)/n;

        double step = (double)(x1-x0)/n;

        double multiple = 0;

        for(float i = 0; i < n; i++){
            multiple += funksjonen(x0+i*step);
        }

        return mp*multiple;
    }
}