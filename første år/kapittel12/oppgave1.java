public class oppgave1{
    public static void main(String[] args){
        Student[] test = new Student[10];
        OppgaveOversikt oversikt = new OppgaveOversikt(test);

        String[] navn = {"Knut", "Johan", "Petter", "Mohammed", "Minh"};

        //legger til studenter i oversikten
        for(int i = 0; i < 5; i++){
            oversikt.leggTilStudent(navn[i]);
            oversikt.okAntOppg(i,i); //øker antall oppgaver for hver elev med i
        }

        System.out.println(oversikt.antallStudenter());

        for(int i = 0; i < 5; i++){
            System.out.println(oversikt.antallOppg(i));
        }

        /* Test for Student klassen
        System.out.println(test.getNavn());
        test.okAntOppg(5);
        System.out.println(test.getAntallOppg());
        System.out.println(test.toString());
        */
    }
}


class Student{
    private String navn;
    private int antOppg = 0;

    public Student(String navn){
        this.navn = navn;
    }

    public String getNavn(){
        return navn;
    }
    
    public int getAntallOppg(){
        return antOppg;
    }

    public void okAntOppg(int okning){
        antOppg += okning;
    }   
}

class OppgaveOversikt{
    private Student[] studenter;
    private int antallStudenter;
    
    public OppgaveOversikt(Student[] studenter){
        this.studenter = studenter;
    }

    public int antallStudenter(){
        return antallStudenter;
    }

    public int antallOppg(int studentnr){
        return studenter[studentnr].getAntallOppg();
    }

    public void leggTilStudent(String navn){
        studenter[antallStudenter] = new Student(navn);
        antallStudenter++;
    }

    public void okAntOppg(int studentnr, int antallOppgaver){
        studenter[studentnr].okAntOppg(antallOppgaver);
    }
}