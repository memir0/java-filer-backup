import java.util.Scanner;
import java.util.*;

class tallsystem{
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        System.out.println("1: Til et gitt tallsystem fra 10-tallsystemet  2: Fra et gitt tallsystem til 10-tallsystemet");
        int rettning = sc.nextInt();

        //Input
        System.out.print("Tallsystem: ");
        Long tallsystem = sc.nextLong();
        System.out.print("Tallet: ");
        Long tall = sc.nextLong();

        if(rettning==1){  //Til et gitt tallsystem fra 10-tallsystemet

        //Dynamisk array som legger inn siffrene i tallet 
        Vector<Integer> rest = new Vector<Integer>();

        while(tall/tallsystem > 0){
            //Deler tallet på tallsystemet og sparer resten helt til tallet er mindre enn tallsystemet
            rest.addElement((int)(tall%tallsystem));
            tall = tall/tallsystem;
        }

        //legger til resten som et siffer i sluttallet i vectoren rest
        rest.addElement((int)(tall%tallsystem));

        int lenght = rest.size();
        if(tallsystem <= 10){
            for(int i = lenght-1; i >= 0; i--){
                //Printer ut hvert siffer i vectoren rest fra slutt til start slik at det til sammen blir tallet man er ute etter
                System.out.print(rest.get(i));
            }
        }else {
            for(int i = lenght-1; i >= 0; i--){
                //Printer ut hvert siffer i vectoren rest fra slutt til start slik at det til sammen blir tallet man er ute etter
                int temprest = rest.get(i);
                if(temprest <= 10){
                    System.out.print(rest.get(i));
                } else {
                    System.out.print((char)(temprest+54));
                }
            }
        }
        
        } else { //Fra et gitt tallsystem til 10-tallsystemet
            int i = 0;
            long rest = 0;
            int titallstall = 0;

            while(tall > 0){
                //Deler opp tallet i sine siffere
                rest = tall%10;
                tall = tall/10;
                
                //Opphøyer alle tallsystemet i alle sifferene avhengig av posisjonen deres i tallet og plusser sammen;
                titallstall += rest*Math.pow(tallsystem, i);
                i++;
            }
            //Output
            System.out.print(titallstall);
        }
    }
}