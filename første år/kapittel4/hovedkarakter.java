import java.util.Scanner;

class hovedkarakter{
    public static void main (String[] args){
        Scanner sc = new Scanner(System.in);
        int antallKarakterer = sc.nextInt();
        int total = 0;
        char karakter;
        for (int i = 0; i < antallKarakterer; i++){
            karakter = sc.next().charAt(0);
            total += karakter;
        }
        double gjennomsnitt = (double) total/antallKarakterer;
        int gjennomsnittInt = (int) gjennomsnitt;
        int resultat = (int) (gjennomsnitt*2);
        if(resultat%2 != 0){
            gjennomsnittInt += 1;
        }
        if(gjennomsnitt+0.5 == (int) (gjennomsnitt + 0.5)){
            gjennomsnittInt--;
        }
        System.out.println("Snittet er " + (char) gjennomsnittInt);
    }
}