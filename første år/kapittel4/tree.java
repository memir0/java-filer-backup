import java.util.Scanner;

class tree{
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        int height = sc.nextInt();
        for (int i = 0; i < height; i++){
            int spaces = height - i;
            for(int j = 0; j < spaces; j++){
                System.out.print(" ");
            }
            for(int j = 0; j < 1 + i*2; j++){
                System.out.print("*");
            }
            System.out.println("");
        }
    }
}