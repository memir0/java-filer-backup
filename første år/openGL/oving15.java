package triangle;

import java.awt.*;
import javax.swing.*;

import com.jogamp.opengl.GL2;
import com.jogamp.opengl.glu.GLU;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLEventListener;
import com.jogamp.opengl.awt.GLCanvas;
import com.jogamp.opengl.util.gl2.GLUT;
import com.jogamp.opengl.glu.GLUquadric;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;


/**
 * NeHe Lesson #3 (JOGL 2 Port): Adding Colors (to Lesson #2 - Basic 2D).
 * @author Tomas Holt, based on code from Hock-Chuan Chua (May 2012)
 * @version October 2016
 */

/* Main class which extends GLCanvas. This means that this is a OpenGL canvas.
   We will use OpenGL commands to draw on this canvas.
   This implementation has no animation or user input.
*/
public class oving15 extends GLCanvas implements GLEventListener {
   // constants
   private static String TITLE = "�ving 13 oppgave 2";
   private static final int CANVAS_WIDTH = 700;  // width of the drawable
   private static final int CANVAS_HEIGHT = 700; // height of the drawable
   
   public double rotAnglex, rotAnglez;
   
   float colors[][] = {
           {0.583f,  0.771f,  0.014f},
           {0.609f,  0.115f,  0.436f},
           {0.327f,  0.483f,  0.844f},
           {0.822f,  0.569f,  0.201f},
           {0.435f,  0.602f,  0.223f},
           {0.310f,  0.747f,  0.185f},
           {0.597f,  0.770f,  0.761f},
           {0.559f,  0.436f,  0.730f},
           {0.359f,  0.583f,  0.152f},
           {0.483f,  0.596f,  0.789f},
           {0.559f,  0.861f,  0.639f},
           {0.195f,  0.548f,  0.859f},
           {0.014f,  0.184f,  0.576f},
           {0.771f,  0.328f,  0.970f},
           {0.406f,  0.615f,  0.116f},
           {0.676f,  0.977f,  0.133f},
           {0.971f,  0.572f,  0.833f},
           {0.140f,  0.616f,  0.489f},
           {0.997f,  0.513f,  0.064f},
           {0.945f,  0.719f,  0.592f},
           {0.543f,  0.021f,  0.978f},
           {0.279f,  0.317f,  0.505f},
           {0.167f,  0.620f,  0.077f},
           {0.347f,  0.857f,  0.137f},
           {0.055f,  0.953f,  0.042f},
           {0.714f,  0.505f,  0.345f},
           {0.783f,  0.290f,  0.734f},
           {0.722f,  0.645f,  0.174f},
           {0.302f,  0.455f,  0.848f},
           {0.225f,  0.587f,  0.040f},
           {0.517f,  0.713f,  0.338f},
           {0.053f,  0.959f,  0.120f},
           {0.393f,  0.621f,  0.362f},
           {0.673f,  0.211f,  0.457f},
           {0.820f,  0.883f,  0.371f},
           {0.982f,  0.099f,  0.879f}
       };
   
  
   // Setup OpenGL Graphics Renderer 
   private GLU glu;  // for the GL Utility
   
   /** Constructor to setup the GUI for this Component */
   public oving15() {
      this.addGLEventListener(this);
      this.addKeyListener(new RotateKeyListener()); //listener for keyboard
   }
   
// ------ Implement methods declared in GLEventListener (init,reshape,display,dispose)          

   /**
    * Called immediately after the OpenGL context is initialized. Can be used 
    * to perform one-time initialization. Run only once.
    */
   public void init(GLAutoDrawable drawable) {
      GL2 gl = drawable.getGL().getGL2();      // get the OpenGL graphics context
      glu = new GLU();                         // get GL Utilities
      gl.glClearColor(1.0f, 1.0f, 1.0f, 0.0f); // set background (clear) color
      gl.glEnable(GL2.GL_DEPTH_TEST); // enables depth testing
      gl.glHint(GL2.GL_PERSPECTIVE_CORRECTION_HINT, GL2.GL_NICEST); // best perspective correction
      gl.glShadeModel(GL2.GL_SMOOTH); // blends colors nicely
   }

   /**
    * Handler for window re-size event. Also called when the drawable is 
    * first set to visible
    */
   public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {
      GL2 gl = drawable.getGL().getGL2();  // get the OpenGL 2 graphics context

      if (height == 0) height = 1;   // prevent divide by zero     
      float aspect = (float)width / height;

      //Set the view port (display area) to cover the entire window
      //gl.glViewport(0, 0, width/2, height/2);

      // Setup perspective projection, with aspect ratio matches viewport
      gl.glMatrixMode(GL2.GL_PROJECTION);  // choose projection matrix
      gl.glLoadIdentity();             // reset projection matrix
      glu.gluPerspective(45.0, aspect, 0.1, 100.0); // fovy, aspect, zNear, zFar

      // Enable the model-view transform
      gl.glMatrixMode(GL2.GL_MODELVIEW);
      gl.glLoadIdentity(); // reset
   }

   /**
    * Called by OpenGL for drawing
    */
   public void display(GLAutoDrawable drawable) {
      GL2 gl = drawable.getGL().getGL2();  // get the OpenGL 2 graphics context
      gl.glClear(GL2.GL_COLOR_BUFFER_BIT | GL2.GL_DEPTH_BUFFER_BIT); // clear color and depth buffers
      gl.glLoadIdentity();  // reset the model-view matrix 
     
      gl.glTranslatef(0, 0, -8f);
      
      int width = 700, height = 700;
      rotAnglex = Math.sin(counter);
      rotAnglez = Math.cos(counter);
      glu.gluLookAt(rotAnglex,1,rotAnglez,0,0,0,0,1,0);
      drawMan(gl);      
   }
   
   double armHoyde = 0.1;
   
   public double[][] kubePunktGenerator(double[][] endepunkter, int radius) {
	   double[][] nyePunkter = new double[8][3];
	   for(int i = 0; i < 8; i++) {
		   nyePunkter[i][0] = endepunkter[i/4][0]*radius;
	   }
	   
	   return nyePunkter;
   }
   
   public void drawMan(GL2 gl) {
	   
	   
	 //armer
	   //double[][] venstreArm = kubePunktGenerator({{1d,1d,1d}, {0,0,0}});
	   //drawCube(gl, venstreArm, 1);
	   //drawArm(4,5,6,7,gl,punkter);
	   	
	 //krop
	   
   }
   
   public void drawCube(GL2 gl, double[][] punkter, int radius ) {
	   
		 //Forside og bakside
		   drawSide(0,1,2,3,gl,punkter);
		   drawSide(4,5,6,7,gl,punkter);
		   	
		 //H�yre og venstre side
		   drawSide(0,1,5,4,gl,punkter);
		   drawSide(2,3,7,6,gl,punkter);
		   
		 //top og bunn
		   drawSide(2,6,5,1,gl,punkter);
		   drawSide(3,7,4,0,gl,punkter);
	   }
	   
   public void drawSide( int a, int b, int c, int d, GL2 gl, double[][] punkter){	   
	   gl.glColor3fv(colors[d],0);
	   gl.glBegin(GL2.GL_POLYGON);
	    gl.glVertex3dv(punkter[a],0);
	    gl.glVertex3dv(punkter[b],0);
	    gl.glVertex3dv(punkter[c],0);
	    gl.glVertex3dv(punkter[d],0);
	   gl.glEnd();
   }
   
   /** 
    * Called before the OpenGL context is destroyed. Release resource such as buffers. 
    */
   public void dispose(GLAutoDrawable drawable) { }
   
   public double counter = 0.1;
   
   private class RotateKeyListener extends KeyAdapter{
       public void keyPressed(KeyEvent e) {
           //accept any key    
           counter += 0.1;
           oving15.this.repaint();//repaint our canvas
       }
  }
   
   /** The entry main() method to setup the top-level JFrame with our OpenGL canvas inside */
   public static void main(String[] args) {
       GLCanvas canvas = new oving15();
       canvas.setPreferredSize(new Dimension(CANVAS_WIDTH, CANVAS_HEIGHT));
       
       final JFrame frame = new JFrame(); // Swing's JFrame or AWT's Frame
       frame.getContentPane().add(canvas);
       frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
       frame.setTitle(TITLE);
       frame.pack();
       frame.setVisible(true);      
   }
}
