package triangle;


import java.awt.*;
import javax.swing.*;

import com.jogamp.opengl.GL2;
import com.jogamp.opengl.glu.GLU;
import com.jogamp.opengl.glu.GLUquadric;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLEventListener;
import com.jogamp.opengl.awt.GLCanvas;


/**
 * NeHe Lesson #3 (JOGL 2 Port): Adding Colors (to Lesson #2 - Basic 2D).
 * @author Tomas Holt, based on code from Hock-Chuan Chua (May 2012)
 * @version October 2016
 */

/* Main class which extends GLCanvas. This means that this is a OpenGL canvas.
   We will use OpenGL commands to draw on this canvas.
   This implementation has no animation or user input.
*/
public class oving13 extends GLCanvas implements GLEventListener {
   // constants
   private static String TITLE = "�ving 13";
   private static final int CANVAS_WIDTH = 520;  // width of the drawable
   private static final int CANVAS_HEIGHT = 540; // height of the drawable
   
  
   // Setup OpenGL Graphics Renderer 
   private GLU glu;  // for the GL Utility
   
   /** Constructor to setup the GUI for this Component */
   public oving13() {
      this.addGLEventListener(this);
   }
   
// ------ Implement methods declared in GLEventListener (init,reshape,display,dispose)          

   /**
    * Called immediately after the OpenGL context is initialized. Can be used 
    * to perform one-time initialization. Run only once.
    */
   public void init(GLAutoDrawable drawable) {
      GL2 gl = drawable.getGL().getGL2();      // get the OpenGL graphics context
      glu = new GLU();                         // get GL Utilities
      gl.glClearColor(1.0f, 1.0f, 1.0f, 0.0f); // set background (clear) color
      gl.glEnable(GL2.GL_DEPTH_TEST); // enables depth testing
      gl.glHint(GL2.GL_PERSPECTIVE_CORRECTION_HINT, GL2.GL_NICEST); // best perspective correction
      gl.glShadeModel(GL2.GL_SMOOTH); // blends colors nicely
   }

   /**
    * Handler for window re-size event. Also called when the drawable is 
    * first set to visible
    */
   public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {
      GL2 gl = drawable.getGL().getGL2();  // get the OpenGL 2 graphics context

      if (height == 0) height = 1;   // prevent divide by zero     
      float aspect = (float)width / height;

      //Set the view port (display area) to cover the entire window
      //gl.glViewport(0, 0, width/2, height/2);

      // Setup perspective projection, with aspect ratio matches viewport
      gl.glMatrixMode(GL2.GL_PROJECTION);  // choose projection matrix
      gl.glLoadIdentity();             // reset projection matrix
      glu.gluPerspective(45.0, aspect, 0.1, 100.0); // fovy, aspect, zNear, zFar

      // Enable the model-view transform
      gl.glMatrixMode(GL2.GL_MODELVIEW);
      gl.glLoadIdentity(); // reset
   }

   /**
    * Called by OpenGL for drawing
    */
   public void display(GLAutoDrawable drawable) {
	   
      GL2 gl = drawable.getGL().getGL2();  // get the OpenGL 2 graphics context
      gl.glClear(GL2.GL_COLOR_BUFFER_BIT | GL2.GL_DEPTH_BUFFER_BIT); // clear color and depth buffers
      //gl.glLoadIdentity();  // reset the model-view matrix 
      
      float[] punkter = {0,2,0,1.5f,1.5f,0,2f,0,0,1.5f,-1.5f,0,0,-2f,0,-1.5f,-1.5f,0,-2f,0,0,-1.5f,1.5f,0};
      
      gl.glTranslatef(-2f, 0, -15f);
      gl.glRotatef(30, 0, 0, 0);
      glu.gluLookAt(-1,1,1,0,0,0,0,1,0);
      for(int i = 0; i < punkter.length;i+=3) {
	      gl.glTranslatef(punkter[i], punkter[i+1], punkter[i+2]); // translate left and into the screen
	      hus(gl);
      }
      
      GLUquadric quad = glu.gluNewQuadric();
      gl.glTranslatef(2f, 0.5f, 2f);
      //glu.gluSphere(quad, 1, 5, 5);
	   
   }
   
   public void hus(GL2 gl) {
	   gl.glBegin(GL2.GL_POINTS);
     	gl.glColor3f(0.0f, 0.0f, 0.0f); // Black
     	for(int i = 0; i < 10; i++) {
     		gl.glVertex3f(0.0f, i/10f, 0.0f);
     	}
	   gl.glEnd();
     
	   gl.glBegin(GL2.GL_LINE_LOOP); // Vindukarm
	      gl.glColor3f(0.0f, 0.0f, 0.0f); // Black
	      
	      for(float i=0; i<2*Math.PI; i += 0.1)
	      {
	    	  gl.glVertex3f(((float)(Math.cos(i)/3)+0.45f), ((float)(Math.sin(i)/6)-0.5f),0.0f);
	      }
	      
	   gl.glEnd();
	   
	   gl.glBegin(GL2.GL_TRIANGLES); // Tak
	      gl.glColor3f(0.9f, 0.5f, 0.2f); // Brown
	      
	      gl.glVertex3f(0.0f, 1.0f, 0.0f);
	      gl.glVertex3f(1.0f, 0.0f, 0.0f);
	      gl.glVertex3f(-1.0f, 0.0f, 0.0f);
	   gl.glEnd();
	   
	   gl.glBegin(GL2.GL_QUAD_STRIP); // Skorsteinspipe
	      gl.glColor3f(0.0f, 0.0f, 0.0f); // Black
	      
	      gl.glVertex3f(-0.4f, 0.0f, 0.0f);
	      gl.glVertex3f(-0.6f, 0.0f, 0.0f);
	      gl.glVertex3f(-0.4f, 1.0f, 0.0f);
	      gl.glVertex3f(-0.6f, 1.0f, 0.0f);
	      gl.glVertex3f(-0.7f, 1.0f, 0.0f);
	      gl.glVertex3f(-0.3f, 1.0f, 0.0f);
	      gl.glVertex3f(-0.7f, 1.15f, 0.0f);
	      gl.glVertex3f(-0.3f, 1.15f, 0.0f);
	      
	   gl.glEnd();
	   
	   gl.glBegin(GL2.GL_LINE_STRIP); // Bl� d�r
	      gl.glColor3f(0.0f, 0.0f, 1.0f); // Blue
	      
	      gl.glVertex3f(-0.5f, -1.0f, 0.0f);
	      gl.glVertex3f(0.0f, -1.0f, 0.0f);
	      gl.glVertex3f(-0.5f, -0.5f, 0.0f);
	      gl.glVertex3f(0.0f, -0.5f, 0.0f);
	      gl.glVertex3f(0.0f, -1.0f, 0.0f);
	      gl.glVertex3f(0.0f, -0.5f, 0.0f);
	      gl.glVertex3f(-0.5f, -1.0f, 0.0f);
	      gl.glVertex3f(-0.5f, -0.5f, 0.0f);
	   gl.glEnd();
	   
	   gl.glBegin(GL2.GL_LINES); // vindu innside
	      gl.glColor3f(0.0f, 0.0f, 0.0f); // Black
	      
	      gl.glVertex3f(0.1f, -0.5f, 0.0f);
	      gl.glVertex3f(0.8f, -0.5f, 0.0f);
	      gl.glVertex3f(0.45f, -0.33f, 0.0f);
	      gl.glVertex3f(0.45f, -0.66f, 0.0f);
	   gl.glEnd();
	   
	   gl.glBegin(GL2.GL_QUADS); // Red box
	      gl.glColor3f(1.0f, 0.0f, 0.0f); // Red
	      
	      gl.glVertex3f(-1.0f, 0.0f, 0.0f);
	      gl.glVertex3f(1.0f, 0.0f, 0.0f);
	      gl.glVertex3f(1.0f, -1.0f, 0.0f);
	      gl.glVertex3f(-1.0f, -1.0f, 0.0f);
	      
	   gl.glEnd();
	   
	   gl.glBegin(GL2.GL_TRIANGLE_FAN); // d�rmatte
	      gl.glColor3f(0.0f, 0.0f, 0.0f); // black
	      
	      gl.glVertex3f(-0.25f, -1.0f, 0.0f);
	      gl.glVertex3f(0.0f, -1.0f, 0.0f);
	      gl.glVertex3f(-0.125f, -1.1f, 0.2f);
	      gl.glVertex3f(-0.25f, -1.15f, 0.3f);
	      gl.glVertex3f(-0.375f, -1.1f, 0.2f);
	      gl.glVertex3f(-0.5f, -1.0f, 0.0f);
	      
	   gl.glEnd();
	   
	   gl.glBegin(GL2.GL_TRIANGLE_STRIP); // Plen
	      gl.glColor3f(0.1f, 1.0f, 0.1f); // green
	      
	      gl.glVertex3f(-1.0f, -1.0f, 0.0f);
	      gl.glVertex3f(-0.8f, -1.5f, 0.5f);
	      gl.glVertex3f(-0.5f, -1.0f, 0.0f);
	      gl.glVertex3f(0.0f, -1.5f, 0.5f);
	      gl.glVertex3f(0.5f, -1.0f, 0.0f);
	      gl.glVertex3f(1.0f, -1.5f, 0.5f);
	      
	   gl.glEnd();
   }
   
   /** 
    * Called before the OpenGL context is destroyed. Release resource such as buffers. 
    */
   public void dispose(GLAutoDrawable drawable) { }
   
   /** The entry main() method to setup the top-level JFrame with our OpenGL canvas inside */
   public static void main(String[] args) {
       GLCanvas canvas = new oving13();
       canvas.setPreferredSize(new Dimension(CANVAS_WIDTH, CANVAS_HEIGHT));
       
       final JFrame frame = new JFrame(); // Swing's JFrame or AWT's Frame
       frame.getContentPane().add(canvas);
       frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
       frame.setTitle(TITLE);
       frame.pack();
       frame.setVisible(true);      
   }
}
