/*
 * JOptionPaneTest.java   G.S. 2006-01-17
 *
 * Pr�ver ulike dialogbokser.
 */
import static javax.swing.JOptionPane.*;
import static java.lang.Double.*;
import javax.swing.ImageIcon;

class JOptionPaneTest {
  public static void main(String[] args) {
    String tall1Lest = showInputDialog("Tall 1: ");
    String tall2Lest = showInputDialog("Tall 2: ");
    double tall1 = parseDouble(tall1Lest);
    double tall2 = parseDouble(tall2Lest);
    double sum = tall1 + tall2;

    showMessageDialog(null, "Summen er " + (sum-2) + " kr");

    showMessageDialog(null, " Feil, summen er faktisk " + sum, "Lage egen tittel til tittellinja!", ERROR_MESSAGE); // kan bruke tall

    ImageIcon bilde = new ImageIcon("boss.gif");
	showMessageDialog(null, "Viser et selvvalgt bilde", "Eget ikon :o)", 0, bilde);


  }
}