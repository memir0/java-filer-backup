class PrintfTest {
	public static void main (String[] args){
		String tekst = "e";

		for (int i = 0; i < 5; i++) {
			int tall1 = i * 10;
			double tall2 = tall1 + 0.5;
			tekst += "e";  // en 'e' mer for hvert gjennoml�p

		    System.out.printf("%2d %3d %07.2f %13s\n", i, tall1, tall2, tekst);

		}

		double a = 0.0000056565;
		double b = 1345.675677;
		double c = 3445686768.45678;

		System.out.println("\n");
		System.out.printf("%10.2f %10.2g %10.2e \n", a ,a, a);
		System.out.printf("%10.2f %10.2g %10.2e \n", b, b, b);
		System.out.printf("%10.2f %10.2g %10.2e \n", c, c, c); // ikke nok plass til tallet

		java.util.Date n� = new java.util.Date();
		System.out.printf("\nAkkurat n� er klokka %1$tR, datoen er %1$tF\n", n�);
		System.out.printf("Akkurat n� er klokka %1$tH:%1$tM, datoen er %1$tY-%1$tm-%1$td\n", n�);
	}
}
/* Resultat av kj�ring:

 0   0 0000,50            ee
 1  10 0010,50           eee
 2  20 0020,50          eeee
 3  30 0030,50         eeeee
 4  40 0040,50        eeeeee


      0,00    5.7e-06   5.66e-06
   1345,68    1.3e+03   1.35e+03
3445686768,46    3.4e+09   3.45e+09

Akkurat n� er klokka 15:05, datoen er 2008-09-26
Akkurat n� er klokka 15:05, datoen er 2008-09-26

*/