import java.util.Scanner;

class ScanTest{
	public static void main(String[] args){
		Scanner scan = new Scanner(System.in);
		System.out.println("Skriv inn navn: ");
		String navn = scan.nextLine();
		System.out.println("Du har skrevet inn: " + navn);

		System.out.println("Skriv inn flere navn: ");

		String innlesteNavn = "";
		String tekst = scan.nextLine();

		Scanner scanFlere = new Scanner(tekst);
		while (scanFlere.hasNext()){
			innlesteNavn += scanFlere.next() + " ";
		}
		scan.close();

		System.out.println("Du skrev inn f�lgende navn:\n " + innlesteNavn);



	}
}

