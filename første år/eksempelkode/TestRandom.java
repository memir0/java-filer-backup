
import java.util.Random;
class TestRandom {
  public static void main(String[] args) {
    final int grense = 1000; // �nsker tall i intervallet [0, 99]
    final int fr� = 17; // fr�et b�r v�re et primtall
    //Random randomGen = new java.util.Random();  // uten fr�
	 Random randomGen = new java.util.Random(fr�);  // med fr�
	/*
	   Ved bruk av fr�: Trekker da de samme tilfeldinge tallene hver gang
	   programmet kj�res (s�kalte pseudotilfeldige tall.
	*/

    // Trekker fire heltall:
    int tall1 = randomGen.nextInt(grense);
    int tall2 = randomGen.nextInt(grense);
    int tall3 = randomGen.nextInt(grense);
    int tall4 = randomGen.nextInt(grense);
    System.out.println("Her er fire tilfeldige tall i intervallet [0.." + (grense - 1) + "]: " + tall1 + " " + tall2 + " " + tall3 + " " + tall4);

	// Trekk et tall av type Long
    Long tall5 = randomGen.nextLong();
    System.out.println("Et tilfelding Long tall: " + tall5);

	//Trekk heltall fra -500 til og med 500
    int tall6 = randomGen.nextInt(1001)-500;
	System.out.println("Her er 1 tilfeldig tall i intervallet [-500..500]: " + tall6);


  }
}
/* Kj�ring av programmet uten fr�:
Her er fire tilfeldige tall i intervallet [0..99]: 49 21 27 43
Et tilfelding Long tall: -5200892463858158659
Her er 1 tilfeldig tall i intervallet [-500..500]: 192

Kj�ring med fr�:
Her er fire tilfeldige tall i intervallet [0..99]: 76 20 94 16
Et tilfelding Long tall: 1530270151771565451
Her er 1 tilfeldig tall i intervallet [-500..500]: -199
*/
