class Ing{
	public static int a = 1;
	private int b = 3;


	public static void setA(int newValue){
		a = newValue;
	}

	public void setB(int newValue){
		b = newValue;
	}

	public String toString(){
		return a + " " + b;
	}
}

class TestIng{
	public static void main(String[] args){
		Ing en = new Ing();
		Ing to = new Ing();
		System.out.println(en);
		System.out.println(to);
		en.setA(100);
		to.setB(400);
		System.out.println(en);
		System.out.println(to);

	}
}