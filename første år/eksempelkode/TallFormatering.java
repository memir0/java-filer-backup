import java.util.Locale;
import java.text.NumberFormat;
class TallFormatering {
  public static void main(String[] args) {
    System.out.println("Svensk:");
    Locale svensk = new Locale("sv", "SE");
    Locale.setDefault(svensk);
    double tall = 0.757;
    NumberFormat tallformat = NumberFormat.getNumberInstance();
    NumberFormat prosentformat = NumberFormat.getPercentInstance();
    NumberFormat pengeformat = NumberFormat.getCurrencyInstance();
    System.out.println("Som desimaltall: " + tallformat.format(tall));
    System.out.println("Som prosent: " + prosentformat.format(tall));
    System.out.println("Som bel�p: " + pengeformat.format(tall));

    System.out.println("\nNorsk:");
    Locale norsk = new Locale("no", "NO");
    Locale.setDefault(norsk);
	tall = 0.757;
    tallformat = NumberFormat.getNumberInstance();
    prosentformat = NumberFormat.getPercentInstance();
    pengeformat = NumberFormat.getCurrencyInstance();
    System.out.println("Som desimaltall: " + tallformat.format(tall));
    System.out.println("Som prosent: " + prosentformat.format(tall));
    System.out.println("Som bel�p: " + pengeformat.format(tall));

	System.out.println("\nUSA:");
    Locale us = new Locale("us", "US");
    Locale.setDefault(us);
	tall = 0.757;
    tallformat = NumberFormat.getNumberInstance();
    prosentformat = NumberFormat.getPercentInstance();
    pengeformat = NumberFormat.getCurrencyInstance();
    System.out.println("Som desimaltall: " + tallformat.format(tall));
    System.out.println("Som prosent: " + prosentformat.format(tall));
    System.out.println("Som bel�p: " + pengeformat.format(tall));

  }
}
/* Kj�ring av programmet:
Svensk:
Som desimaltall: 0,757
Som prosent: 76%
Som bel�p: 0,76 kr

Norsk:
Som desimaltall: 0,757
Som prosent: 76%
Som bel�p: kr 0,76

USA:
Som desimaltall: 0.757
Som prosent: 76%
Som bel�p: USD 0.76

*/
