 /*Programmet analyserer en tekst ved � finne antall ord    og gjennomsnittlig ordlengde.
   Teksten og skilletegnene leses inn fra brukeren. */
import java.util.StringTokenizer;
import static javax.swing.JOptionPane.*;
class Tekstanalyse {
	public static void main(String[] args) {
		String tekst = showInputDialog("Teksten: ");
		String skilletegn = showInputDialog("Skilletegn: ");
		StringTokenizer analyse = new StringTokenizer(tekst, skilletegn);
		int antOrd = analyse.countTokens();
		int antBokst = 0;

	    while (analyse.hasMoreTokens()) {
			String ord = analyse.nextToken();
			antBokst += ord.length();
			System.out.println(ord);
		}
		if (antOrd > 0) {
			double gjsnitt = (double) antBokst / (double) antOrd;
			showMessageDialog(null, "Antall ord er " + antOrd + ".\nGjennomsnittling ordlengde er " + gjsnitt);
		} else {
			showMessageDialog(null, "Ingen ord skrevet inn.");
		}



		tekst = "e";
		for (int i = 0; i < 5; i++) {
			int tall1 = i * 10;
			double tall2 = tall1 + 0.5;
			tekst += "e";  // en 'e' mer for hvert gjennoml�p
			System.out.printf("%2d %3d %07.2f %13s\n", i, tall1, tall2, tekst);
		}

	}
}