/*
 * TestDataLeser.java  E.L. 2004-03-19
 *
 * Filen inneholder to klasser:
 * DataLeser: En klasse med generelle metoder for innlesing av tekst og tall.
 * TestDataLeser: En enkel testklient for klassen. Viser hvordan metodene brukes.
 */

import static javax.swing.JOptionPane.*;
class DataLeser {

  /*
   * Leser en tekst fra brukeren. Blank tekst godtas ikke.
   * Teksten "trimmes" f�r den returneres til klienten.
   */
  public static String lesTekst(String ledetekst) {
    String tekst = showInputDialog(ledetekst);
    while (tekst == null || tekst.trim().equals("")) {
      showMessageDialog(null, "Du m� oppgi data.");
      tekst = showInputDialog(ledetekst);
    }
    return tekst.trim();
  }

  /*
   * Leser et heltall fra brukeren. Desimaltall godtas ikke.
   */
  public static int lesHeltall(String ledetekst) {
    int tall = 0;
    boolean ok = false;
    do {
      String lestTekst = lesTekst(ledetekst);
      try {
        tall = Integer.parseInt(lestTekst);
        ok = true;
      } catch (NumberFormatException e) {
        showMessageDialog(null, "Ugyldig heltall.\n");
      }
    } while (!ok);
    return tall;
  }

  /*
   * Leser et desimaltall fra brukeren. Heltall godtas ogs�.
   */
  public static double lesDesimaltall(String ledetekst) {
    double tall = 0;
    boolean ok = false;
    do {
      String lestTekst = lesTekst(ledetekst);
      try {
        tall = Double.parseDouble(lestTekst);
        ok = true;
      } catch (NumberFormatException e) {
        showMessageDialog(null, "Ugyldig desimaltall.\n");
      }
    } while (!ok);
    return tall;
  }
}

/* Enkel testklient */
class TestDataLeser {
  public static void main(String[] args) {
    String tekst = DataLeser.lesTekst("Skriv en tekst: ");
    double desimaltall = DataLeser.lesDesimaltall("Skriv et desimaltall: ");
    int heltall = DataLeser.lesHeltall("Skriv et heltall: ");
    System.out.println("Du skrev dette: ");
    System.out.println(tekst);
    System.out.println(desimaltall);
    System.out.println(heltall);
  }
}