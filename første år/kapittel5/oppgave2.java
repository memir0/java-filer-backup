import static javax.swing.JOptionPane. *;

public class oppgave2 {
    public static void main(String[] args){
        //Array av Valuta objekter
        Valuta[] valutaArray= new Valuta[3];
        //Kronen som et object for expandiblity 
        Valuta nok = new Valuta("Norske kroner",1);
        //Dollar
        valutaArray[0] = new Valuta("Dollar", 9.70);
        //Euro
        valutaArray[1] = new Valuta("Euro", 8.34);
        //Svenske kroner
        valutaArray[2] = new Valuta("Svenske kroner", 0.92);
        
        //Input
        String input = showInputDialog("Til\n 1: dollar\n2: euro\n3: svenske kroner\n4: avslutt");
        int valutavalg = Integer.parseInt(input);
        if(valutavalg == 4){
            return;
        }
        input = showInputDialog("Antall kroner:");
        int mengde = Integer.parseInt(input);
        //Delt på 100 for å få rett desimaltall
        showMessageDialog(null, nok.converter(valutaArray[valutavalg-1], mengde)/100 + " " + valutaArray[valutavalg-1].navn);

        input = showInputDialog("Fra\n 1: dollar\n2: euro\n3: svenske kroner\n4: avslutt");
        valutavalg = Integer.parseInt(input);
        if(valutavalg == 4){
            return;
        }
        input = showInputDialog("Antall " + valutaArray[valutavalg-1].navn + ": ");
        mengde = Integer.parseInt(input);
        showMessageDialog(null, valutaArray[valutavalg-1].converter(nok, mengde)/100 + " Kroner");
    }
}

class Valuta {
    private double kroneverdi;
    String navn;

    //Constructor
    public Valuta(String navn, double kroneverdi){
        this.kroneverdi = kroneverdi;
        this.navn = navn;
    }
    
    public double converter(Valuta temp, int mengde){
        return(Math.round(this.kroneverdi/temp.getKroneverdi()*mengde*100));
    }

    public double getKroneverdi(){
        return kroneverdi;
    }
}