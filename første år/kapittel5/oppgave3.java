import static javax.swing.JOptionPane. *;
//import java.util.Random;

public class oppgave3 {
    public static void main(String[] args){
        int rundenr = 0;
        //konsturerer de to spillerne
        Spiller spiller1 = new Spiller(1);
        Spiller spiller2 = new Spiller(2);
        
        //Kjører helt til en av spillerne treffer 100 poeng
        while(spiller1.erFerdig() && spiller2.getSumPoeng() < 100){
            spiller1.kastTerningen();
            spiller2.kastTerningen();
            rundenr++;
        }
        
        if(spiller1.getSumPoeng() < 100){
            //Hvis begge når 100 poeng samtidig vinner spiller 2
            showMessageDialog(null, "Spiller 1 vant" + "\n Rundenummer: " + rundenr);
        } else {
            //Hvis begge når 100 poeng samtidig vinner spiller 2
            showMessageDialog(null, "Spiller 2 vant" + "\n Rundenummer: " + rundenr);
        }
    }
}

class Spiller {
    //Random tallgenerator
    java.util.Random terning = new java.util.Random();

    //spiller nummer og poengsum
    public int poengsum = 0;
    int Spiller;

    //Constructor
    public Spiller(int Spiller){
        this.Spiller = Spiller;
    }

    public void kastTerningen(){
        //int tall = (int)(Math.random()*7+1);
        int tall = terning.nextInt(6)+1;
        if(tall == 1){ 
            //hvis man ruller 1 blir poengsummen satt til 0
            poengsum = 0;
        }
        else{
            poengsum = poengsum + tall;
            //for å se score mens den kjører
            System.out.println(Spiller + ": " + poengsum);
        }
    }
    
    public int getSumPoeng(){
        return poengsum;
    }

    public boolean erFerdig(){
        return poengsum < 100;
    } 
}