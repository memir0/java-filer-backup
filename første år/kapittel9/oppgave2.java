public class oppgave2{
    public static void main(String args[]){
        Temperaturer january = new Temperaturer(31);
        january.fillTableRandom();
        System.out.println(january.midTempWhole());
        System.out.println(january.interval(10, Integer.MAX_INT));
    }
}

class Temperaturer{
    int days = 0;
    float tempTable[][];

    public Temperaturer(int days){
        //konstruerer objektet avhengig av antall dager i måneden
        tempTable = new float[days][24];
        this.days = days;
    }

    float midTempDay(int day){
        float totalTemp = 0; 
        //For hver time i dagen legg til temperaturen til totalTemp
        for(int i = 0; i < 24; i++){
            totalTemp += tempTable[day][i];
        }
        return totalTemp/24;
    }
    float[] midTempDays(){
        float[] templist = new float[days];
        for(int i = 0; i < days; i++){
            templist[i] = midTempDay(i);
        }
        return templist;
    }

    float midTempHour(int hour){
        float totalTemp = 0; 
        //For antall dager legg til temperaturen til totalTemp
        for(int i = 0; i < days; i++){
            totalTemp += tempTable[i][hour];
        }
        return totalTemp/days;
    }
    float[] midTempHours(){
        float[] templist = new float[24];
        for(int i = 0; i < 24; i++){
            templist[i] = midTempHour(i);
        }
        return templist;
    }

    float midTempWhole(){
        float totalTemp = 0; 
        //For antall dager finn gjennomsnittstemperaturen for dagen og legg til i totalTemp
        for(int i = 0; i < days; i++){
            totalTemp += midTempDay(i);
        }
        return totalTemp/days;
    }

    int interval(int low, int hi){
        int count= 0;
        //for antall dager sjekk om gjennomsnittstemperaturen er innenfor intervallet
        for(int i = 0; i < days; i++){
            float temp = midTempDay(i);
            if(temp > low && temp < hi){
                count++;
            }
        }
        //returnerer antall dager som er innenfor intervallet
        return count;
    }

    void fillTableRandom(){
        java.util.Random rand = new java.util.Random();

        //Fyller tabellen med tilfeldige temperaturer mellom 0 og 9
        for(int i= 0; i < days; i++){
            for(int j = 0; j < 24; j++){
                tempTable[i][j] = rand.nextInt(10);
            }
        }
    }
}