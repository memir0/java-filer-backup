import java.util.ArrayList;
import java.util.Arrays;

class BrokRegning{
    private int teller, nevner, resultatTeller, resultatNevner;

    public static void main(String[] args){
        BrokRegning brok = new BrokRegning(12,27);
        BrokRegning brok2 = new BrokRegning(13,22);
        //BrokRegning brok = new BrokRegning(1,1);
        //BrokRegning brok2 = new BrokRegning(7,49);

        //Addering Test
        brok.summer(brok2);
        int[] resultat = brok.getResultat();
        if(resultat[0] == 205 && resultat[1] == 198){
            System.out.println("Adderingen var vellykket!");
        } else {
            System.out.println("Noe gikk galt med adderingen :(");
            System.out.println("Fikk " + resultat[0] + "/" + resultat[1]);
            System.out.println("Forventet 205/198");
        }

        //Subtrahere test
        brok.subtrahere(brok2);
        resultat = brok.getResultat();
        if(resultat[0] == -29 && resultat[1] == 198){
            System.out.println("Subtraheringen var vellykket!");
        } else {
            System.out.println("Noe gikk galt med subtraheringen :(");
            System.out.println("Fikk " + resultat[0] + "/" + resultat[1]);
            System.out.println("Forventet -29/198");
        }

        //Dividere test
        brok.dividere(brok2);
        resultat = brok.getResultat();
        if(resultat[0] == 88 && resultat[1] == 117){
            System.out.println("Divideringen var vellykket!");
        } else {
            System.out.println("Noe gikk galt med divideringen :(");
            System.out.println("Fikk " + resultat[0] + "/" + resultat[1]);
            System.out.println("Forventet 88/117");
        }

        //Multiplisere test
        brok.multiplisere(brok2);
        resultat = brok.getResultat();
        if(resultat[0] == 26 && resultat[1] == 99){
            System.out.println("Multipliseringen var vellykket!");
        } else {
            System.out.println("Noe gikk galt med multipliseringen :(");
            System.out.println("Fikk " + resultat[0] + "/" + resultat[1]);
            System.out.println("Forventet 26/99");
        }
    }

    BrokRegning(int teller, int nevner){
        if(nevner == 0){
            throw new IllegalArgumentException("Nevner kan ikke være 0");
        } else{
            this.nevner = nevner;
            this.teller = teller;
        }
    }

    BrokRegning(int teller){
        this.teller = teller;
        this.nevner = 1;
    }

    int[] getBrok(){
        return new int[] {teller, nevner};
    }

    int[] getResultat(){
        return new int[] {resultatTeller, resultatNevner};
    }

    void summer(BrokRegning innBrok){
        int[] brok1 = getBrok();
        int[] brok2 = innBrok.getBrok();
        
        resultatNevner = brok1[1]*brok2[1];
        resultatTeller = brok1[0]*brok2[1] + brok1[1]*brok2[0];
        forkortBrok();
    }

    void subtrahere(BrokRegning innBrok){
        int[] brok1 = getBrok();
        int[] brok2 = innBrok.getBrok();
        
        resultatNevner = brok1[1]*brok2[1];
        resultatTeller = brok1[0]*brok2[1] - brok1[1]*brok2[0];
        forkortBrok();
    }

    void multiplisere(BrokRegning innBrok){
        int[] brok1 = getBrok();
        int[] brok2 = innBrok.getBrok();
        
        resultatNevner = brok1[1]*brok2[1];
        resultatTeller = brok1[0]*brok2[0];
        forkortBrok();
    }

    void dividere(BrokRegning innBrok){
        int[] brok1 = getBrok();
        int[] brok2 = innBrok.getBrok();
        
        resultatNevner = brok1[1]*brok2[0];
        resultatTeller = brok1[0]*brok2[1];
        forkortBrok();
    }

    void forkortBrok(){
        int minsteTall;
        boolean tellerErNegativ = false, nevnerErNegativ = false;
        if(resultatTeller < 0){
            tellerErNegativ = true;
            resultatTeller = Math.abs(resultatTeller);
        }
        if(resultatNevner < 0){
            nevnerErNegativ = true;
            resultatNevner = Math.abs(resultatNevner);
        }
        if(resultatTeller < resultatNevner){
            minsteTall = resultatTeller;
        } else {
            minsteTall = resultatNevner;
        }
        
        ArrayList<Integer> muligeDividender = primtallListe(minsteTall);
        
        for(int i = 0; i < muligeDividender.size(); i++){
            int tempInt = muligeDividender.get(i);
            
            while(resultatTeller%tempInt == 0 && resultatNevner%tempInt == 0){
                resultatTeller = resultatTeller/tempInt;
                resultatNevner = resultatNevner/tempInt;
            }
        }
        if(tellerErNegativ){
            resultatTeller *= -1;
        }
        if(nevnerErNegativ){
            resultatNevner *= -1;
        }
    }

    ArrayList<Integer> primtallListe(int tall){
        ArrayList<Integer> returListe = new ArrayList<Integer>();
        boolean[] tallListe = new boolean[tall+1];
        Arrays.fill(tallListe, true);
        
        for(int i = 2; i < tall+1; i++){
            if(tallListe[i]){
                if(erPrimtall(i)){
                    int j = i*2;
                    while(j < tall){
                        tallListe[j] = false;
                        j += i;
                    }
                    returListe.add(i);
                }
            }
        }

        return returListe;
    }

    boolean erPrimtall(int tall){
        if(tall%2==0){
            return true;
        }
        else{
        for(int i = 3; i < tall; i +=2){
            if(tall%i==0){
                return false;
            }
        }
        return true;
        }
    }
}