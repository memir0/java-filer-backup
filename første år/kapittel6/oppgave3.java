import static javax.swing.JOptionPane. *;

public class oppgave3 {
    public static void main(String[] args){
        //konstruksjon av objeketet
        MinRandom test = new MinRandom();
        //output
        System.out.println(test.nesteHeltall(4,100));
        System.out.println(test.nesteDesimaltall(2.22,23.22));
    }
}

class MinRandom{
    //random numgen
    private java.util.Random rand = new java.util.Random();

    public MinRandom(){
        //tom konstrukter
    }

    public int nesteHeltall(int nedre, int ovre){
        //finner et tall mellom de og legger til nedre for å få rett tall
        return rand.nextInt(ovre-nedre) + nedre;
    }

    public double nesteDesimaltall(double nedre, double ovre){
        return rand.nextDouble()*(ovre-nedre) + nedre;
    }
}