import static javax.swing.JOptionPane. *;

public class oppgave1 {
    public static void main(String[] args){
        //input
        String input = showInputDialog("fagnavn:");
        String fagnavn = input;
        input = showInputDialog("eksamentype, 1=muntlig, 2=skriftlig, 3=prosjekt: ");
        int eksamenType = Integer.parseInt(input);
        double eksamenLengde;
        if(eksamenType == 3){
            eksamenLengde = 0;
        }
        else{
        input = showInputDialog("eksamen lengde: ");
        eksamenLengde = Double.parseDouble(input);
        }
        int antallBesvarelser;
        if(eksamenType == 1){
            antallBesvarelser = 0;
        }
        else{
            input = showInputDialog("antall besvarelser: ");
            antallBesvarelser = Integer.parseInt(input); 
        }

        //konstruksjon av objekt
        Sensurering eksamen = new Sensurering(fagnavn, eksamenType, eksamenLengde, antallBesvarelser);

        //output med metoden antallLonnetTimer()
        showMessageDialog(null, "antall lønnet timer: " + eksamen.antallLonnetTimer());
    }
}

class Sensurering {
    public String fagnavn;
    // 1=muntlig, 2=skriftlig, 3=prosjekt
    public int examenType; 
    public double examenLengde;
    public int antallBesvarelser;
    //Constructor
    public Sensurering(String fagnavn, int examenType, double examenLengde, int antallBesvarelser){
        this.fagnavn = fagnavn;
        this.examenLengde = examenLengde;
        this.examenType = examenType;
        this.antallBesvarelser = antallBesvarelser;
    }
    
    public double antallLonnetTimer(){
        //beregner antall timer avhengig av eksamenstype
        if(examenType == 1){
            return 3+examenLengde;
        }
        else if(examenType==2){
            //referer til en hjelpe klasse
            return skriftligEksamen();
        }
        else{
            return antallBesvarelser*8;
        }
    }

    private double skriftligEksamen(){
        //regner ut skritlig eksamenstimer
        if(antallBesvarelser <= 10){
            return 0.15*examenLengde*antallBesvarelser+3;
        }
        else{
            return 0.15*examenLengde*10 + 0.1*examenLengde*(antallBesvarelser-10)+3;
        }
    }
}