public class monteTest{
    public static void main (String[] arg) {        
        int runder = 20;
        java.util.Random rand = new java.util.Random();
        int temp;
        int posisjon = 1;
        int[] monster = new int[runder];
        int[] posTab  = new int[runder];
        
        //Genererer mønster
		for(int i = 0; i < runder; i++){
            temp = rand.nextInt(6);
            if(temp == 1){
                monster[i] = 7;
            } else {
                if(posisjon == 2){
                    temp = rand.nextInt(2);
                    monster[i] = temp+2;
                    if(temp == 0){
                        posisjon = 1;
                    } else {
                        posisjon = 3;
                    }
                }
                else if(posisjon == 3){
                    temp = rand.nextInt(2);
                    if(temp == 0){
                        monster[i] = 6;
                        posisjon = 1;
                    } else {
                        monster[i] = 4;
                        posisjon = 2;
                    }
                }
                else{
                    temp = rand.nextInt(2);
                    if(temp == 0){
                        monster[i] = 5;
                        posisjon = 3;
                    } else {
                        monster[i] = 1;
                        posisjon = 2;
                    }
                }
            }
            posTab[i] = posisjon;
        }

        //Kjører mønsteret
        for(int i = 0; i < runder; i++){
            System.out.println("Monster " + i + ": " + monster[i]);
            System.out.println("Posisjon:" + posTab[i]);
        }
    }
}