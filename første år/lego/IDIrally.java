/* Speedinger v0.1
    Speedingo's ultra raske dragracer
*/
import lejos.hardware.motor.*;
import lejos.hardware.lcd.*;
import lejos.hardware.port.Port;
import lejos.hardware.Brick;
import lejos.hardware.BrickFinder;
import lejos.hardware.ev3.EV3;
import lejos.hardware.Keys;
import lejos.robotics.SampleProvider;
import lejos.hardware.sensor.*;
import lejos.hardware.Sound;
import java.io.File;

public class HitraTunnel{
    static int teller = 0;

	public static void main (String[] arg) throws Exception  {
		try{

			Thread sound = new Thread(() -> {
				while(true){
					Sound.setVolume(100);
					File file = new File("tokyo.wav");
					Sound.playsample(file, 100);
				}
			});

			Brick brick = BrickFinder.getDefault();
			Port s1 = brick.getPort("S1"); // fargesensor
            Port s2 = brick.getPort("S2"); // trykksensor
            
			EV3 ev3 = (EV3) BrickFinder.getLocal();
			TextLCD lcd = ev3.getTextLCD();
			Keys keys = ev3.getKeys();

			SampleProvider trykksensor = new EV3TouchSensor(s2);
			float[] trykkSample = new float[trykksensor.sampleSize()]; // tabell som inneholder avlest verdi
			

			/* Definerer en fargesensor og fargeAvleser */
			EV3ColorSensor fargesensor = new EV3ColorSensor(s1); // ev3-fargesensor
			SampleProvider fargeLeser = fargesensor.getMode("RGB");  // svart = 0.01..
			//SampleProvider fargeLeser = fargesensor.getColorIDMode();  // OBS: Funker ikke - får arrayfeil. can identify 8 unique colors (NONE, BLACK, BLUE, GREEN, YELLOW, RED, WHITE, BROWN).

			float[] fargeSample = new float[fargeLeser.sampleSize()];  // tabell som innholder avlest verdi

			boolean fortsett  = true;
            
            Motor.A.setSpeed(900);
		    Motor.B.setSpeed(900); // sett hastighet (toppfart = 900)

			while(fortsett) {
				//Fargesensor
				fargeLeser.fetchSample(fargeSample, 0);  // hent verdi fra fargesensor
				lcd.drawString("Farge: " + fargeSample[0], 0, 3);

				if(fargeSample[0] > 0.0035){
					backward(50);
					turn();
				} else{
					forward(50);
				}
				
				//Hvis man trykker på killswitchen ender programmet:
	  			if (trykkSample != null && trykkSample.length > 0){
	  				trykksensor.fetchSample(trykkSample, 0);
	  				if (trykkSample[0] > 0){
		  				System.out.println("Avslutter");
		 			 	fortsett = false;
	 				}
  	 			}
			} // while

		}catch(Exception e){
			System.out.println("Feil: " + e);
			Thread.sleep(10000);
		} //try-catch
	} // main

	//Funksjoner
	static void forward(int time){
        if(forwardSatt){
            Motor.A.backward();
            Motor.B.backward();
            forwardSatt = false;
        }
		Thread.sleep(time);
    }

    static void backward(int time){
		Motor.A.forward();
        Motor.B.forward();
        Thread.sleep(time);
        forwardSatt = true;
    }
    
    static void turn(){
        //litt til høyre
        if(teller > 10){
            Motor.A.backward();
            Motor.B.forward();
            Thread.sleep(10);
        }

        //venstre
        if(teller <= 10 && teller > 100){
            Motor.A.forward();
            Motor.B.backward();
            Thread.sleep(10);
            teller++;
        } else { 
            teller = 0;
        } 
        forwardSatt = true;
    }
} // class SensorTest