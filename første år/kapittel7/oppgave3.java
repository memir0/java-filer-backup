import java.util.Arrays;

public class oppgave3{
    public static void main(String args[]){
        String text = "ddddbbbbbcccc. !å";
        System.out.println(text);
        Tekstanalyse test = new Tekstanalyse(text);

        test.printAlt();

        System.out.println("antall forskjellige bokstaver: " + test.antallForskjelligeBokstaver());
        System.out.println("Prosent karakterer som ikke er bokstaver: " + test.prosentKaraktererSomIkkeErBokstaver() + "%");
        System.out.println("Antall e-er: " + test.antallBokstav('e'));
        System.out.println("Mest forekommende bokstaver: ");
        char[] forekommendeBokstaver = test.mestForekommende();
        int i = 0;
        while(forekommendeBokstaver[i] != 0){
            System.out.println(forekommendeBokstaver[i]);
            i++;
        }
    }
}

class Tekstanalyse {
    String tekst;
    //array som tellet antall occurrences
    int counter[] = new int[30];
    
    Tekstanalyse(String tekst){
        this.tekst = tekst;
        for(int i= 0; i < tekst.length(); i++){
            char tegn = tekst.charAt(i); //går gjennom alle bokstaver i stringen
            int charCode = (int)(tegn); //gjør om char til dens desmial kode

            if(64 < charCode && charCode < 91){ //Hvis det er en stor bokstav A-Z
                counter[charCode-65]++;
            }
            else if(96 < charCode && charCode < 123){ //Hvis det er en liten bokstav a-z
                counter[charCode-97]++;
            }
            
            else if(charCode == 229 || charCode == 197){ //Å eller å
                counter[28]++;
            }
            else if(charCode == 248 || charCode == 216){ //Ø eller ø
                counter[27]++;
            }
            else if(charCode == 230 || charCode == 198){ //Æ eller æ
                counter[26]++;
            }
            
            else{ // Alt annet
                counter[29]++;
            }
        }
    }

    int antallForskjelligeBokstaver(){
        int returtall = 0;
        for (int i = 0; i < 29; i++){
            if(counter[i] != 0){ 
                returtall++;
            }
        }
        return returtall;
    }

    int antallBokstaver(){
        //plusser alle tallene i arrayen
        int total = 0;
        for(int i = 0; i < 29; i++){
            total += counter[i];
        }
        return total;
    }

    float prosentKaraktererSomIkkeErBokstaver(){
        return (float)(counter[29])/(float)(antallBokstaver())*100;
    }

    int antallBokstav(char x){
        char temp = Character.toUpperCase(x);
        return counter[(int)(temp)-65];
    }

    char[] mestForekommende(){
        int index = 0;
        char[] returnArray = new char[30];
        returnArray[0] = 'A';
        
        for(int i = 1; i < 29; i++){
            if(counter[(int)(returnArray[0])-65] < counter[i]){
                //resetter arrayen med den nye 
                index = 1;
                returnArray = new char[30];
                returnArray[0] = (char)(i+65);
            }
            else if(counter[(int)(returnArray[0])-65] == counter[i]){
                returnArray[index] = (char)(i+65);
                index++;
            }
        }
        return returnArray;
    }

    void printAlt(){
        for(int i= 0; i < 30; i++){
            System.out.print((char)(i+65) + ": ");
            System.out.println(counter[i]);
        }
    }
}