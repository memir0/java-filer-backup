public class oppgave1{
    public static void main(String[] args){        
        //Konstruksjon av random objekt
        java.util.Random random = new java.util.Random();

        //Antall tall
        int antallTall = 10;

        //Array som oppbevarer antall treff på de ulike tallene
        int antall[] = new int[antallTall];

        for(int i = 0; i < 1000; i++){
            //plusser på en til et sted i arrayet med tilfeldig index
            antall[random.nextInt(antallTall)]++;
        }

        //Output
        for(int i = 0; i < antallTall; i++){
            System.out.print(i + ": " + antall[i] + " ");

            //Printer ut stjerner for hver tiende gjenntakelse av tallet
            //Trykkfeil i boken btw (1/100)
            for(int j = 0; j < Math.round((float)(antall[i])/10); j++){
                System.out.print("*");
            }
            System.out.println("");
        }
        
    }
}