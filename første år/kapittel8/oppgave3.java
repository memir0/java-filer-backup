import java.util.StringTokenizer;

public class oppgave3{
    public static void main(String[] args){        
        NyString test = new NyString("denne setningen er for å teste programmet! Jeg legger til ekstra tekst her. Tror du denne kommer til å fungere?");
        System.out.println("Gjennomsnittlig ordlengde: " + test.gjennomsnittOrdlengde());
        System.out.println("Antall ord: " + test.antallOrd());
        System.out.println("Antall settninger: " + test.antallSetninger());
        System.out.println("Antall ord per periode: " + test.antallOrdPerPeriode());
        System.out.println(test.ordSkifter("er", "ber"));
        System.out.println(test.hentTekstStor());
    }
}

final class NyString {
    //Immutable variabel
    private String text;

    public NyString(String text){
        this.text = text;
    }

    int antallOrd(){
        int antall = 0;
        boolean taNeste = true;

        for(int i = 0; i < text.length(); i++){
            //Lagrer charen for å bare kalle charAt() en gang
            char tempChar = text.charAt(i);

            if(tempChar == ' '){ //Hvis charen er et mellomrom
                taNeste = true;
            }
            else if(taNeste == true){ //Hvis den forrige charen var mellomrom starter et nytt ord
                antall++;
                taNeste = false;
            }
        }
        return antall;
    }

    float gjennomsnittOrdlengde(){
        float antallBokstaver = 0;

        for(int i = 0; i < text.length(); i++){
            //Lagrer charen for å bare kalle charAt() en gang
            char tempChar = text.charAt(i);

            if(tempChar != ' ' && tempChar != ',' && tempChar != '.' && tempChar != ':' && tempChar != '!'){ //Hvis charen ikke er et skille
                antallBokstaver++;
            }
        }
        return antallBokstaver/(float)(antallOrd());
    }

    int antallSetninger(){
        int antall = 1;

        for(int i = 0; i < text.length()-1; i++){ //-1 siden den ikke skal telle med avsluttende skilletegn
            //Lagrer charen for å bare kalle charAt() en gang
            char tempChar = text.charAt(i);

            if(tempChar == '.' || tempChar == ':' || tempChar == '!' || tempChar == '?'){ //Hvis charen er et skille
                antall++;
            }
        }
        return antall;
    }

    float antallOrdPerPeriode(){
        return (float)(antallOrd())/(float)(antallSetninger());
    }

    String ordSkifter(String fra, String til){
        //deler opp text i ord delt med mellomrom
        StringTokenizer deltText = new StringTokenizer(text, " ");
        String nyTekst = "";

        while(deltText.hasMoreTokens()){
            String tempToken = deltText.nextToken();

            if(tempToken.equals(fra)){ //hvis ordet er ordet vi leter etter erstattes det
                nyTekst += " " + til;
            } else { //hvis ikke legges det opprinelige ordet til vårt nye ord
                nyTekst += " " + tempToken;
            }
        }
        //oppdaterer text variablen
        return nyTekst.substring(1);
    }

    String hentTekst(){
        return text;
    }

    String hentTekstStor(){
        return text.toUpperCase();
    }
}