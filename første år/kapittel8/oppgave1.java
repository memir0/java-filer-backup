public class oppgave1{
    public static void main(String[] args){        
        NyString test = new NyString("denne setningen er for aa teste programmet");
        System.out.println(test.forkort());
        System.out.println(test.fjernBokstav('e'));
    }
}

final class NyString {
    //Immutable variabel
    private final String text;

    public NyString(String text){
        this.text = text;
    }

    String forkort() {
        String kortOrd = "";
        char tempChar;
        /* Hvis programmet støter på et mellomrom tar den neste char 
           med mindre den også er et mellomrom */
        boolean taNeste = true;

        for(int i = 0; i < text.length(); i++){
            //Lagrer charen for å bare kalle charAt() en gang
            tempChar = text.charAt(i);

            if(tempChar == ' '){ //Hvis charen er et mellomrom
                taNeste = true;
            }
            else if(taNeste == true){
                kortOrd += tempChar; //legger til charen til ordet vårt
                taNeste = false; //resetter boolen
            }
        }
        return kortOrd;
    }

    String fjernBokstav(char bokstav){
        String kortOrd = "";
        char tempChar;
        for(int i = 0; i< text.length(); i++){
            tempChar = text.charAt(i);
            if(tempChar != bokstav){
                kortOrd += tempChar;
            }
        }
        return kortOrd;
    }
}