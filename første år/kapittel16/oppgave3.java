import java.io.*;
import javax.swing.JOptionPane.*;
import java.util.ArrayList;

class oppgave3{
    public static void main(String[] args) throws IOException{ //kan droppe throw IOException
        ArrayList<String> linjer= lesFil("saldo.txt");

        double saldo = Double.parseDouble(linjer.get(0));
        System.out.println(saldo);

        linjer = lesFil("transaksjon.txt");

        for(int i = 0; i < linjer.size()-1; i++){
            String transaksjon = linjer.get(i);
            System.out.println(transaksjon);
            if(transaksjon.charAt(0) == 'U'){
                saldo -= Double.parseDouble(transaksjon.substring(2));
            }
            else if(transaksjon.charAt(0) == 'I'){
                saldo += Double.parseDouble(transaksjon.substring(2));
            }
        }
        System.out.println(saldo);

        //Skriver til filen
        FileWriter forbindelse = new FileWriter("saldo.txt");
        PrintWriter skriver = new PrintWriter(new BufferedWriter(forbindelse));
        skriver.print(saldo);
        skriver.close();        
    }

    static ArrayList<String> lesFil(String filnavn){
        try{
            FileReader leseforbTilFil = new FileReader(filnavn);
            BufferedReader leser = new BufferedReader(leseforbTilFil);
            ArrayList<String> linjer = new ArrayList<String>();
            String tempLinje = "";
            while(tempLinje != null){
            tempLinje = leser.readLine();
            linjer.add(tempLinje);
            }
            leser.close();
            return linjer;
        }
        catch(Exception e) {
            System.out.println("ERROR");
            return null;
        }
        //Mer spesifike catches
        //try with resources
    }
}