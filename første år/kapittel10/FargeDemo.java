import java.awt.Graphics;
import java.awt.Color;
import javax.swing.JFrame;
import javax.swing.JPanel;

class Vindu extends JFrame {
    private static final long serialVersionUID = 1L;

    public Vindu(String tittel) {
        setTitle(tittel); //Vindu tittel
        setSize(500, 250); //Bredde og høyde
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        Tegning tegningen = new Tegning();
        add(tegningen);
    }
}

class Tegning extends JPanel {
    private static final long serialVersionUID = 1L;

    public void paintComponent(Graphics tegneflate) {
        super.paintComponent(tegneflate); //Husk denne!
        tegneflate.drawString("Alle standardfargene", 40, 30);
        Color[] farger = {Color.BLACK, Color.GRAY, Color.ORANGE, Color.YELLOW, Color.BLUE, Color.GREEN, Color.PINK, Color.CYAN, Color.LIGHT_GRAY, Color.RED, Color.DARK_GRAY, Color.MAGENTA, Color.WHITE};
        for(int i = 0; i < farger.length; i++){
            tegneflate.setColor(farger[i]);
            tegneflate.fillRect(40 + 30*i, 50, 30, 120);
        }
    }
}

class FargeDemo {
    public static void main(String[] args){
        Vindu etVindu = new Vindu("Standardfargene i klassen Color");
        etVindu.setVisible(true);
    }
}