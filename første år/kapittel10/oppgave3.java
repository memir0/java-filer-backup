import java.awt.Graphics;
import java.awt.Color;
import javax.swing.JFrame;
import javax.swing.JPanel;

class Vindu extends JFrame {
    public Vindu(String tittel) {
        setTitle(tittel); //Vindu tittel
        setSize(400, 300); //Bredde og høyde
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        Tegning tegningen = new Tegning();
        add(tegningen);
    }
}

class Tegning extends JPanel {
    public void paintComponent(Graphics tegneflate) {
        super.paintComponent(tegneflate);
        java.util.Random rand = new java.util.Random();
        int forrigeX = 0, forrigeY = 0, tempX, tempY;
        boolean flip = true;

        for(int i = 0; i < 11; i++){
            tempX = rand.nextInt(360) + 20;
            tempY = rand.nextInt(230) + 20;
            if(i != 0){
                tegneflate.drawLine(forrigeX, forrigeY, tempX, tempY);
            }

            if(flip){
                tegneflate.fillOval(tempX-10, tempY-10, 20, 20);
                flip = !flip;
                System.out.print("true");
            } else{
                tegneflate.drawOval(tempX-10, tempY-10, 20, 20);
                flip = !flip;
                System.out.print("false");
            }
            forrigeX = tempX;
            forrigeY = tempY;
        }
    }
}

class oppgave3 {
    public static void main(String[] args){
        Vindu etVindu = new Vindu("Standardfargene i klassen Color");
        etVindu.setVisible(true);
    }
}