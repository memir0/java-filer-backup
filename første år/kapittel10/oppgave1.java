import java.awt.Graphics;
import java.awt.Color;
import javax.swing.JFrame;
import javax.swing.JPanel;

class Vindu extends JFrame {
    private static final long serialVersionUID = 1L;

    public Vindu(String tittel) {
        setTitle(tittel); //Vindu tittel
        setSize(400, 300); //Bredde og høyde
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        Tegning tegningen = new Tegning();
        add(tegningen);
    }
}

class Tegning extends JPanel {
    private static final long serialVersionUID = 1L;

    public void paintComponent(Graphics tegneflate) {
        super.paintComponent(tegneflate);
        tegneflate.setColor(Color.YELLOW); 
        tegneflate.fillOval(0, 0, 250, 250);
        tegneflate.setColor(Color.BLACK);
        tegneflate.drawArc(60, 100, 100, 100, 180, 180);
        tegneflate.drawOval(40, 40, 40, 40);
        tegneflate.drawOval(150, 40, 40, 40);
    }
}

public class oppgave1 {
    public static void main(String[] args){
        Vindu etVindu = new Vindu("Standardfargene i klassen Color");
        etVindu.setVisible(true);
    }
}