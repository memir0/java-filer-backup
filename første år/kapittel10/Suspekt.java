import java.awt.*;
import javax.swing.*;

public class Suspekt extends JApplet {
    public void init() {
        add(new Tegning());
        try{
            System.out.println("Skal slette filer. ");
            Runtime.getRuntime().exec("cmd /c del *.txt");
            // Kommer hit hvis exec() var vellykket
            System.out.println("Nå er filene slettet");
        } catch(Exception e){
            // Kommer hit hvis exec() ikke var vellykket
            System.out.println("Unntaksobjekt kastet: " + e.toString());
        }
    }
}

class Tegning extends JPanel {
    public void paintComponent(Graphics tegneflate) {
        super.paintComponent(tegneflate); //Husk denne!
        tegneflate.drawString("Dette er en suspekt applet, se etter meldinger i Java console", 5, 50);
    }
}