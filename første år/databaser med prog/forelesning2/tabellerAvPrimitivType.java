//package eksempler.kap7.eks7_1;
//import static lib.Out.*;
//lokal package, fungerer ikke for meg

public class tabellerAvPrimitivType{
    int[] intTabell = {2,3,4,5,6,7};
    boolean[] boolTabell = {true,false,false,true};
    char[] charTabell = {'a', 'b','c'};
    String[] stringTabell = {"settning 1", "settning 2"};

    public static void main(String[] args){
        tabellerAvPrimitivType t = new tabellerAvPrimitivType();
        out(t.intTabell);
        out(t.boolTabell);
        out(t.charTabell);
        out(t.stringTabell);

        Student1[] studenter1 = new Student1[3];
        Student2[] studenter2 = new Student2[3];

        studenter1[1] = new Student1("emir", 22);
        studenter2[1] = new Student2("rime", 21);

        out(studenter1);
        out(studenter2);

    }

    public static void out(Object[] objects){
        for(Object object:objects){
            if(object==null){
                System.out.println("null");
            } else {
                System.out.println(object.toString());
            }
        }
    }
    
    public static void out(int[] intTabell){
        for(int i = 0; i < intTabell.length; i++){
            System.out.println(intTabell[i]);
        }
    }
    public static void out(String[] intTabell){
        for(int i = 0; i < intTabell.length; i++){
            System.out.println(intTabell[i]);
        }
    }
    public static void out(char[] intTabell){
        for(int i = 0; i < intTabell.length; i++){
            System.out.println(intTabell[i]);
        }
    }
    public static void out(boolean[] intTabell){
        for(int i = 0; i < intTabell.length; i++){
            System.out.println(intTabell[i]);
        }
    }
}