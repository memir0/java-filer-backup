package dyrehage;

class HunnIndivid extends Individ{
    private int antKull = 0;
    public HunnIndivid(String navn, int fDato, boolean hanndyr, boolean farlig, String norskNavn, String latNavn, String latFamilie,
             int ankommetDato, String adresse, int antKull){
        super(navn, fDato, hanndyr, farlig, norskNavn, latNavn, latFamilie, ankommetDato, adresse);
        this.antKull = antKull;
    }

    @Override
    public int getAntKull(){
        return antKull;
    }

    public void setAntkull(int nyttAntall){
        antKull = nyttAntall;
    }

    @Override
    public void leggTilKull(int antall){
        antKull += antall;
    }

    @Override
    public void leggTilNyttKull(){
        antKull++;
    }
}