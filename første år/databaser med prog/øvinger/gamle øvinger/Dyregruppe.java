package dyrehage;
class Dyregruppe extends Dyr{
    final private String gruppenavn;
    private int antIdivider;

    public Dyregruppe(String gruppenavn, int antIdivider, String norskNavn, String latNavn, String latFamilie,
             int ankommetDato, String adresse){
        super(norskNavn, latNavn, latFamilie, ankommetDato, adresse);
        this.gruppenavn = gruppenavn;
        this.antIdivider = antIdivider;
    }

    @Override
    public String getNorskNavn(){
        return "gruppe av " + super.getNorskNavn();
    }

    public String getGruppeavn(){
        return gruppenavn;
    }

    public int getantIdivider(){
        return antIdivider;
    }

    public String toString(){
        String returString = "Gruppenavn: " + gruppenavn + "\nAntall individer: " + antIdivider;
        return returString;
    }
}