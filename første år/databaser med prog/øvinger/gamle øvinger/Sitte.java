class Sitte extends Tribune{
    private int [] antOpptatt;  // tabellstørrelse: antall rader
    private int seterPerRad, antallRader;

    public Sitte(String tribunenavn, int kapasitet, int pris, int antallRader){
        super(tribunenavn, kapasitet, pris);
        antOpptatt = new int[antallRader];
        seterPerRad = kapasitet/antallRader;
        this.antallRader = antallRader;
    }

    public int finnAntallSolgteBilletter(){
        int antOpptattPlasser = 0;

        for(int i = 0; i < antOpptatt.length; i++){
            antOpptattPlasser += antOpptatt[i];
        }
        return antOpptattPlasser;
    }

    public Billett[] kjopBilletter(int antallBilletter){
        if(antallBilletter < 1) return null;

        Billett[] returBiletter = new Billett[antallBilletter];

        if(antallBilletter+finnAntallSolgteBilletter()>super.getKapasitet()) return returBiletter;        

        for(int i = 0; i < antallRader; i++){
            if(seterPerRad-antOpptatt[i] >= antallBilletter){
                for(int j=0; j < antallBilletter; j++){
                        SitteplassBillett tempBillett = new SitteplassBillett(super.getTribunenavn(), super.getPris(), i, j+antOpptatt[i]);
                        returBiletter[j] = tempBillett;
                    }
                antOpptatt[i] += antallBilletter;
            }
        }
        return returBiletter;
    }

    public Billett[] kjopBilletter(String[] navn){
        int antallBilletter = navn.length;
        if(antallBilletter < 1) return null;

        Billett[] returBiletter = new Billett[antallBilletter];

        if(antallBilletter+finnAntallSolgteBilletter()>super.getKapasitet()) return returBiletter;        

        for(int i = 0; i < antallRader; i++){
            if(seterPerRad-antOpptatt[i] >= antallBilletter){
                for(int j=0; j < antallBilletter; j++){
                        SitteplassBillett tempBillett = new SitteplassBillett(super.getTribunenavn(), super.getPris(), i, j+antOpptatt[i]);
                        returBiletter[j] = tempBillett;
                    }
                antOpptatt[i] += antallBilletter;
            }
        }
        return returBiletter;
    }

    public String toString(){
        String returString = getTribunenavn();
        returString += "\n" + "Antall solgte billetter: " + finnAntallSolgteBilletter() + "/" + getKapasitet();
        returString += "\n" + "intekter: " + finnAntallSolgteBilletter()*getPris();
        for(int i = 0; i < antallRader; i++){
            returString += "\n" + "rad: " + (i+1) + ", antall opptatte " + antOpptatt[i];
        }
        return returString;
    }
}