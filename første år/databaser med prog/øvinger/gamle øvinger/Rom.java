import java.util.ArrayList;

public class Rom{
    private int romNummer;
    private int storrelse;

    ArrayList<Reservasjon> reservasjoner = new ArrayList<Reservasjon>();    

    public Rom(int romNummer, int storrelse){
        this.romNummer = romNummer;
        this.storrelse = storrelse;
    }

    public int getRomNummer(){
        return romNummer;
    }

    public int getStorrelse(){
        return storrelse;
    }

    public int antallReservasjoner(){
        return reservasjoner.size();
    }

    public ArrayList<Reservasjon> getReservasjoner(){
        return reservasjoner;
    }

    public boolean reserverRom(Reservasjon nyReservasjon){
            for(int i = 0; i < reservasjoner.size(); i++){
                if(reservasjoner.get(i).overlapp(nyReservasjon.getFraTid(), nyReservasjon.getTilTid()))
                    return false;
            }
            reservasjoner.add(nyReservasjon);
            return true;
    }

}