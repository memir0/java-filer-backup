import java.time.LocalDate;

class GullMedlem extends BonusMedlem{
    public GullMedlem(int medlNr, Personalia pers, LocalDate innmeldtDato, int poeng){
        super(medlNr, pers, innmeldtDato, poeng);
    }
    /* Unødvendig
    public int getMedlnr(){
        return super.getMedlnr();
    }

    public Personalia getPers(){
        return super.getPersonlia();
    }

    public LocalDate getInnmeldtDato(){
        return super.getInnmeldt();
    }

    public int getPoeng(){
        return super.getPoeng();
    }

    public int finnKvalPoeng(LocalDate dato){
        return super.finnKvalPoeng(dato);
    }

    public boolean okPassord(String password){
        return super.okPassord(password);
    }
    */
    public boolean registrerPoeng(int poeng){
        return super.registrerPoeng((int)(poeng*1.5));
    }
}
    