import dyrehage.SkandinaviskeRovdyr;
import dyrehage.Rovdyrfabrikk;
import static java.lang.System.out;

public class rovdyrTest{
    public static void main(String[] args){
        
        Rovdyrfabrikk rovFab = new Rovdyrfabrikk();

        SkandinaviskeRovdyr binne = rovFab.nyBinne("Kari", 2001, 2010, "Bjorn Bur 1");
        SkandinaviskeRovdyr bjorn = rovFab.nyHannbjorn("Kåre", 2000, 2010, "Bjorn Bur 1");

        SkandinaviskeRovdyr ulvetispe = rovFab.nyUlvetispe("Sharry", 2004, 2004, "Kunstig skog");
        SkandinaviskeRovdyr ulvehann = rovFab.nyUlvehann("Shako", 2008, 2009, "Kunstig skog");

        
        out.println(binne.getNavn());
        out.println(bjorn.getFDato());
        out.println(ulvetispe.getAlder());
        out.println(ulvehann.getAdresse());
        bjorn.flytt("isolert bur");
        out.println(bjorn.getAdresse());

        bjorn.leggTilNyttKull();
        binne.leggTilNyttKull();
        binne.leggTilKull(3);
        ulvetispe.leggTilNyttKull();

        out.println(bjorn.getAntKull());
        out.println(binne.getAntKull());
        out.println(ulvetispe.getAntKull());

        
    }
}