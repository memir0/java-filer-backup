import java.util.*;

class Bord{
    String[] reservasjonsNavn;

    public Bord(int antallBord){
        reservasjonsNavn = new String[antallBord];
    }

    public int getLedige(){
        int ledige = 0;
        for(int i = 0; i < reservasjonsNavn.length; i++){
            if(reservasjonsNavn[i] == null){
                ledige++;
            }
        }
        return ledige;
    }

    public int getAntallBord(){
        return reservasjonsNavn.length;
    }

    public boolean reserver(String navn, int antall){
        if(getLedige() >= antall){
            int reserverte = 0;
            for(int i = 0; i < reservasjonsNavn.length; i++){
                if(reservasjonsNavn[i] == null){
                    reservasjonsNavn[i] = navn;
                    reserverte++;
                    if(antall <= reserverte){
                        return true;
                    }
                }
            }
        }
        
        return false;
    }

    public int[] reservasjoner(String navn){
        ArrayList<Integer> tempListe = new ArrayList<Integer>();

        for(int i = 0; i < reservasjonsNavn.length; i++){
            if(reservasjonsNavn[i] != null){
                if(reservasjonsNavn[i].equals(navn)){
                    tempListe.add(i);
                }
            }
        }
        
        int[] returnListe = new int[tempListe.size()];

        for(int i = 0; i < tempListe.size(); i++){
            returnListe[i] = tempListe.get(i);
        }

        return returnListe;
    }

    public void frigi(int[] bordnummere){
        for(int i = 0; i < bordnummere.length; i++){
            reservasjonsNavn[bordnummere[i]] = null;
        }
    }
}

class Resturant{
    String navn;
    int etablertAar;
    Bord mineBord;

    public Resturant(String navn, int etablertAar, int antallBord){
        this.navn = navn;
        this.etablertAar = etablertAar;
        this.mineBord = new Bord(antallBord);
    }

    public String getNavn(){
        return navn;
    }

    public void setNavn(String nyttNavn){
        this.navn = nyttNavn;
    }

    public int getEtablertAar(){
        return etablertAar;
    }

    public int getAlder(){
        Calendar cal = Calendar.getInstance();
        int year = Calendar.getInstance().get(Calendar.YEAR);
        return year - etablertAar;
    }

    public int antallLedigeBord(){
        return mineBord.getLedige();
    }

    public int antallOpptatteBord(){
        return mineBord.getAntallBord() - mineBord.getLedige();
    }

    public void reserverBord(String navn, int antall){
        mineBord.reserver(navn, antall);
    }

    public int[] reserverteBord(String navn){
        return mineBord.reservasjoner(navn);
    }

    public void frigiBord(int[] bordnummere){
        mineBord.frigi(bordnummere);
    }

}

class klient{
    public static void main(String[] args){
        Resturant papaJohns = new Resturant("Papa Johns", 1999, 20);
        papaJohns.reserverBord("Knut", 5);
        int[] frittgaaendeBord = {1,3};
        papaJohns.frigiBord(frittgaaendeBord);
        System.out.println("alder: " + papaJohns.getAlder());

        int[] reservasjoner = papaJohns.reserverteBord("Knut");
        for(int i = 0; i < reservasjoner.length; i++){
            System.out.println(reservasjoner[i]);
        }
        System.out.println("antall opptatte bord: " + papaJohns.antallOpptatteBord());
    }
}