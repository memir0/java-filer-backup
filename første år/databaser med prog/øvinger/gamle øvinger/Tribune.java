import java.io.*;

abstract class Tribune implements Comparable<Tribune>, Serializable{
    private final String tribunenavn;
    private final int kapasitet;
    private final int pris;

    public Tribune(String tribunenavn, int kapasitet, int pris){
        this.tribunenavn = tribunenavn;
        this.kapasitet = kapasitet;
        this.pris = pris;
    }

    public int getKapasitet(){
        return kapasitet;
    }

    public String getTribunenavn(){
        return tribunenavn;
    }

    public int getPris(){
        return pris;
    }

    abstract public String toString();

    abstract public int finnAntallSolgteBilletter();

    public int finnInntekt(){ return finnAntallSolgteBilletter()*pris;}

    abstract public Billett[] kjopBilletter(int antallBilletter);

    abstract public Billett[] kjopBilletter(String[] navn);

    public int compareTo(Tribune annenTribune){
        int sum = finnInntekt() - annenTribune.finnInntekt();
        if(sum == 0) return 0;
        else if(sum > 0) return 1;
        else return -1;
    }
}