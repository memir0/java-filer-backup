package dyrehage;
public class Rovdyrfabrikk{
    public SkandinaviskeRovdyr nyBinne(String navn, int fDato, int ankommetDato, String adresse){
        return new HunnIndivid(navn, fDato, false, true, "Brunbjorn", "Urus arctos", "Ursidae", ankommetDato, adresse, 0);
    }
    public SkandinaviskeRovdyr nyHannbjorn(String navn, int fDato, int ankommetDato, String adresse){
        return new HannIndivid(navn, fDato, true, true, "Brunbjorn", "Urus arctos", "Ursidae", ankommetDato, adresse);
    }
    public SkandinaviskeRovdyr nyUlvetispe(String navn, int fDato, int ankommetDato, String adresse){
        return new HunnIndivid(navn, fDato, false, true, "Ulv", "Canis lupus", "Canidae", ankommetDato, adresse, 0);
    }
    public SkandinaviskeRovdyr nyUlvehann(String navn, int fDato, int ankommetDato, String adresse){
        return new HannIndivid(navn, fDato, true, true, "Ulv", "Canis lupus", "Canidae", ankommetDato, adresse);

    }
}