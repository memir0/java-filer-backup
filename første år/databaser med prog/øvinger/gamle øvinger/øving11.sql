/* 1 */
-- a --
SELECT * FROM ordrehode, ordredetalj
WHERE ordrehode.ordrenr = ordredetalj.ordrenr
AND levnr = 44;

-- b -- 
SELECT DISTINCT navn, levby FROM levinfo, ordrehode, ordredetalj
WHERE levinfo.levnr = ordrehode.levnr
AND ordrehode.ordrenr = ordredetalj.ordrenr
AND delnr = 1;

-- c --
SELECT levinfo.levnr, navn, pris FROM levinfo JOIN prisinfo 
USING(levnr)
WHERE prisinfo.delnr = 201
ORDER BY pris
LIMIT 1

-- d --
SELECT ordrenr, dato, delnr, beskrivelse, kvantum, pris, CONCAT(pris*kvantum)
FROM ordrehode 
JOIN ordredetalj USING(ordrenr)
JOIN prisinfo USING(delnr)
JOIN delinfo USING(delnr)

-- e --
SELECT delnr, levnr FROM prisinfo
WHERE pris > (SELECT pris FROM prisinfo WHERE katalognr = "X7770");

-- f --
-- i --
DROP TABLE IF EXISTS fylke;
DROP TABLE IF EXISTS levinfo;

CREATE TABLE fylke(
    byNavn  VARCHAR(45) NOT NULL,
    fylke   TEXT NOT NULL,
    CONSTRAINT fylke_pk PRIMARY KEY(byNavn)
);

CREATE TABLE levinfo( 
    levnr   INTEGER, 
    navn    VARCHAR(20) NOT NULL, 
    adresse VARCHAR(20) NOT NULL, 
    levby   VARCHAR(20) NOT NULL, 
    postnr  INTEGER NOT NULL,
    CONSTRAINT levinfo_pk PRIMARY KEY(levnr),
    CONSTRAINT levinfo_fk FOREIGN KEY(levby) REFERENCES fylke(byNavn)
);

insert into fylke(fylke, byNavn) values('Oslo', 'Oslo');
insert into fylke(fylke, byNavn) values('ostfold', 'os');
insert into fylke(fylke, byNavn) values('S-Trondelag', 'Trondheim');
insert into fylke(fylke, byNavn) values('Telemark', 'ol');

insert into levinfo(levnr, navn, adresse, levby, postnr) values(6,'Kontorekspressen AS','Skolegata 3','Oslo',1234);
insert into levinfo(levnr, navn, adresse, levby, postnr) values(82,'Kontordata AS','osveien 178','Trondheim',7023);
insert into levinfo(levnr, navn, adresse, levby, postnr) values(9,'Kontorutstyr AS','Villa Villekulla','os',1456);
insert into levinfo(levnr, navn, adresse, levby, postnr) values(44,'Billig og Bra AS','Aveny 56','Oslo',1222);
insert into levinfo(levnr, navn, adresse, levby, postnr) values(12,'Mister Office AS','Storgt 56','os',1456);
insert into levinfo(levnr, navn, adresse, levby, postnr) values(81,'Kontorbutikken AS','Gjennomveien 78','ol',3345);

-- ii --
DROP VIEW IF EXISTS gamelTabell;

CREATE VIEW gamelTabell AS
SELECT levnr, navn, adresse, levby, postnr, fylke
FROM levinfo JOIN fylke
WHERE levby = byNavn;

UPDATE gamelTabell
SET fylke = "oslo"
WHERE levby = "Oslo";
-- Fungerte --

DELETE FROM gamelTabell WHERE byNavn = "Trondheim";
-- Can not delete from join view --


INSERT INTO gamelTabell(levnr, navn, adresse, levby, postnr) values(69, "Emir's bakgate", "*nothin persenel kid", "Oslo", 420);
-- Fungerte --

-- g --
SELECT levBy FROM levinfo 
LEFT JOIN prisinfo USING(levnr)
GROUP BY levby
HAVING COUNT(delnr) = 0

-- h --
DROP VIEW ordre18;

CREATE VIEW ordre18 AS
SELECT delnr, levnr, pris FROM prisinfo
WHERE delnr = 3
OR delnr = 4

SELECT levnr, SUM(pris*kvantum) AS totalPris FROM ordre18 
JOIN ordredetalj 
WHERE ordre18.delnr = ordredetalj.delnr 
AND ordredetalj.ordrenr = 18
GROUP BY levnr
HAVING COUNT(*) = 2
ORDER BY totalPris
LIMIT 1

/* 2 */
-- a --
SELECT * FROM forlag; --?--

SELECT * FROM forlag 
WHERE LEFT(1, telefon) = "2"
UNION
SELECT * FROM forlag 
WHERE LEFT(1, telefon) != "2"
UNION
SELECT * FROM forlag
WHERE telefon IS NULL;

-- b --
SELECT AVG(dode) AS gjennomsnittsAlder
FROM(
    SELECT AVG(dod_aar - fode_aar) AS dode FROM forfatter
    WHERE fode_aar IS NOT NULL
    AND dod_aar IS NOT NULL
    UNION 
    SELECT AVG(YEAR(CURRENT_DATE) - fode_aar) AS levende FROM forfatter
    WHERE fode_aar > 1900
    AND dod_aar IS NULL
) AS dodeOgLevende

-- c --
SELECT SUM(antallDode) AS antallForfattereTelt
FROM(
    SELECT COUNT(*) AS antallDode FROM forfatter
    WHERE fode_aar IS NOT NULL
    AND dod_aar IS NOT NULL
    UNION 
    SELECT COUNT(*) AS antallLevende FROM forfatter
    WHERE fode_aar > 1900
    AND dod_aar IS NULL
) AS antallForfattere