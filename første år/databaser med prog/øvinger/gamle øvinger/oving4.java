import static javax.swing.JOptionPane.*;
import java.util.ArrayList;

public class oving4{

    private static Konferansesenter ks = new Konferansesenter();

    public static void main(String[] args){
        String antallRomInput = showInputDialog("Antall rom");
        int antallRom = Integer.parseInt(antallRomInput);

        ArrayList<Integer> romNummere = new ArrayList<Integer>();

        for(int i = 0; i < antallRom; i++){
            String romNummerInput = showInputDialog("Rom Nummer");
            int romNummer = Integer.parseInt(romNummerInput);
            String romStorrelseInput = showInputDialog("Rom Storrelse");
            int romStorrelse = Integer.parseInt(romStorrelseInput);
            ks.registrereRom(romNummer, romStorrelse);
            romNummere.add(romNummer);
        }

        for(int i = 0; i < romNummere.size(); i++){
            System.out.println("indeks " + i + " sitt romnummer er: " + ks.finnRomRomNummer(romNummere.get(i)).getRomNummer());
        }

        String antallReservasjonerInput = showInputDialog("Antall reservasjoner");
        int antallReservasjoner = Integer.parseInt(antallReservasjonerInput);

        String navn = showInputDialog("Ditt navn");
        String tlf = showInputDialog("Ditt nummer");
        Kunde kunde = new Kunde(navn, tlf);

        for(int i = 0; i < antallReservasjoner; i++){
            String romIndeksInput = showInputDialog("Rom Indeks");
            int romIndeks = Integer.parseInt(romIndeksInput);
            Rom tempRom = ks.finnRomIndeks(romIndeks);
            if(tempRom == null){
                showMessageDialog(null, "Indeksen gir ikke et gyldig rom");
                i--;
            } else {
                String fraTidspunktInput = showInputDialog("Fra tidspunkt");
                Tidspunkt fraTidspunkt = new Tidspunkt(Long.parseLong(fraTidspunktInput));
                String tilTidspunktInput = showInputDialog("Til tidspunkt");
                Tidspunkt tilTidspunkt = new Tidspunkt(Long.parseLong(tilTidspunktInput));

                Reservasjon nyReservasjon = new Reservasjon(fraTidspunkt, tilTidspunkt, kunde);
                boolean gjennomfort = tempRom.reserverRom(nyReservasjon);
                if(!gjennomfort){
                    showMessageDialog(null, "Tidspunktet overlapper med en annen reservasjon");
                    i--;
                }
            }
        }
        for(int i = 0; i < antallRom; i++){
                Rom tempRom = ks.finnRomIndeks(i);
                ArrayList<Reservasjon> reservasjoner = tempRom.getReservasjoner(); 
                System.out.println("Reservasjoner for rom nummer " + tempRom.getRomNummer() + ":");
                for(int j = 0; j < reservasjoner.size(); j++){
                    System.out.println(reservasjoner.get(j).toString());
                }
            }
    }
}