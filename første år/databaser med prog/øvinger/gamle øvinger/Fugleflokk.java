package dyrehage;
class Fugleflokk extends Dyregruppe{
    final private float gjennomsnittligVekt;
    final private boolean svommer;

    public Fugleflokk(float gjennomsnittligVekt, boolean svommer, String gruppenavn, int antIdivider, String norskNavn, String latNavn, String latFamilie,
             int ankommetDato, String adresse){
        super(gruppenavn, antIdivider, norskNavn, latNavn, latFamilie, ankommetDato, adresse);
        this.gjennomsnittligVekt = gjennomsnittligVekt;
        this.svommer = svommer;
    }

    public float getGjennomsnittligVekt(){
        return gjennomsnittligVekt;
    }

    public boolean getSvommer(){
        return svommer;
    }

    public String toString(){
        String returString = super.toString() + "\nGjennomsnittlig vekt: " + gjennomsnittligVekt;
        
        if(svommer) returString += "\nKan svomme";
        else returString += "\nKan ikke svomme";

        return returString;
    }
}