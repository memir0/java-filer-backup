package dyrehage;
import java.util.Calendar;

class Individ extends Dyr implements SkandinaviskeRovdyr{
    final private String navn;
    final private int fDato;
    final private boolean hanndyr;
    final private boolean farlig;

    public Individ(String navn, int fDato, boolean hanndyr, boolean farlig, String norskNavn, String latNavn, String latFamilie,
             int ankommetDato, String adresse){
        super(norskNavn, latNavn, latFamilie, ankommetDato, adresse);
        this.navn = navn;
        this.fDato = fDato;
        this.hanndyr = hanndyr;
        this.farlig = farlig;
    }

    public String getNavn(){
        return navn;
    }

    public int getFDato(){
        return fDato;
    }

    public boolean getHanndyr(){
        return hanndyr;
    }

    public boolean getFarlig(){
        return farlig;
    }

    public String skrivUtInfo(){
        String returString = super.toString();

        if(farlig) returString += "\nDyret er farlig";
        else returString += "\nDyret er ikke farlig";

        if(hanndyr) returString += "\nDyret er hunn";
        else returString += "\nDyret er hann";

        return returString;
    }

    public void flytt(String nyAdresse){
        setAdresse(nyAdresse);
    }

    public int getAlder(){
        return Calendar.getInstance().get(Calendar.YEAR) - fDato;
    }

    public int getAntKull(){
        return 0;
    }
    public void leggTilKull(int antall){}
    public void leggTilNyttKull(){}
}