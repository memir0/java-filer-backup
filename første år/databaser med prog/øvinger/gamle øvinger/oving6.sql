/* Oppgave 1 */
-- Oppgave a--
SELECT * /*projeksjon*/  FROM bok WHERE bok_id > 10 /*seleksjon*/;

-- Oppgave b--
SELECT * FROM bok, forlag;
-- Resultatet er en kombinasjon av de to tabellene hvor informasjonen fra forlag er dyttet inn--

-- Oppgave c--
SELECT * FROM bok 
    JOIN forlag 
    WHERE bok.forlag_id = forlag.forlag_id;

SELECT * FROM bok 
    NATURAL JOIN forlag;
--Vi får bare forlag_id en gang med natural join--

-- Oppgave d--
-- forlag og bok, og bok og forfatter er unionkompitable--

-- Unionkompatibilitet: union setter kolonner under hverandre istedenfor ved siden av hverandre. Lik type data

SELECT * FROM bok 
    INNER JOIN forlag 
    ON forlag.forlag_id = bok.forlag_id;

SELECT * FROM bok_forfatter 
    INNER JOIN forfatter 
    ON bok_forfatter.forfatter_id = forfatter.forfatter_id
    INNER JOIN bok
    ON bok.bok_id = bok_forfatter.bok_id;

/* Oppgave 2 */
-- Oppgave a--
SELECT DISTINCT forlag_navn FROM forlag;

-- husk distinct

-- Oppgave b--
SELECT forlag_navn 
    FROM forlag 
    LEFT JOIN bok 
    ON forlag.forlag_id = bok.forlag_id 
    WHERE bok.forlag_id IS NULL;

-- Oppgave c--
SELECT fornavn, etternavn, fode_aar 
    FROM forfatter 
    WHERE fode_aar = 1948;

-- Oppgave d--
SELECT forlag_navn, telefon 
    FROM forlag 
    NATURAL JOIN bok 
    WHERE bok.tittel = "Generation X";

-- Oppgave e--
SELECT tittel FROM bok_forfatter 
    NATURAL JOIN bok 
    WHERE forfatter_id = 7; 

SELECT tittel FROM bok_forfatter 
    NATURAL JOIN bok 
    NATURAL JOIN forfatter 
    WHERE etternavn = "Hamsun";

-- Oppgave f--
SELECT tittel, utgitt_aar, forlag_navn, adresse, telefon
    FROM forlag
    LEFT JOIN bok
    ON bok.forlag_id = forlag.forlag_id;