import java.util.ArrayList;

public class Konferansesenter{

    ArrayList<Rom> rom = new ArrayList<Rom>();    

    public boolean registrereRom(int romNummer, int storrelse){
        for(int i = 0; i < rom.size(); i++){
            Rom tempRom = rom.get(i);
            if(tempRom.getRomNummer() == romNummer){
                return false;
            }
        }
        Rom nyttRom = new Rom(romNummer, storrelse);
        rom.add(nyttRom);
        return true;
    }

    public boolean reserverRom(Rom rom, Tidspunkt fraTid, Tidspunkt tilTid, Kunde kunde){
        Reservasjon nyReservasjon = new Reservasjon(fraTid, tilTid, kunde);
        return rom.reserverRom(nyReservasjon);
    }

    public Rom finnRomRomNummer(int romnr){
        for(int i = 0; i < rom.size(); i++){
            Rom tempRom = rom.get(i);
            if(tempRom.getRomNummer() == romnr){
                return tempRom;
            }
        }
        return null;
    }

    public Rom finnRomIndeks(int indeks){
        if(indeks < rom.size()){
            return rom.get(indeks);
        }
        return null;
    }

    public int getAntallRom(){
        return rom.size();
    }
}