import java.util.ArrayList;
import java.util.Random;
import java.time.LocalDate;

public class Medlemsarkiv{

    ArrayList<BonusMedlem> medlemsListe = new ArrayList<BonusMedlem>();

    private BonusMedlem finnMedlem(int medlemsnr){
        for(int i = 0; i < medlemsListe.size(); i++){
            if(medlemsListe.get(i).getMedlnr() == medlemsnr){
                return medlemsListe.get(i);
            }
        }
        return null;
    }

    public int finnPoeng(int medlemsnr, String passord){
        BonusMedlem tempMedlem = finnMedlem(medlemsnr);
        if(tempMedlem == null) return -1;
        if(tempMedlem.okPassord(passord)) return tempMedlem.getPoeng();
        else return -1;
    }

    public boolean registrerPoeng(int medlemsnr, int poeng){
        if(poeng > 0){
            BonusMedlem tempMedlem = finnMedlem(medlemsnr);
            if(tempMedlem == null) return false;
            tempMedlem.registrerPoeng(poeng);
            return true;
        } else {
            return false;
        }
    }
    
    public int nyMedlem(Personalia pers, LocalDate innmeldt){
        int medlemsnr = finnLedigNr();
        BasicMedlem nyttMedlem = new BasicMedlem(medlemsnr, pers, innmeldt);
        medlemsListe.add(nyttMedlem);
        return medlemsnr;
    }

    private int finnLedigNr(){
        Random rand = new Random();
        int medlemsnr = rand.nextInt(2147483647);
        while(finnMedlem(medlemsnr) != null){
            medlemsnr = rand.nextInt(2147483647);
        }
        return medlemsnr;
    }

    public boolean sjekkMedlemmer(LocalDate dato){
        boolean updated = false;
        for(int i = 0; i < medlemsListe.size(); i++){
            BonusMedlem tempBonusMedlem = medlemsListe.get(i);
            if(tempBonusMedlem instanceof BasicMedlem){
                BasicMedlem tempMedlem = (BasicMedlem) tempBonusMedlem;
                if(tempMedlem.finnKvalPoeng(dato) >= 75000){
                    GullMedlem oppgradertMedlem = new GullMedlem(tempMedlem.getMedlnr(),tempMedlem.getPers(),tempMedlem.getInnmeldtDato(), tempMedlem.getPoeng());
                   medlemsListe.add(i, oppgradertMedlem);
                    updated = true;
                }
                else if(tempMedlem.finnKvalPoeng(dato) >= 25000){
                    SoelvMedlem oppgradertMedlem = new SoelvMedlem(tempMedlem.getMedlnr(),tempMedlem.getPers(),tempMedlem.getInnmeldtDato(), tempMedlem.getPoeng());
                    medlemsListe.add(i, oppgradertMedlem);
                    updated = true;
                }
            }
            if(tempBonusMedlem instanceof SoelvMedlem){
                SoelvMedlem tempMedlem = (SoelvMedlem) tempBonusMedlem;
                if(tempMedlem.finnKvalPoeng(dato) >= 75000){
                    GullMedlem oppgradertMedlem = new GullMedlem(tempMedlem.getMedlnr(),tempMedlem.getPers(),tempMedlem.getInnmeldtDato(), tempMedlem.getPoeng());
                    medlemsListe.add(i, oppgradertMedlem);
                    updated = true;
                }
            }
        }
        return updated;
    }    
}