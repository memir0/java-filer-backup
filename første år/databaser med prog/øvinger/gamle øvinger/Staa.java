class Staa extends Tribune{
    private int antallSolgt = 0;

    public Staa(String tribunenavn, int kapasitet, int pris){
        super(tribunenavn, kapasitet, pris);
    }

    public int finnAntallSolgteBilletter(){
        return antallSolgt;
    }

    public Billett[] kjopBilletter(int antallBilletter){
        if(antallBilletter < 1) return null;

        Billett[] returBiletter = new Billett[antallBilletter];

        if(antallBilletter+antallSolgt>super.getKapasitet()) return returBiletter;        

        for(int i = 0; i < antallBilletter; i++){
            StaaplassBillett tempBillett = new StaaplassBillett(super.getTribunenavn(), super.getPris());
            returBiletter[i] = tempBillett;
        }
        antallSolgt += antallBilletter;
        return returBiletter;
    }
    public Billett[] kjopBilletter(String[] navn){
        int antallBilletter = navn.length;
        if(antallBilletter < 1) return null;

        Billett[] returBiletter = new Billett[antallBilletter];

        if(antallBilletter+antallSolgt>super.getKapasitet()) return returBiletter;        

        for(int i = 0; i < antallBilletter; i++){
            StaaplassBillett tempBillett = new StaaplassBillett(super.getTribunenavn(), super.getPris());
            returBiletter[i] = tempBillett;
        }
        antallSolgt += antallBilletter;
        return returBiletter;
    }

    public String toString(){
        String returString = getTribunenavn();
        returString += "\n" + "Antall solgte billetter: " + finnAntallSolgteBilletter() + "/" + getKapasitet();
        returString += "\n" + "intekter: " + finnAntallSolgteBilletter()*getPris();
        return returString;
    }
}