package dyrehage;

class HannIndivid extends Individ{
    public HannIndivid(String navn, int fDato, boolean hanndyr, boolean farlig, String norskNavn, String latNavn, String latFamilie,
             int ankommetDato, String adresse){
        super(navn, fDato, hanndyr, farlig, norskNavn, latNavn, latFamilie, ankommetDato, adresse);
    }
}