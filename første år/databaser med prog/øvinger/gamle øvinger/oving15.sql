DROP TABLE IF EXISTS Kandidat;
DROP TABLE IF EXISTS Bedrift;
DROP TABLE IF EXISTS Kvalifikasjon;
DROP TABLE IF EXISTS Oppdrag;
DROP TABLE IF EXISTS Jobbhistorikk;
DROP TABLE IF EXISTS Kvalifikasjon_has_Kandidat;


CREATE TABLE Kandidat(
    kandidatNr INT NOT NULL AUTO_INCREMENT,
    fornavn TEXT,
    etternavn TEXT,
    tlf INT,
    epost TEXT,
    PRIMARY KEY(kandidatNr)
);

CREATE TABLE Bedrift(
    Orgnr INT NOT NULL AUTO_INCREMENT,
    navn TEXT,
    tlf INT,
    epost TEXT,
    PRIMARY KEY(Orgnr)
);

CREATE TABLE Kvalifikasjon(
    kvalifikasjonNr INT NOT NULL AUTO_INCREMENT,
    kvalifikasjon VARCHAR(45),
    PRIMARY KEY(kvalifikasjonNr)
);

CREATE TABLE Kvalifikasjon_has_Kandidat(
    kvalifikasjonNr INT NOT NULL,
    kandidatNr INT NOT NULL,
    PRIMARY KEY(kvalifikasjonNr,kandidatNr),
    FOREIGN KEY(kvalifikasjonNr) REFERENCES Kvalifikasjon(kvalifikasjonNr),
    FOREIGN KEY(kandidatNr) REFERENCES Kandidat(kandidatNr)
);

CREATE TABLE Oppdrag(
    oppdragNr INT NOT NULL AUTO_INCREMENT,
    bedriftNavn TEXT,
    kvalifikasjon VARCHAR(45),
    startDato DATE,
    sluttDato DATE,
    PRIMARY KEY(oppdragNr)
);

/* View? */
CREATE TABLE Jobbhistorikk(
    kandidatNr INT NOT NULL,
    oppdragNr INT NOT NULL,
    startDato DATE,
    sluttDato DATE,
    timer INT,
    atest TEXT, 
    PRIMARY KEY(kandidatNr, oppdragNr),
    FOREIGN KEY(kandidatNr) REFERENCES Kandidat(kandidatNr),
    FOREIGN KEY(oppdragNr) REFERENCES Oppdrag(oppdragNr)
);

-- INSERT --

INSERT INTO Kandidat(fornavn, etternavn, tlf, epost) VALUES("Emir", "D", 12345678, "emierd@nett.no");
INSERT INTO Bedrift(navn, tlf, epost) VALUES("EMIR CO", 1234, "noreply@emir.co");
INSERT INTO Kvalifikasjon(kvalifikasjon) VALUES("Excel");
INSERT INTO Kvalifikasjon_has_Kandidat VALUES(1,1);
INSERT INTO Oppdrag(bedriftNavn, kvalifikasjon, startDato, sluttDato) VALUES("EMIR CO", NULL, "2013-10-18", "2014-01-30");
INSERT INTO Jobbhistorikk(kandidatNr,oppdragNr,startDato,sluttDato,timer,atest) VALUES(1,1, "2013-10-18", "2019-01-22", 2000, "Emir er flink");

-- SELECT --
-- 1
SELECT navn, tlf, epost FROM Bedrift;

-- 2
SELECT oppdragNr, bedriftNavn, tlf FROM Oppdrag, Bedrift WHERE Bedrift.navn = Oppdrag.bedriftNavn;

-- 3
SELECT * FROM Kvalifikasjon_has_Kandidat, Kvalifikasjon, Kandidat
WHERE Kvalifikasjon.kvalifikasjonNr = Kvalifikasjon_has_Kandidat.kvalifikasjonNr
AND Kandidat.kandidatNr = Kvalifikasjon_has_Kandidat.kandidatNr;

-- 4
SELECT * FROM Kvalifikasjon_has_Kandidat 
LEFT JOIN Kandidat ON Kandidat.kandidatNr = Kvalifikasjon_has_Kandidat.kandidatNr
JOIN Kvalifikasjon
WHERE Kvalifikasjon.kvalifikasjonNr = Kvalifikasjon_has_Kandidat.kvalifikasjonNr;

-- 5
SELECT fornavn, Jobbhistorikk.sluttDato, Oppdrag.oppdragNr, bedriftNavn FROM Kandidat, Jobbhistorikk, Oppdrag
WHERE Kandidat.kandidatNr = 1
AND Jobbhistorikk.kandidatNr = Kandidat.kandidatNr
AND Oppdrag.oppdragNr = Jobbhistorikk.oppdragNr;