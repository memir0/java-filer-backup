-- DROP --
DROP TABLE IF EXISTS Borettslag;
DROP TABLE IF EXISTS Medlem;
DROP TABLE IF EXISTS Byggning;
DROP TABLE IF EXISTS Leilighet;

-- CREATE --
CREATE TABLE IF NOT EXISTS Borettslag(
        borettslagNr INT NOT NULL AUTO_INCREMENT, 
        navn TEXT NOT NULL, 
        adresse TEXT NOT NULL, 
        antallHus INT NOT NULL, 
        etablertAar INT,
        PRIMARY KEY (borettslagNr)
    );
CREATE TABLE IF NOT EXISTS Medlem(
        medlemsNr INT NOT NULL AUTO_INCREMENT, 
        navn TEXT,
        PRIMARY KEY (medlemsNr)
    );
CREATE TABLE IF NOT EXISTS Byggning(
        byggNr INT NOT NULL AUTO_INCREMENT, 
        antallLeiligheter INT NOT NULL,
        antallEtasjer INT NOT NULL,
        borettslagNr INT NOT NULL,
        PRIMARY KEY (byggNr),
        FOREIGN KEY (borettslagNr) REFERENCES Borettslag(borettslagNr)
    );
CREATE TABLE IF NOT EXISTS Leilighet(
        leilighetNr INT NOT NULL AUTO_INCREMENT, 
        antallRom INT NOT NULL,
        etasje INT NOT NULL,
        areal INT NOT NULL,
        eier INT,
        byggNr INT NOT NULL,
        PRIMARY KEY (leilighetNr),
        FOREIGN KEY (eier) REFERENCES Medlem(medlemsNr),
        FOREIGN KEY (byggNr) REFERENCES Byggning(byggNr)
    );

-- INSERT --
INSERT INTO Borettslag(navn, adresse, antallHus, etablertAar) 
    VALUES ("Emir's Guttahus", "Kongensgate 20", 20, 1999);

INSERT INTO Medlem(navn)
    VALUES ("Emir. D");

INSERT INTO Byggning(antallLeiligheter, antallEtasjer, borettslagNr)
    VALUES (20, 4, 1);

INSERT INTO Leilighet(antallRom, etasje, areal, eier, byggNr)
    VALUES (4, 1, 70, 1, 1);