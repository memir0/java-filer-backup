package dyrehage;
class Fiskestim extends Dyregruppe{
    final private float gjennomsnittligLengde;
    final private boolean kanDeleAkvarium;

    public Fiskestim(float gjennomsnittligLengde, boolean kanDeleAkvarium, String gruppenavn, int antIdivider, String norskNavn, String latNavn, String latFamilie,
             int ankommetDato, String adresse){
        super(gruppenavn, antIdivider, norskNavn, latNavn, latFamilie, ankommetDato, adresse);
        this.gjennomsnittligLengde = gjennomsnittligLengde;
        this.kanDeleAkvarium = kanDeleAkvarium;
    }

    public float getGjennomsnittligLengde(){
        return gjennomsnittligLengde;
    }

    public boolean getKanDeleAkvarium(){
        return kanDeleAkvarium;
    }

    public String toString(){
        String returString = super.toString() + "\nGjennomsnittlig lengde: " + gjennomsnittligLengde;

        if(kanDeleAkvarium) returString += "\nKan dele akvarium";
        else returString += "\nKan ikke dele akvarium";

        return returString;
    }
}