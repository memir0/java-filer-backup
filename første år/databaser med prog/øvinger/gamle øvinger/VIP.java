class VIP extends Tribune{
    private String[][] tilskuer; // tabellstørrelse: antall rader * antall plasser pr rad
    private int seterPerRad;
    private int[] antOpptatt;
    private int antallRader;

    public VIP(String tribunenavn, int kapasitet, int pris, int antallRader){
        super(tribunenavn, kapasitet, pris);
        antOpptatt = new int[antallRader];
        seterPerRad = kapasitet/antallRader;
        this.antallRader = antallRader;
        tilskuer = new String[antallRader][seterPerRad];
    }

    public int finnAntallSolgteBilletter(){
        int antOpptattPlasser = 0;

        for(int i = 0; i < antOpptatt.length; i++){
            antOpptattPlasser += antOpptatt[i];
        }
        return antOpptattPlasser;
    }

    public Billett[] kjopBilletter(int antallBilletter){
        return null;
    }

    public Billett[] kjopBilletter(String[] navn){
        int antallBilletter = navn.length;
        if(antallBilletter < 1) return null;

        Billett[] returBiletter = new Billett[antallBilletter];

        if(antallBilletter+finnAntallSolgteBilletter()>super.getKapasitet()) return returBiletter;        

        for(int i = 0; i < antallRader; i++){
            if(seterPerRad-antOpptatt[i] >= antallBilletter){
                for(int j=0; j < antallBilletter; j++){
                        SitteplassBillett tempBillett = new SitteplassBillett(super.getTribunenavn(), super.getPris(), i, j+antOpptatt[i]);
                        returBiletter[j] = tempBillett;
                        tilskuer[i][j+antOpptatt[i]] = navn[j];
                    }
                antOpptatt[i] += antallBilletter;
            }
        }
        return returBiletter;
    }

    public String toString(){
        try{
            String returString = getTribunenavn();
            returString += "\n" + "Antall solgte billetter: " + finnAntallSolgteBilletter() + "/" + getKapasitet();
            returString += "\n" + "intekter: " + finnAntallSolgteBilletter()*getPris();
            for(int i = 0; i < antallRader; i++){
                for(int j = 0; j < seterPerRad; j++){
                    if(tilskuer[i][j] != null){
                        returString += "\n" + tilskuer[i][j] + "; rad: " + (i+1) + ", sete: " + (j+1);
                    }
                }
            }
            return returString;
        }
        catch(Exception e){
            e.printStackTrace();
            return "noe gikk galt";
        }
    }
}