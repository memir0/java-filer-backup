import java.time.*;

class BonusMedlem{
    private final int medlNr;
    private final Personalia pers;
    private final LocalDate innmeldtDato; 
    private int poeng = 0;

    public BonusMedlem(int medlNr, Personalia pers, LocalDate innmeldtDato, int poeng){
        this.medlNr = medlNr;
        this.pers = pers;
        this.innmeldtDato = innmeldtDato;
        this.poeng = poeng;
    }

    public int getMedlnr(){
        return medlNr;
    }

    public Personalia getPersonlia(){
        return pers;
    }

    public LocalDate getInnmeldt(){
        return innmeldtDato;
    }

    public int getPoeng(){
        return poeng;
    }

    public int finnKvalPoeng(LocalDate dato){
        int aarMellom = Period.between(innmeldtDato, dato).getYears();
        
        if(aarMellom >= 1) return 0;
        else return poeng;
    }

    public boolean okPassord(String password){
        return pers.okPassord(password);
    }

    public boolean registrerPoeng(int poeng){
        this.poeng += poeng;
        return true; 
    }
}