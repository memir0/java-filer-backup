-- 1 --
SELECT * FROM borettslag 
WHERE etabl_aar > 1974 AND etabl_aar < 1986;

-- 2 --
SELECT CONCAT(fornavn," ", etternavn, " ansiennitet: ", ansiennitet, " år") FROM andelseier ORDER BY ansiennitet;

-- 3 --
SELECT etabl_aar FROM borettslag 
ORDER BY etabl_aar LIMIT 1;

-- 4 --
SELECT SUM(ant_rom) AS antallRom, bygn_adr 
FROM bygning JOIN leilighet
WHERE bygning.bygn_id = leilighet.bygn_id
GROUP BY bygn_adr
HAVING antallRom > 3;

-- 5 --
SELECT bolag_navn, COUNT(*) AS antallBygg FROM bygning
WHERE bolag_navn = "Tertitten"
GROUP BY bolag_navn;

-- 6 --
SELECT borettslag.bolag_navn, COUNT(bygning.bolag_navn) AS antallBygg
FROM borettslag LEFT JOIN bygning
ON borettslag.bolag_navn = bygning.bolag_navn
GROUP BY borettslag.bolag_navn
ORDER BY borettslag.bolag_navn;

-- 7 --
SELECT borettslag.bolag_navn, COUNT(*) AS antallLeiligheter
FROM borettslag JOIN bygning JOIN leilighet
WHERE borettslag.bolag_navn = bygning.bolag_navn 
AND bygning.bygn_id = leilighet.bygn_id
GROUP BY borettslag.bolag_navn;

-- 8 --
SELECT borettslag.bolag_navn, bygning.ant_etasjer AS HøyesteEtasje
FROM borettslag JOIN bygning 
WHERE borettslag.bolag_navn = bygning.bolag_navn
ORDER BY HøyesteEtasje DESC
LIMIT 1;

-- 9 --
SELECT fornavn, etternavn, telefon
FROM andelseier LEFT JOIN  leilighet
ON andelseier.and_eier_nr = leilighet.and_eier_nr
WHERE leil_nr IS NULL

-- 10 --
SELECT borettslag.bolag_navn, COUNT(andelseier.and_eier_nr) AS antallAndelsEiere
FROM borettslag 
LEFT JOIN andelseier ON borettslag.bolag_navn = andelseier.bolag_navn
GROUP BY borettslag.bolag_navn;
ORDER BY antallAndelsEiere DESC

-- 11 --
SELECT fornavn, etternavn, leil_nr 
FROM andelseier LEFT JOIN leilighet 
ON leilighet.and_eier_nr = andelseier.and_eier_nr;

-- 12 --
SELECT borettslag.bolag_navn 
FROM borettslag JOIN bygning JOIN leilighet
WHERE borettslag.bolag_navn = bygning.bolag_navn
AND bygning.bygn_id = leilighet.bygn_id
GROUP BY borettslag.bolag_navn;

-- 13 --
SELECT postnr, COUNT(postnr) AS antallAndelsEiere 
FROM andelseier JOIN leilighet JOIN bygning
WHERE andelseier.and_eier_nr = leilighet.and_eier_nr
AND leilighet.bygn_id = bygning.bygn_id
GROUP BY postnr

-- ekstra --
SELECT postnr, COUNT(and_eier_nr) AS antallAndelsEiere 
FROM bygning LEFT JOIN leilighet 
ON bygning.bygn_id = leilighet.bygn_id
LEFT JOIN andelseier
ON andelseier.and_eier_nr = leilighet.and_eier_nr
AND leilighet.bygn_id = bygning.bygn_id
GROUP BY postnr