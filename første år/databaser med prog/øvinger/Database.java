import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.ResultSet;

public class Database{
    Statement stmt = null;
    ResultSet rs = null;
    Connection conn = null;
    public Database(){
        try{
            conn = DriverManager.getConnection("jdbc:mysql://mysql.stud.iie.ntnu.no:3306/emirde?user=emirde&password=fnMNpNwI");
            stmt = conn.createStatement();
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }

    public void lukkForbindelse(){
        try{
            stmt.close();
            conn.close();
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }

    public boolean regNyBok(Bok nyBok){
        try{
            conn.setAutoCommit(false);

            //String sql = "SELECT * FROM boktittel WHERE isbn"

            sql = "insert into boktittel(isbn, forfatter, tittel) values('" + nyBok.getIsbn() + "', '" + nyBok.getForfatter() + "', '" + nyBok.getTittel() + "')";   
            stmt.executeUpdate(sql);   
            
            sql = "insert into eksemplar(isbn, eks_nr) values ('" + nyBok.getIsbn() + "', 1)";
            stmt.executeUpdate(sql); 

            conn.commit();
            conn.setAutoCommit(true);
            return true;
        }
        catch(SQLException se){
            se.printStackTrace();
            return false;
        }
    }

    public int regNyttEksemplar(String isbn){
        try{
            conn.setAutoCommit(false);

            String sql = "SELECT * FROM eksemplar WHERE isbn = '" + isbn + "'";
            rs = stmt.executeQuery(sql);
            int bigga = -1; 
            while(rs.next()){
                int eks_nr = rs.getInt("eks_nr");
                if(bigga < eks_nr) bigga = eks_nr;
            }

            if(bigga == -1) return 0;

            bigga += 1;

            sql = "insert into eksemplar(isbn, eks_nr) values ('" + isbn +"', " + bigga + ")";
            stmt.executeUpdate(sql); 

            conn.commit();
            conn.setAutoCommit(true);
            return bigga;
        }
        catch(SQLException se){
            se.printStackTrace();
            return 0;
        }
    }

    public boolean laanUtEksemplar(String isbn, String navn, int eksNr){
        try{
            String sql = "SELECT * FROM eksemplar WHERE eks_nr = " + eksNr +" AND isbn = '" + isbn + "'";
            rs = stmt.executeQuery(sql);
            if(!rs.next()) return false;

            sql = "update eksemplar set laant_av = '" + navn +"' where isbn = '" + isbn +"' and eks_nr = " + eksNr;
            stmt.executeUpdate(sql);
            return true;
        }
        catch(Exception e){
            e.printStackTrace();
            return false;
        }
    }

    public static void main(String[] args){
        Database db = new Database();
        Bok nyBok = new Bok("1234", "Pirater og greier", "Emir");

        System.out.println(db.regNyBok(nyBok));
        System.out.println(db.regNyttEksemplar("1234"));
        System.out.println(db.laanUtEksemplar("1234", "Knut", 1));

        db.lukkForbindelse();
    }
}