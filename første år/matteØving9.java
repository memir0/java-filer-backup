class EulersKalkulator {    
    public static void main(String[] args){
        eulersMetode(1, 0, 3, 1);
        eulersMetode(0.1, 0, 3, 1);
        eulersMetode(0.01, 0, 3, 1);
        eulersMetode(0.001, 0, 3, 1);   
        eulersMetode(0.0001, 0, 3, 1);
        eulersMetode(0.00001, 0, 3, 1);
        eulersMetode(0.000001, 0, 3, 1);           
    }

    private static void eulersMetode(double stegLengde, double x, double y, double stoppeSted){
        for(double i = x; i < stoppeSted; i += stegLengde){
            y = nesteY(x, y, stegLengde);
            x += stegLengde;
        }
        System.out.println("h = " + stegLengde + ": y(1) = " + y);
        System.out.println("Feil: " + Math.abs(2+Math.exp(-Math.pow(stoppeSted, 3))-y));
    }

    private static double nesteY(double forrigeX, double forrigeY, double stegLengde){
        return forrigeY+stegLengde*funksjonenAvXOgY(forrigeX, forrigeY);
    }

    private static double funksjonenAvXOgY(double x, double y){
        return 6*Math.pow(x,2)-3*Math.pow(x,2)*y;
    }
}