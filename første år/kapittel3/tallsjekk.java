import java.util.Scanner;

class tallsjekk{
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        int tall = sc.nextInt();
        if(tall < 0){
            System.out.println("tallet er negativt");
        } else {
            System.out.println("tallet er positivt");
        }
        int divisor = sc.nextInt();
        int produkt = tall/divisor;
        if(produkt*divisor == tall){
            System.out.println("tallet er delelig på " + divisor);
        } else{
            System.out.println("tallet er ikke delelig på " + divisor);
        }
        int nedre = sc.nextInt();
        int ovre = sc.nextInt();

        if(tall > nedre && tall < ovre){
            System.out.println("tallet er innenfor intervallet");
        } else {
            System.out.println("tallet er ikke innenfor intervallet");
        }
    }
}