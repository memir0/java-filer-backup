import static javax.swing.JOptionPane. *;

class build{
    public static void main(String[] args){
        String input = showInputDialog("skriv inn årstall: ");
        int aar = Integer.parseInt(input);
        if(aar%4==0 && aar%100!=0){
            showMessageDialog(null, "Det er skuddår!");
        }
        else if(aar%400==0){
            showMessageDialog(null, "Det er skuddår!");
        }
        else{
            showMessageDialog(null, "Det er ikke skuddår");
        }
    }
}