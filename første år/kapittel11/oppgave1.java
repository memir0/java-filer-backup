import static javax.swing.JOptionPane. *;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import java.util.Calendar;
import java.util.ArrayList;

public class oppgave1 {
    public static void main(String[] args){
        boolean quit = false;
        String input;
        ArrayList <Person> personRegister = new ArrayList<Person>();
        ArrayList <ArbTaker> ansatte = new ArrayList<ArbTaker>();

        while(!quit){
            Object[] options1 = { "Legg til person", "Legg til arbeidstaker",
                    "Administerer arbeidere", "Quit" };

            JPanel panel = new JPanel();
            panel.add(new JLabel("Arbeidstaker administrator 9000 ONLINE, vennligst velg handling:)"));

            int result = JOptionPane.showOptionDialog(null, panel, "Admin",
                    JOptionPane.DEFAULT_OPTION, JOptionPane.PLAIN_MESSAGE,
                    null, options1, null);
            if (result == 0){
                input = showInputDialog("Fornavn: ");
                String fornavn = input;
                input = showInputDialog("Etternavn: ");
                String etternavn = input;
                input = showInputDialog("Fodselsaar");
                int fodselsaar = Integer.parseInt(input);
                personRegister.add(new Person(fornavn, etternavn, fodselsaar));
            }
            else if (result==1){
                String personer = "";
                for(int i = 0; i < personRegister.size(); i++){
                    personer += i + ": " + personRegister.get(i).getFulltNavn() + "\n";
                }
                input = showInputDialog("Vennligst skriv inn tallet som koresponderer til personen du onsker å legge til som ansatt\n" + personer);
                int person = Integer.parseInt(input);
                input = showInputDialog("Ansettelsesaar: ");
                int ansettelseaar = Integer.parseInt(input);
                input = showInputDialog("Maanedslonn: ");
                int maanedslonn = Integer.parseInt(input);
                input = showInputDialog("Skatteprosent: ");
                float skatteprosent = Float.parseFloat(input);

                ansatte.add(new ArbTaker(personRegister.get(person), ansatte.size(), ansettelseaar, maanedslonn, skatteprosent));
            }
            else if (result==2){
                String personer = "";
                
                for(int i = 0; i < ansatte.size(); i++){
                    ArbTaker temp = ansatte.get(i);
                    personer += i + ": " + temp.getFulltNavn() + ", maanedslonn: " + temp.getMaandeslonn() + ", Skatteprosent: " + temp.getSkatteprosent()  + ", Total skatt: " + temp.totalSkatt() + ", Brutto årslønn: " + temp.aarslonn()  + "\n";
                }

                input = showInputDialog("Vennligst skriv inn tallet som koresponderer til den ansatte du onsker aa administrere\n" + personer);
                int person = Integer.parseInt(input);

                input = showInputDialog("1: Endre lonn\n2: Endre skatteprosent");
                int valg = Integer.parseInt(input);
                if(valg == 1){
                    input = showInputDialog("Ny lonn: ");
                    int nyLonn = Integer.parseInt(input);
                    ansatte.get(person).setMaanedslonn(nyLonn);
                    showMessageDialog(null, ansatte.get(person).getFulltNavn() + " sin lonn er naa satt til " + nyLonn);
                }
                else if(valg == 2){
                    input = showInputDialog("Ny skatteprosent: ");
                    float nySkatt = Float.parseFloat(input);
                    ansatte.get(person).setSkatteprosent(nySkatt);
                    showMessageDialog(null, ansatte.get(person).getFulltNavn() + " sin skatteprosent er naa satt til " + nySkatt);
                }
                
            }
            else {
                quit = true;
            }
        }
    }
}

class Person {
    private final String fornavn, etternavn;
    private int fodselsaar;

    public Person(String fornavn, String etternavn, int fodselsaar){
        this.fornavn = fornavn;
        this.etternavn = etternavn;
        this.fodselsaar = fodselsaar;
    }

    String getFornavn(){
        return fornavn;
    }
    String getEtternavn(){
        return etternavn;
    }
    int getFodselsaar(){
        return fodselsaar;
    }
    String getFulltNavn(){
        return etternavn + ", " + fornavn;
    }
}

class ArbTaker {
    private Person personlia = new Person("Placeholder", "Placeholder", 0);
    private int arbtakernr, ansettelseaar, maanedslonn;
    private float skatteprosent;

    ArbTaker(Person personlia, int arbtakernr, int ansettelseaar, int maanedslonn, float skatteprosent){
        this.personlia = personlia;
        this.arbtakernr = arbtakernr;
        this.ansettelseaar = ansettelseaar;
        this.maanedslonn = maanedslonn;
        this.skatteprosent = skatteprosent;
    }

    int getArbtakernr(){
        return arbtakernr;
    }
    int getAnsettelseaar(){
        return ansettelseaar;
    }
    int getMaandeslonn(){
        return maanedslonn;
    }
    float getSkatteprosent(){
        return skatteprosent;
    }
    void setSkatteprosent(float nyProsent){
        skatteprosent = nyProsent;
    }
    void setMaanedslonn(int nyLonn){
        maanedslonn = nyLonn;
    }
    int skattPerMaaned(){
        return (int)(maanedslonn/100*skatteprosent);
    }
    int aarslonn(){
        return maanedslonn*12;
    }
    int totalSkatt(){
        return (int)(maanedslonn*10/100*skatteprosent+maanedslonn/200*skatteprosent);
    }
    String getFulltNavn(){
        return personlia.getFulltNavn();
    }
    int alder(){
        Calendar now = Calendar.getInstance();   // Gets the current date and time
        return now.get(Calendar.YEAR) - personlia.getFodselsaar();
    }
    int aarAnsatt(){
        Calendar now = Calendar.getInstance();   // Gets the current date and time
        return now.get(Calendar.YEAR) - ansettelseaar;
    }
    boolean annsattMerEnn(int aar){
        return aarAnsatt() < aar;
    }
}